#!/bin/bash

MESHES_PATH=../meshes
PCD_PATH=../pcd

mkdir -p $PCD_PATH
for f in $MESHES_PATH/*.dae
do
    BASENAME=`basename $f`
    FILENAME=${BASENAME%%.*}
    echo $FILENAME
    meshlabserver -i $f -o $MESHES_PATH/$FILENAME.obj -s sample_mesh.mlx
    pcl_obj2pcd $MESHES_PATH/$FILENAME.obj $PCD_PATH/$FILENAME.binary.pcd
    pcl_convert_pcd_ascii_binary $PCD_PATH/$FILENAME.binary.pcd $PCD_PATH/$FILENAME.pcd 0
    rm $MESHES_PATH/$FILENAME.obj $PCD_PATH/$FILENAME.binary.pcd
done
