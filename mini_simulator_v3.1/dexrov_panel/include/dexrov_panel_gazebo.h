#ifndef DEXROV_PANEL_GAZEBO_H
#define DEXROV_PANEL_GAZEBO_H

#include <vector>
#include <string>
#include <set>
#include <map>
#include <angles/angles.h>
#include <gazebo_ros_control/robot_hw_sim.h>
#include <gazebo/physics/physics.hh>
#include <pluginlib/class_list_macros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Empty.h>

#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>

namespace dexrov_panel
{
    
static boost::mutex jointStateMutex;

class DexrovPanelGazebo : public gazebo_ros_control::RobotHWSim
{
public:

  DexrovPanelGazebo();

  bool initSim(const std::string& robot_ns,
               ros::NodeHandle nh,
               gazebo::physics::ModelPtr model,
               const urdf::Model* const urdf_model,
               std::vector<transmission_interface::TransmissionInfo> transmissions);
  void readSim(ros::Time time, ros::Duration period);
  void writeSim(ros::Time time, ros::Duration period);
  void triggerJointStatesCallback(const std_msgs::Empty::ConstPtr &msg);
  void init();

private:

    class CBoostRandomGenerator
    {
    public:
        typedef boost::normal_distribution<> NumberDistributionT;
        //typedef boost::uniform_real<> NumberDistributionT;

    private:
        struct timeval _start;
        boost::mt19937 _random_generator;
        boost::shared_ptr< NumberDistributionT > _number_distribution;
        boost::shared_ptr< boost::variate_generator<boost::mt19937&, NumberDistributionT > > _variate_generator;

    public:
    	CBoostRandomGenerator(double std_dev)
        {
            if(std_dev > 0)
            {
                gettimeofday (&_start, NULL);
                _random_generator.seed (_start.tv_usec);
                _number_distribution = boost::shared_ptr< NumberDistributionT > (new NumberDistributionT(0, std_dev));
                //_number_distribution = boost::shared_ptr< NumberDistributionT > (new NumberDistributionT(-std_dev, std_dev));
                _variate_generator = boost::shared_ptr< boost::variate_generator<boost::mt19937&, NumberDistributionT > >( new boost::variate_generator<boost::mt19937&, NumberDistributionT > (_random_generator,*_number_distribution));
            }
        }

        bool get(double &rand)
        {
            if(_variate_generator)
            {
                rand = (*_variate_generator)();
                // ROS_INFO_STREAM("rand: "<< rand);

                return true;

            }
            else
                return false;
        }
    };

  ros::NodeHandle nh;
  bool isInitialized;
  unsigned int numDofs;
  ros::Time time;
  std::vector<double> joints;
  std::vector<double> joints_noise;
  std::vector<std::string> jointNames;
  std::vector<gazebo::physics::JointPtr> gazeboJoints;
  ros::Subscriber recognitionTriggerSubscriber;
  ros::Publisher jointStatePublisher;
  ros::Publisher jointStatePublisher_gt;

  boost::shared_ptr<CBoostRandomGenerator> joint_state_noise_generator;
  double dexrov_panel_joint_state_noise_std_dev;
};

}

#endif // DEXROV_PANEL_GAZEBO_H
