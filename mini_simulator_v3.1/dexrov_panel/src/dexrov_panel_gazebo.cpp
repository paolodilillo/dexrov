#include <dexrov_panel_gazebo.h>

namespace dexrov_panel
{

DexrovPanelGazebo::DexrovPanelGazebo() : gazebo_ros_control::RobotHWSim()
{
}

// subscriber to recognition trigger -> only publish once when triggered
void DexrovPanelGazebo::triggerJointStatesCallback(const std_msgs::Empty::ConstPtr &msg) {
    
    // start panel joint state publishers when this callback is triggered for the first time
    if(!isInitialized) {
        init();
    }

    jointStateMutex.lock();
    sensor_msgs::JointState jointStateMsg;
    sensor_msgs::JointState jointStateNoiseMsg;
    jointStateNoiseMsg.header.frame_id = jointStateMsg.header.frame_id = "";
    jointStateNoiseMsg.header.stamp = jointStateMsg.header.stamp = this->time;

    for(unsigned int i = 0; i < numDofs; ++i)
    {
        jointStateMsg.name.push_back(jointNames[i]);
        jointStateMsg.position.push_back(joints[i]);

        jointStateNoiseMsg.name.push_back(jointNames[i]);
        jointStateNoiseMsg.position.push_back(joints_noise[i]);
    }
    
    jointStatePublisher_gt.publish(jointStateMsg);
    jointStatePublisher.publish(jointStateNoiseMsg);

    jointStateMutex.unlock();
}

bool DexrovPanelGazebo::initSim(const std::string& robot_ns,
    ros::NodeHandle nh, gazebo::physics::ModelPtr model,
    const urdf::Model* const urdf_model,
    std::vector<transmission_interface::TransmissionInfo> transmissions)
{
    ros::param::param("/dexrov/panel_joint_state_noise_std_dev", dexrov_panel_joint_state_noise_std_dev, 0.0);

    this->nh = nh;
    isInitialized = false;
    gazeboJoints.clear();
    joints.clear();
    joints_noise.clear();
    gazeboJoints = model->GetJoints();
    numDofs = gazeboJoints.size();
    joints.resize(numDofs);
    joints_noise.resize(numDofs);
    jointNames.resize(numDofs);

    // wait for trigger until we update the published joint states
    recognitionTriggerSubscriber = nh.subscribe("/panel/trigger_joint_states", 1, &DexrovPanelGazebo::triggerJointStatesCallback, this);
    joint_state_noise_generator = boost::shared_ptr<CBoostRandomGenerator>(new CBoostRandomGenerator(dexrov_panel_joint_state_noise_std_dev));

    return true;
}

void DexrovPanelGazebo::init() {

    jointStatePublisher = nh.advertise<sensor_msgs::JointState>("/panel/joint_states", 1);
    jointStatePublisher_gt = nh.advertise<sensor_msgs::JointState>("/panel/joint_states_gt", 1);
    
    isInitialized = true;
}

void DexrovPanelGazebo::readSim(ros::Time time, ros::Duration period)
{
    // read joint states
    jointStateMutex.lock();
    this->time = time;
    double noise = 0.0;

    for(unsigned int i = 0; i < numDofs; ++i)
    {
        jointNames[i] = gazeboJoints[i]->GetName();
        joints_noise[i] = joints[i] = gazeboJoints[i]->GetAngle(0).Radian();
        if(joint_state_noise_generator->get(noise)) {
           joints_noise[i] += noise;
        } else {
            std::cout<<"DexrovPanelGazebo::readSim ... could not add noise to joint!"<<std::endl;
        }
    }
    jointStateMutex.unlock();

}

void DexrovPanelGazebo::writeSim(ros::Time time, ros::Duration period)
{

}

} // dexrov_panel_gazebo

PLUGINLIB_EXPORT_CLASS(dexrov_panel::DexrovPanelGazebo, gazebo_ros_control::RobotHWSim)
