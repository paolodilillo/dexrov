#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/physics/Model.hh>
#include <gazebo/common/Plugin.hh>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include <vector>
#include <thread>
#include <geometry_msgs/Wrench.h>




using namespace std;



namespace gazebo
{


  
  class ForceSensorPlugin : public ModelPlugin
  {
  
  physics::ModelPtr model;
  physics::JointPtr joint1;
  geometry_msgs::Wrench wrench;
  
  private: std::unique_ptr<ros::NodeHandle> rosNode;
  private: ros::Publisher _sensorPublisher;
  private: event::ConnectionPtr updateConnection;
  private: ros::Time start, end;
  physics::JointWrench PrevWrench;
  
    /// \brief Constructor
    public: ForceSensorPlugin() {
    

    
    }
    
    
    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
    {
      
  if (_model->GetJointCount() == 0)
  {
    std::cerr << "\n\n\n\nInvalid joint count, joint force sensor plugin not loaded\n\n\n\n\n";
    return;
  }
 std::cerr << "\n\n\n\nThe Force Sensor plugin is attached to the model\n\n\n\n\n";
  
  int argc = 0;
  char **argv = NULL;
        
      ros::init(argc, argv, "force_sensor_plugin");
        
    
  std::string ns, topic_name, full_name;
  
  if (_sdf->HasElement("ns"))
  	ns = _sdf->Get<std::string>("ns");
  	
  if (_sdf->HasElement("topic"))
  	topic_name = _sdf->Get<std::string>("topic");
  	
  full_name = "/" + ns + "/" + topic_name;
  full_name = "/arm/desired_joint_velocity";
  	
  this->rosNode.reset(new ros::NodeHandle("force_sensor_plugin"));

      _sensorPublisher = rosNode->advertise<geometry_msgs::Wrench>("/arm/L/force", 1);
  
  this->model = _model;

  
  this->joint1 = _model->GetJoint("arm_joint6");
  joint1->SetProvideFeedback(true);
 
  start = ros::Time::now();
  end = ros::Time::now();
  
  PrevWrench = joint1->GetForceTorque(0);
  
   this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&ForceSensorPlugin::OnUpdate, this));

    }
    
  public: void OnUpdate(){
  
  end = ros::Time::now();
 // std::cout << (end-start).sec + (end-start).nsec*0.0000000001 << "\n\n";
  
 
  if((end-start).sec + (end-start).nsec*0.0000000001 >= 0.001){
	  	
	  physics::JointWrench jointwrench;
	  physics::JointWrench CurrJointWrench;
	  
	  CurrJointWrench = joint1->GetForceTorque(0);
	  jointwrench.body2Force = (CurrJointWrench.body2Force - PrevWrench.body2Force)/2; 
	  jointwrench.body2Torque = (CurrJointWrench.body2Torque - PrevWrench.body2Torque)/2;
	  PrevWrench = CurrJointWrench;
	  
	  wrench.force.x = jointwrench.body2Force.x;
	  wrench.force.y = jointwrench.body2Force.y;
	  wrench.force.z = jointwrench.body2Force.z;
	  wrench.torque.x = jointwrench.body2Torque.x;
	  wrench.torque.y = jointwrench.body2Torque.y;
	  wrench.torque.z = jointwrench.body2Torque.z;
	  _sensorPublisher.publish(wrench);
  		
  	  start = ros::Time::now();
  }
  



}





    
  };

  // Tell Gazebo about this plugin, so that Gazebo can call Load on this plugin.
  GZ_REGISTER_MODEL_PLUGIN(ForceSensorPlugin)
}
