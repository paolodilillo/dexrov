#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"

using namespace std;

double pose1[7];
double pose2[7];

bool init1 = false;
bool init2 = false;
bool done1 = true;
bool done2 = true;

void pose1CB(const geometry_msgs::PoseStamped msg){


if(done1){

pose1[0] = msg.pose.position.x;
pose1[1] = msg.pose.position.y;
pose1[2] = msg.pose.position.z;
pose1[3] = msg.pose.orientation.x;
pose1[3] = msg.pose.orientation.y;
pose1[3] = msg.pose.orientation.z;
pose1[3] = msg.pose.orientation.w;
init1 = true;
done1 = false;

}
}

void pose2CB(const geometry_msgs::PoseStamped msg){


if(done2){
pose2[0] = msg.pose.position.x;
pose2[1] = msg.pose.position.y;
pose2[2] = msg.pose.position.z;
pose2[3] = msg.pose.orientation.x;
pose2[3] = msg.pose.orientation.y;
pose2[3] = msg.pose.orientation.z;
pose2[3] = msg.pose.orientation.w;

init2 = true;
done2 = false;

}
}


int main(int argc, char **argv)
{

  ros::init(argc, argv, "trajectory");
  ros::NodeHandle n;
  
  ros::Publisher traj1_pub = n.advertise<geometry_msgs::PoseStamped>("/cog_proxy/L/desired_end_effector_pose", 1);
  ros::Publisher traj2_pub = n.advertise<geometry_msgs::PoseStamped>("/cog_proxy/R/desired_end_effector_pose", 1);
  
  ros::Subscriber pose_sub = n.subscribe("/arm/L/end_effector_pose_hf", 1, pose1CB);
  ros::Subscriber pose2_sub = n.subscribe("/arm/R/end_effector_pose_hf", 1, pose2CB);

  ros::Rate loop_rate(100);

  geometry_msgs::PoseStamped msg1, msg2;
  
  double r1=0.1, r2=0.1, omega1 = 0.3, omega2=0.3;
  
  double t=0;
  
  while (ros::ok())
  {

    if( init1 && init2){
    
    msg1.header.stamp = ros::Time::now();
    msg1.pose.position.x = pose1[0];
    msg1.pose.position.y = pose1[1] + r1 * cos(omega1*t);
    msg1.pose.position.z = pose1[2] + r1 * sin(omega1*t);
    msg1.pose.orientation.x = 0.5;
    msg1.pose.orientation.y = 0.5;
    msg1.pose.orientation.z = 0.5;
    msg1.pose.orientation.w = 0.5;
    
    
    msg2.header.stamp = ros::Time::now();
    msg2.pose.position.x = pose2[0];
    msg2.pose.position.y = pose2[1] + r2 * cos(omega2*t);
    msg2.pose.position.z = pose2[2] + r2 * sin(omega2*t);
    msg2.pose.orientation.x = 0.5;
    msg2.pose.orientation.y = 0.5;
    msg2.pose.orientation.z = 0.5;
    msg2.pose.orientation.w = 0.5;
    
    t+=0.01;
    
    traj1_pub.publish(msg1);
    traj2_pub.publish(msg2);

  }
  
  
   ros::spinOnce();
   loop_rate.sleep();
  
  
  }


  return 0;
}
