# Dexrov vehicle velocity plugin

## Description
This is the low-level controller for the vehicle in the Gazebo simulator. It reads a desired linear/angular velocity in vehicle frame and applies it to the model in the simulation. This version of the low-level controller is purely kinematic: the desired velocity is instantaneously applied to the model, without any kind of dynamics.

## Installation
In `dexrov_docker/dexrov_simulation/src` clone this repository:

`git clone git@gitlab.spaceapplications.com:dexrov/dexrov_vehicle_velocity_plugin.git`

move to `dexrov_docker` and build the code:

`./build.sh`

## Usage

To apply the plugin to the vehicle model edit the `dexrov_rov.gazebo` file in `dexrov_docker/dexrov_simulation/dexrov_rov_description/urdf`:

Remove the buoyancy plugin commenting these lines:

```xml
<!--
<gazebo>
	<plugin name="buoyancy" filename="libBuoyancyPlugin.so">
		<link name="base_link">
			<fluid_density>999.1026</fluid_density>
			<center_of_volume>0 0 0 </center_of_volume>
			<volume>0.218</volume>
		</link>
	</plugin>
	</gazebo>
-->

```
Set the gravity property to `false` for the links at the end of the file:

```xml
 	<gazebo reference="base_link">
		<gravity>false</gravity>
	</gazebo>
	<gazebo reference="depth_camera_frame">
		<gravity>false</gravity>
  	    <material>Gazebo/Black</material>
	</gazebo>
	<gazebo reference="logical_camera_frame">
		<gravity>false</gravity>
  	    <material>Gazebo/Black</material>
	</gazebo>

```

Apply this plugin by adding these lines:

```xml
<gazebo>
<plugin name="isme_vehicle_control" filename="libdexrov_vehicle_velocity_plugin.so">
   		
		<topic>cmd_vel</topic>
		<ns> rov </ns>
    </plugin>
</gazebo>

```

The plugin reads the desired vehicle velocity from topic. It receives two parameters as input:
1. `<ns>`: prefix of the topic to read the desired velocity from
2. `<topic>`: the topic to read the desired velocity from

The given example reads the desired vehicle velocity from the topic `/rov/cmd_vel`


