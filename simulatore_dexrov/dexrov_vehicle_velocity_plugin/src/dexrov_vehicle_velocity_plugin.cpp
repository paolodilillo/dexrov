#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/physics/Model.hh>

#include <gazebo/common/Plugin.hh>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include <vector>
#include <thread>
#include <geometry_msgs/Twist.h>
#include <dexrov_msgs/RovTeleopCmd.h>


using namespace std;



namespace gazebo
{
  
  class VehicleVelocityPlugin : public ModelPlugin
  {
  
  physics::ModelPtr model;
  physics::LinkPtr link;
  physics::LinkPtr camera_link;
 
  
  double velocity[6];
  double vel_des[3];
  double yaw_des;
  double pitch_des, roll_des;
  

  private: std::unique_ptr<ros::NodeHandle> rosNode;
  private: ros::Subscriber rosSub;
  private: ros::Subscriber rosSub_directcommands;
  private: ros::CallbackQueue rosQueue;
  private: ros::CallbackQueue rosQueue_directcommands;
  private: std::thread rosQueueThread;
  private: std::thread rosQueueThread_directcommands;
  
  private: event::ConnectionPtr updateConnection;

  
  
    public: VehicleVelocityPlugin() {}
    
  
  
  public: void OnRosMsg(const geometry_msgs::TwistConstPtr &_msg)
{


	vel_des[0] = _msg->linear.x;
	vel_des[1] = _msg->linear.y;
	vel_des[2] = _msg->linear.z;

	yaw_des = _msg->angular.z;
//	roll_des = _msg->angular.x;
//	pitch_des = _msg->angular.y;
	
	
}


  public: void OnRosMsg_directcommands(const dexrov_msgs::RovTeleopCmdConstPtr &_msg)
{



	double lateral;
	double vertical;
	double forwardreverse;
	double rotate;

	double powerout_value = 5.0;

	float u_max = 1.6; 		//m/s	(-10% of maximum velocity in Performance/Dimensions)
	float v_max = 1.3;
	float w_max = 0.68;
	float r_max = 120 * M_PI/180; 	//rad/s

	lateral = (double)_msg->lateral;
	vertical = (double)_msg->vertical;
	forwardreverse = (double)_msg->forwardReverse;
	rotate = (double)_msg->rotate;
	
	vel_des[0] = forwardreverse * u_max/powerout_value;
	vel_des[1] = lateral * v_max/powerout_value;
	vel_des[2] = vertical * w_max/powerout_value;

	yaw_des = rotate * r_max/powerout_value;
	
	if(vel_des[0] > u_max)
	
		vel_des[0] = u_max;
			
	if(vel_des[1] > v_max)
	
		vel_des[1] = v_max;
			
	if(vel_des[2] > w_max)
	
		vel_des[2] = w_max;
			
	if(yaw_des > r_max)
	
		yaw_des = r_max;
		
		
		if(vel_des[0] < -u_max)
	
		vel_des[0] = -u_max;
			
	if(vel_des[1] < -v_max)
	
		vel_des[1] = -v_max;
			
	if(vel_des[2] < -w_max)
	
		vel_des[2] = -w_max;
			
	if(yaw_des < -r_max)
	
		yaw_des = -r_max;
		
//	cout << vel_des[0] << "\t" << vel_des[1] << "\t" << vel_des[2] << "\t" << yaw_des << "\n\n";
	
}


private: void QueueThread()
{
  static const double timeout = 0.01;
  while (this->rosNode->ok())
  {
    this->rosQueue.callAvailable(ros::WallDuration(timeout));
  }
}


private: void QueueThread_directcommands()
{
  static const double timeout = 0.01;
  while (this->rosNode->ok())
  {
    this->rosQueue_directcommands.callAvailable(ros::WallDuration(timeout));
  }
}


    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
    {
       
  if (_model->GetJointCount() == 0)
  {
    std::cerr << "Invalid joint count, vehicle velocity plugin not loaded\n";
    return;
  }
 std::cerr << "The vehicle velocity plugin is attached to the model\n";
  
  int argc = 0;
  char **argv = NULL;
        
      ros::init(argc, argv, "vehicle_velocity_plugin");
        
    for(int i=0;i<6;i++)
  
  	velocity[i] = 0;
  
  std::string ns, topic_name, full_name;
  
  if (_sdf->HasElement("ns"))
  	ns = _sdf->Get<std::string>("ns");
  	
  if (_sdf->HasElement("topic"))
  	topic_name = _sdf->Get<std::string>("topic");
  	
  full_name = "/" + ns + "/" + topic_name;
  
  	
  this->rosNode.reset(new ros::NodeHandle("vehicle_velocity_plugin"));


ros::SubscribeOptions so =
  ros::SubscribeOptions::create<geometry_msgs::Twist>(
      full_name,
      1,
      boost::bind(&VehicleVelocityPlugin::OnRosMsg, this, _1),
      ros::VoidPtr(), &this->rosQueue);
this->rosSub = this->rosNode->subscribe(so);

std::string topic_directcommands = "/rov/direct_commands";

ros::SubscribeOptions so_directcommands =
  ros::SubscribeOptions::create<dexrov_msgs::RovTeleopCmd>(
      topic_directcommands,
      1,
      boost::bind(&VehicleVelocityPlugin::OnRosMsg_directcommands, this, _1),
      ros::VoidPtr(), &this->rosQueue_directcommands);
this->rosSub_directcommands = this->rosNode->subscribe(so_directcommands);



this->rosQueueThread =
  std::thread(std::bind(&VehicleVelocityPlugin::QueueThread, this));
  
this->rosQueueThread_directcommands =
  std::thread(std::bind(&VehicleVelocityPlugin::QueueThread_directcommands, this));
   

  this->model = _model;
  this->link = this->model->GetLink("base_link");
  
  math::Pose pose_des(0,0,0,0,0,0);
  math::Vector3 vel(0,0,0);
  
  this->link->SetGravityMode(false);
  this->camera_link = _model->GetLink("camera_link");

  //this->camera_link->SetGravityMode(false);
  
  
  
 
  
  for(int i=0;i<3;i++)
  
  	vel_des[i] = 0.0;
  
  yaw_des = 0.0;
 
 this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&VehicleVelocityPlugin::OnUpdate, this));
      

    }
    
    
    
    /////////////////////////////////////////////////////////////7
    
    
  public: void OnUpdate(){

 
    math::Pose curr_pose = this->model->GetWorldPose();
    math::Vector3 rpy = this->model->GetWorldLinearVel();
    math::Vector3 angles = curr_pose.rot.GetAsEuler();
 
   

    
  //  math::Vector3 LinVelWorld(vel_des[0] * cos(angles[2]) + vel_des[1] * sin(angles[2]), -vel_des[0] * sin(angles[2]) + vel_des[1] * cos(angles[2]), vel_des[2]);
    
    math::Vector3 LinVelWorld(vel_des[0] * cos(angles[2]) - vel_des[1] * sin(angles[2]), vel_des[0] * sin(angles[2]) + vel_des[1] * cos(angles[2]), vel_des[2]);
    
    math::Vector3 AngVelWorld(0,0,yaw_des);
    

    this->link->SetLinearVel(LinVelWorld);
    this->link->SetAngularVel(AngVelWorld);

}

  
    
  };

  // Tell Gazebo about this plugin, so that Gazebo can call Load on this plugin.
  GZ_REGISTER_MODEL_PLUGIN(VehicleVelocityPlugin)
}
