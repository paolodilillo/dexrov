/*

Node to conversion quaternion to roll, pitch and yaw.

Subscribe message of PoseStamped (position and orientation in quaternion): reference
Subscribe message of PoseStamped (position and orientation in quaternion): estimated pose
Publish a customized message (position and orientation in rpy)

*/

#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "nav_msgs/Odometry.h"
#include "dexrov_vehicle_control/RovConvertedPose.h"
#include "dexrov_msgs/RovTeleopCmd.h"
#include "dexrov_msgs/Waypoints.h"
#include <math.h>

bool flag_estimated_pose = false; //true if a message of estimated pose is received
bool flag_goal_pose = false; //true if a message of reference is received

dexrov_vehicle_control::RovConvertedPose convertedPose;

//Use of global publisher and subscriber
ros::Publisher converted_pose_publisher;
ros::Subscriber goal_pose_subscriber;
ros::Subscriber est_pose_subscriber;

float temp_roll, temp_pitch, temp_yaw;
float goal_roll, goal_pitch, goal_yaw;
float est_roll, est_pitch, est_yaw;

//Convert quaternion to roll, pitch and yaw angles
int conversionQuaternionToRPY(float x, float y, float z, float w)
{
	float magnitude;
	float sqw, sqx, sqy, sqz;

	sqx = x*x;
	sqy = y*y;
	sqz = z*z;
	sqw = w*w;

	//Normalize quaternion
	magnitude = sqrt(sqx + sqy + sqz + sqw);
	x = x/magnitude;
	y = y/magnitude;
	z = z/magnitude;
	w = w/magnitude;
	
	temp_roll = atan2(2*(y*z+w*x), sqw-sqx-sqy+sqz);
	
	double sarg = -2*(x*z-w*y);
	temp_pitch = sarg <= -1.0 ? -0.5*M_PI : (sarg >= 1.0 ? 0.5*M_PI : asin(sarg));

	temp_yaw = atan2(2*(x*y + w*z), sqw+sqx-sqy-sqz);
	
	
}

//publish pose message for control law
int publishPose()
{
	if ((flag_goal_pose) && (flag_estimated_pose))
	{	
	converted_pose_publisher.publish(convertedPose);
	//ROS_INFO("published rpy angles: roll=%f pitch=%f yaw=%f", convertedPose.roll, convertedPose.pitch, convertedPose.yaw);
	}
}

//Function of callback of desired Pose. 
void MsgCallback(const dexrov_msgs::Waypoints::ConstPtr& msg)
{
//	ROS_INFO("received\n");
	float x,y,z,w;
	
	convertedPose.position_x_des.resize(msg->waypoints_list.size());
	convertedPose.position_y_des.resize(msg->waypoints_list.size());
	convertedPose.position_z_des.resize(msg->waypoints_list.size());
	
	convertedPose.roll_des.resize(msg->waypoints_list.size());
	convertedPose.pitch_des.resize(msg->waypoints_list.size());
	convertedPose.yaw_des.resize(msg->waypoints_list.size());
	
	
	for (int i=0;i<msg->waypoints_list.size();i++){
	
	x = msg->waypoints_list[i].pose.orientation.x;
	y = msg->waypoints_list[i].pose.orientation.y;
	z = msg->waypoints_list[i].pose.orientation.z;
	w = msg->waypoints_list[i].pose.orientation.w;
	
	conversionQuaternionToRPY(x, y, z, w);

	goal_roll = temp_roll;
	goal_pitch = temp_pitch;
	goal_yaw = temp_yaw;

	convertedPose.position_x_des[i] = msg->waypoints_list[i].pose.position.x;
	convertedPose.position_y_des[i] = msg->waypoints_list[i].pose.position.y;
	convertedPose.position_z_des[i] = msg->waypoints_list[i].pose.position.z;

	convertedPose.roll_des[i] = goal_roll;
	convertedPose.pitch_des[i] = goal_pitch;
	convertedPose.yaw_des[i] = goal_yaw;
	
	}
	
	
	flag_goal_pose = true;
	publishPose();
}

//Function of callback of estimated Pose.
void MsgCallback_estimated(const nav_msgs::Odometry msgOdom)
{

//	ROS_INFO("received 2\n");
	float x,y,z,w;
	x = msgOdom.pose.pose.orientation.x;
	y = msgOdom.pose.pose.orientation.y;
	z = msgOdom.pose.pose.orientation.z;
	w = msgOdom.pose.pose.orientation.w;

	conversionQuaternionToRPY(x, y, z, 
w);
	
	est_roll = temp_roll;
	est_pitch = temp_pitch;
	est_yaw = temp_yaw;

	convertedPose.position_x_est = msgOdom.pose.pose.position.x;
	convertedPose.position_y_est = msgOdom.pose.pose.position.y;
	convertedPose.position_z_est = msgOdom.pose.pose.position.z;

	convertedPose.roll_est = est_roll;
	convertedPose.pitch_est = est_pitch;
	convertedPose.yaw_est = est_yaw;

	flag_estimated_pose = true;
	publishPose();
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "dexrov_pose");

	ros::NodeHandle nh;
		
	converted_pose_publisher = nh.advertise<dexrov_vehicle_control::RovConvertedPose>("acquired_pose", 1000);
	goal_pose_subscriber = nh.subscribe("/rov/waypoint", 1000, MsgCallback); 
	est_pose_subscriber = nh.subscribe("/rov/odometry", 1000, MsgCallback_estimated); 

	ros::spin();
}


