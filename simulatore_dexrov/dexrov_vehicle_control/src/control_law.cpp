#include <iostream>
#include <Eigen/Dense>
#include <math.h>
#include <limits>

#include <ros/ros.h>
#include <cstdlib>

#include <sys/io.h>
#include <sys/mman.h>


//#include <dexrov_vehicle_control/RovConvertedPose.h>
#include "dexrov_msgs/Waypoints.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Bool.h"

#include <dexrov_msgs/AutoHeading.h>
#include <dexrov_msgs/AutoDepth.h>
#include <dexrov_msgs/AutoAltitude.h>
#include <dexrov_msgs/Command.h>

using namespace Eigen;
using namespace std;

/* INPUT computation_error
		reference: 	position and orientation reference
		p_hat: 		estimated position
		R_hat: 		estimated rotation matrix

/* INPUT dexrov_ctrl
		error: 		position error in Body Frame
		psi_tilde: 	yaw error
		e_old: 		position error in Body Frame at previous timestamp
		psi_tilde_old: 	yaw error at previous timestamp
		uold: 		velocity command at previous timestamp
		omegaold: 	yaw rate at previous timestamp
		delta_T: 	sampling period
		gains: 		controller gains
		vmax: 		max vehicle velocity (linear and angular)
		ebar: 		threshold values of distance of target along surge, sway and heave when to go at max speed (i.e. when to saturate the ROV velocity)
		estop: 		threshold values of position and yaw error when the target can be considered reached
*/

/*output dexrov_ctrl
		unew_sat	body reference velocity command saturated
		uold		velocity command at previous timestamp
		omeganew_sat	yaw rate saturated
		omegaold 	yaw rate at previous timestamp
		e		position error in expressed body frame
		psi_tilde	yaw error
		goal_reached	flag that indicate if the goal has been reached
*/



float lateral_volts;
float vertical_volts;
float forwardreverse_volts;
float rotate_volts;

ros::Publisher twist_publisher;
ros::Publisher reached_publisher;
ros::Publisher error_publisher;
ros::Publisher ack_publisher;
ros::Subscriber estimated_pose_subscriber;
ros::Subscriber desired_pose_subscriber;


ros::Subscriber autoaltitude_subscriber;
ros::Subscriber autoheading_subscriber;
ros::Subscriber autodepth_subscriber;

geometry_msgs::PoseStamped error_msg;

MatrixXf reference;	//desidered pose
Vector3f p_hat(3);	//from Jacobs
Matrix3f R_Hat(3,3);	//from Jacobs
//Matrix3f R_transpose(3,3);

float psi_hat;
float psi_d;
Vector3f error(3);
float psi_tilde;

float phi_hat;
float theta_hat;

float roll;
float pitch;
float yaw;

int gamma1;
int gamma2;
int gamma3;

Vector3f unew(3);
Vector3f uold(3);
Vector3f unew_sat(3);
Vector3f omeganew(3);
Vector3f omeganew_sat(3);
float rold;
float rnew;
float delta_T = 1/100; //10 Hertz 
float psi_tilde_old;

Vector3f omegaold(3);
Vector3f e(3); //error in Body Frame
Vector3f e_old(3); //error in Body Frame

float xi = 1.0;			
Vector3f omegan(3); 

float xi_psi = 1.0;			
float omegan_psi;

Matrix3f gains_Kp(3,3);		
Matrix3f gains_KI(3,3);
float gains_kppsi;
float gains_kIpsi;

Matrix3f Kp(3,3); 
Matrix3f KI(3,3);
float kppsi;
float kIpsi;

float u_max = 0.6;//1.6; 		//m/s	(-10% of maximum velocity in Performance/Dimensions)
float v_max = 0.6;//1.3;
float w_max = 0.3;//0.68;
float r_max = 30 * M_PI/180;//120 * M_PI/180; 	//rad/s


float eu_stop = 0.01; //m  	//To set on board (error margin)
float ev_stop = 0.003;
float ew_stop = 0.01;
float psi_tilde_stop = 0.1 * M_PI/180; //rad

float eu_bar = 5.0; //m		
float ev_bar= 5.0;
float ew_bar= 5.0;
float psi_tilde_bar= 120 * M_PI/180; //rad

bool compute_yaw_rate;

bool goal_reached;
int current_wp;

float currentx,currenty,currentz,currentw;

float temp_roll, temp_pitch, temp_yaw;
float goal_roll, goal_pitch, goal_yaw;
float est_roll, est_pitch, est_yaw;

bool flag_goal_pose = false;
bool flag_estimated_pose = false;
bool new_waypoints = false;

bool flag_autoaltitude = false;
bool flag_autoheading = false;
bool flag_autodepth = false;

geometry_msgs::Twist twistMsg;

double xini[7];
MatrixXd wp_quat;
/*int rpy(Matrix3f R)
{
	phi_hat = atan2(R(2,1),R(2,2)); //roll
	theta_hat  = atan2(-R(2,0), sqrt((R(2,1)*R(2,1)) + (R(2,2)*R(2,2)))); //pitch
	psi_hat = atan2(R(1,0), R(0,0));	//yaw 
}*/


//Convert quaternion to roll, pitch and yaw angles
int conversionQuaternionToRPY(float x, float y, float z, float w)
{
	float magnitude;
	float sqw, sqx, sqy, sqz;

	sqx = x*x;
	sqy = y*y;
	sqz = z*z;
	sqw = w*w;

	//Normalize quaternion
	magnitude = sqrt(sqx + sqy + sqz + sqw);
	x = x/magnitude;
	y = y/magnitude;
	z = z/magnitude;
	w = w/magnitude;
	
	temp_roll = atan2(2*(y*z+w*x), sqw-sqx-sqy+sqz);
	
	double sarg = -2*(x*z-w*y);
	temp_pitch = sarg <= -1.0 ? -0.5*M_PI : (sarg >= 1.0 ? 0.5*M_PI : asin(sarg));

	temp_yaw = atan2(2*(x*y + w*z), sqw+sqx-sqy-sqz);
	
	
}


//Function of callback of desired Pose. 
void MsgCallback(const dexrov_msgs::Waypoints::ConstPtr& msg)
{
//	ROS_INFO("received\n");
	
	float x,y,z,w;
	xini[0] = msg->waypoints_list[0].pose.position.x;
	xini[1] = msg->waypoints_list[0].pose.position.y;
	xini[2] = msg->waypoints_list[0].pose.position.z;
	
	xini[3] = msg->waypoints_list[0].pose.orientation.x;
	xini[4] = msg->waypoints_list[0].pose.orientation.y;
	xini[5] = msg->waypoints_list[0].pose.orientation.z;
	xini[6] = msg->waypoints_list[0].pose.orientation.w;
	
	reference.resize(6,msg->waypoints_list.size());
	wp_quat.resize(4,msg->waypoints_list.size());
	
	for (int i=0;i<msg->waypoints_list.size();i++){
	
	x = msg->waypoints_list[i].pose.orientation.x;
	y = msg->waypoints_list[i].pose.orientation.y;
	z = msg->waypoints_list[i].pose.orientation.z;
	w = msg->waypoints_list[i].pose.orientation.w;
	
	conversionQuaternionToRPY(x, y, z, w);

	goal_roll = temp_roll;
	goal_pitch = temp_pitch;
	goal_yaw = temp_yaw;

	reference(0,i) = msg->waypoints_list[i].pose.position.x;
	reference(1,i) = msg->waypoints_list[i].pose.position.y;
	reference(2,i) = msg->waypoints_list[i].pose.position.z;

	reference(3,i) = goal_roll;
	reference(4,i) = goal_pitch;
	reference(5,i) = goal_yaw;
	
	wp_quat(0,i) = msg->waypoints_list[i].pose.orientation.x;
	wp_quat(1,i) = msg->waypoints_list[i].pose.orientation.y;
	wp_quat(2,i) = msg->waypoints_list[i].pose.orientation.z;
	wp_quat(3,i) = msg->waypoints_list[i].pose.orientation.w;

	
	
	}
	
	dexrov_msgs::Command ack_msg;
	ack_msg.ID = 6;
	ack_msg.command = 1;
	ack_publisher.publish(ack_msg);
	
	//	cout << reference << "\n\n";
	
	new_waypoints = true;
	flag_goal_pose = true;
	
}

//Function of callback of estimated Pose.
void MsgCallback_estimated(const geometry_msgs::PoseStamped msg)
{

//	ROS_INFO("received 2\n");
	float x,y,z,w;
	
	x = msg.pose.orientation.x;
	y = msg.pose.orientation.y;
	z = msg.pose.orientation.z;
	w = msg.pose.orientation.w;

/*
	x = msg.pose.pose.orientation.x;
	y = msg.pose.pose.orientation.y;
	z = msg.pose.pose.orientation.z;
	w = msg.pose.pose.orientation.w;
*/	
	conversionQuaternionToRPY(x, y, z, 
w);
	
	est_roll = temp_roll;
	est_pitch = temp_pitch;
	est_yaw = temp_yaw;

	p_hat(0) = msg.pose.position.x;
	p_hat(1) = msg.pose.position.y;
	p_hat(2) = msg.pose.position.z;
	
	/*
	p_hat(0) = msg.pose.pose.position.x;
	p_hat(1) = msg.pose.pose.position.y;
	p_hat(2) = msg.pose.pose.position.z;
*/
	phi_hat = est_roll;
	theta_hat = est_pitch;
	psi_hat = est_yaw;

	flag_estimated_pose = true;
	
	
	
	
}


//Callback function. AutoHeading 
void subscribeAutoHeading(const dexrov_msgs::AutoHeading& autoHeadingInfo)
{	
	flag_autoheading = autoHeadingInfo.switchSetting;
//	std::cout << "autoheading: " << flag_autoheading << endl;	
}

//Callback function. AutoDepth
void subscribeAutoDepth(const dexrov_msgs::AutoDepth& autoDepthInfo)
{	
	flag_autodepth = autoDepthInfo.switchSetting;	
//	std::cout << "autodepth: " << flag_autodepth << endl;
}

//Callback function. AutoAltitude
void subscribeAutoAltitude(const dexrov_msgs::AutoAltitude& autoAltitudeInfo)
{	
	flag_autoaltitude = autoAltitudeInfo.switchSetting;
//	std::cout << "autoaltitude: " << flag_autoaltitude << endl;	
}


//Method to compute errors
int computation_error()
{

	Vector3f e_I(3); //error in Inertial Frame
	float x_e_I = reference(0,current_wp) - p_hat(0,0); 
	float y_e_I = reference(1,current_wp) - p_hat(1,0); 
	float z_e_I = reference(2,current_wp) - p_hat(2,0); 	
	e_I << x_e_I, y_e_I, z_e_I;	
	
	e = R_Hat.transpose() * e_I;

	error = e;		
//	cout << "error =" << endl << e_I << endl << endl;	
	
	
	error_msg.header.stamp = ros::Time::now();
	error_msg.pose.position.x = e(0);
	error_msg.pose.position.y = e(1);
	error_msg.pose.position.z = e(2);
	//R_transpose = R_Hat.transpose(); 
	//rpy(R_transpose);

	psi_d = reference(5,current_wp);

	

	psi_tilde = psi_d - psi_hat;
	
	error_msg.pose.orientation.x = psi_tilde;
	
	
	 
//	cout << "psi tilde = " << endl << psi_tilde << endl;
}

//Saturation method
float sat (Vector3f x, float c)
{
	float value;
	
	if (x(2,0) == 0.0)
		value = 0.0;
	else if (x.norm()<c)
		value = 1.0;
	else
		value = c/(x.norm());	

	return value;
}

//Yaw Controller
int yawcontroller()
{
	compute_yaw_rate = true;
	
	if (fabs(psi_tilde)>M_PI)
		psi_tilde = psi_tilde-2*M_PI*copysign(1.0, psi_tilde);

	if(fabs(psi_tilde)< psi_tilde_stop)
		compute_yaw_rate = false;
	
	if (compute_yaw_rate)
	{
		if(fabs(psi_tilde) > psi_tilde_bar)
			kIpsi = 0.0;
		
		float K3 = kppsi + 0.5*(kIpsi*delta_T);
		float K4 = 0.5 * (kIpsi*delta_T) - kppsi;
		
		
		rnew = rold + K3*psi_tilde + K4*psi_tilde_old;
		omegaold << 0, 0, rnew;
		
		float satValue = sat(omegaold, r_max);
		rnew = rnew * satValue;
		omeganew_sat << 0, 0, rnew;
	}	
	else
	{
		omegaold << 0, 0, 0;
	}	

//	cout << "angular velocity: " << endl << omeganew_sat << endl;	
}

//Surge Controller
int surgecontroller()
{
	int c1;
	int c2;
	int c3;
	
	e(0,0) = error(0);
	c1 = 1;
	
	e(1,0) = error(1);
	c2 = 1;

	e(2,0) = error(2);
	c3 = 1;

	if (fabs(e(0,0)) < eu_bar)
		gamma1 = 0;
	if (fabs(e(1,0)) < ev_bar)
		gamma2 = 0;
	if (fabs(e(2,0)) < ew_bar)
		gamma3 = 0;

	Matrix3f DiagGamma(3,3);

	DiagGamma << 	gamma1, 0.0, 	0.0,
			0.0, 	gamma2, 0.0,
			0.0, 	0.0,	gamma3;
			
	KI = KI*DiagGamma;
	
	Matrix3f K1(3,3);
	Matrix3f K2(3,3);
	K1 = Kp + 0.5*(KI*delta_T);
	K2 = 0.5*(KI*delta_T) - Kp;

	unew = uold + K1*e + K2*e_old;
	
	Matrix3f DiagC(3,3);

	DiagC << 	c1,	0.0, 	0.0,
			0.0,	c2, 	0.0,
			0.0,	0.0,	c3;

	unew = DiagC * unew;
	
	if ((fabs(e(0,0)) < eu_stop) || error (0,0) == 0.0)
		unew (0,0) = 0.0;
	if ((fabs(e(1,0)) < ev_stop) || error (1,0) == 0.0)
		unew (1,0) = 0.0;
	if ((fabs(e(2,0)) < ew_stop) || error (2,0) == 0.0)
		unew (2,0) = 0.0;
	
	uold = unew;
	
	Vector3f tempVector0;
	tempVector0 << 0, 0, unew(0);
	float satValue0 = sat(tempVector0, u_max);
	unew_sat(0) = unew(0) * satValue0; 

	Vector3f tempVector1;
	tempVector1 << 0, 0, unew(1);
	float satValue1 = sat(tempVector1, v_max);
	unew_sat(1) = unew(1) * satValue1; 

	Vector3f tempVector2;
	tempVector2 << 0, 0, unew(2);
	float satValue2 = sat(tempVector2, w_max);
	unew_sat(2) = unew(2) * satValue2; 
//std::cout << "Reaching wayp: " << reference.col(current_wp) << "\n";

	if ((compute_yaw_rate == false) && (fabs(e(0,0)) < eu_stop) && (fabs(e(1,0)) < ev_stop) && (fabs(e(2,0)) < ew_stop)){
		goal_reached = true;
		
		
		dexrov_msgs::Waypoints reached_msg;
		
		reached_msg.waypoints_list.resize(1);
		
		reached_msg.waypoints_list[0].header.stamp = ros::Time::now();
		reached_msg.waypoints_list[0].header.frame_id = "odom";
		
		reached_msg.waypoints_list[0].pose.position.x = reference(0,current_wp);
		reached_msg.waypoints_list[0].pose.position.y = reference(1,current_wp);
		reached_msg.waypoints_list[0].pose.position.z = reference(2,current_wp);
		
		
		
		
		reached_msg.waypoints_list[0].pose.orientation.x = wp_quat(0,current_wp);
		reached_msg.waypoints_list[0].pose.orientation.y = wp_quat(1,current_wp);
		reached_msg.waypoints_list[0].pose.orientation.z = wp_quat(2,current_wp);
		reached_msg.waypoints_list[0].pose.orientation.w = wp_quat(3,current_wp);
		
		reached_publisher.publish(reached_msg);
		
		if(current_wp < reference.cols()-1)
		current_wp++;
		else{
		current_wp=0;
		new_waypoints = false;
		}
		
		
		
		}
	
//	cout << "linear velocity: " << endl << unew_sat << endl;	
}

//Control Law
int dexrov_ctrl()
{
	rold = omegaold(2,0);

	unew << 0,0,0;
	unew_sat << 0,0,0;
	omeganew << 0,0,0;	
	omeganew_sat << 0,0,0;
		
	e << 0,0,0;
	//goal_reached = 0;

	gamma1 = 1;
	gamma2 = 1;
	gamma3 = 1;
	
	Kp = gains_Kp;
	KI = gains_KI;
	kppsi = gains_kppsi;
	kIpsi = gains_kIpsi;
		
	compute_yaw_rate = false;
	
	
	yawcontroller();
	surgecontroller();
	
	
}

//Method to publish linear and angular velocity
int publishTwist()
{
	twistMsg.linear.x = unew_sat(0);
	twistMsg.linear.y = unew_sat(1);
	twistMsg.linear.z = unew_sat(2);


	twistMsg.angular.z = omeganew_sat(2);

	twist_publisher.publish(twistMsg);
}

//convert velocity to volts and write moving command for ROV thrusters on DAQ board
int writeDAC()
{

	publishTwist();
	
}

//obtain estimated rotate matrix 
void rotateMatrix()
{
	theta_hat = 0.0;
	phi_hat = 0.0;
	float cosphi = cos(psi_hat);
	float costheta = cos(theta_hat);
	float cospsi = cos(phi_hat);
	float sinphi = sin(psi_hat);
	float sintheta = sin(theta_hat);
	float sinpsi = sin(phi_hat);

	
	R_Hat <<	cosphi*costheta,	cosphi*sintheta*sinpsi - sinphi*cospsi, cosphi*sintheta*cospsi + sinphi*sinpsi,
			sinphi*costheta, 	sinphi*sintheta*sinpsi + cosphi*cospsi,	sinphi*sintheta*cospsi - cosphi*sinpsi,
			-sintheta,		costheta*sinpsi,			costheta*cospsi;
			
		
	
}


//Callback function. Receive Pose message
/*
void MsgCallback_acquired(const dexrov_vehicle_control::RovConvertedPose msgPose)
{
//	std::cout << "Ricevuti " << msgPose.position_x_des.size() << " waypoints\n";
	reference.resize(6, msgPose.position_x_des.size());
	
	for(int i=0;i<msgPose.position_x_des.size();i++){
	
	reference(0,i) = msgPose.position_x_des[i];
	reference(1,i) = msgPose.position_y_des[i];
	reference(2,i) = msgPose.position_z_des[i];
	reference(3,i) = msgPose.roll_des[i];
	reference(4,i) = msgPose.pitch_des[i];
	reference(5,i) = msgPose.yaw_des[i];
	
	}
	
	
	
	
	rotateMatrix();
	computation_error();
	dexrov_ctrl();
	
	writeDAC();
	
	
}
*/
int main (int argc, char **argv)
{
	ros::init(argc, argv, "control_law");
	ros::NodeHandle nh;
	
	ros::Rate loop_rate(1000);
	
	//natural frequencies
	omegan(0) = 1 * (1/(2*xi)) * (u_max/eu_bar);	
	omegan(1) = 1 * (1/(2*xi)) * (v_max/ev_bar);
	omegan(2) = 1 * (1/(2*xi)) * (w_max/ew_bar); 

	omegan_psi = 1 * r_max/(2*xi_psi*psi_tilde_bar);

	gains_Kp << 	xi*omegan(0), 	0.0, 		0.0,
			0.0, 		xi*omegan(1), 	0.0,
			0.0, 		0.0,		xi*omegan(2);

	gains_KI << 	omegan(0)*omegan(0),	0.0, 			0.0,
			0.0, 			omegan(1)*omegan(1), 	0.0,
			0.0, 			0.0,			omegan(2)*omegan(2);
			
	

	/*cout << "xi: " << endl << xi << endl;
	cout << "umax: " << endl << u_max << endl;
	cout << "eubar: " << endl << eu_bar << endl;
	cout << "omegan: " << endl << omegan(0) << endl;*/
	gains_kppsi = 2 * xi_psi * omegan_psi;
	gains_kIpsi = omegan_psi * omegan_psi;
	
	
	twist_publisher = nh.advertise<geometry_msgs::Twist>("/rov/cmd_vel", 1);
	reached_publisher = nh.advertise<dexrov_msgs::Waypoints>("/rov/waypoint_reached", 1);
	error_publisher = nh.advertise<geometry_msgs::PoseStamped>("/rov/error", 1);
	ack_publisher = nh.advertise<dexrov_msgs::Command>("/rov/command_ack",1);

//	acquired_pose_subscriber = nh.subscribe("acquired_pose", 1, MsgCallback_acquired); //Topic del messaggio ricevuto di Posa
	
	estimated_pose_subscriber = nh.subscribe("/rov/pose_high", 1, MsgCallback_estimated);
	desired_pose_subscriber = nh.subscribe("/mcc/waypoints", 1, MsgCallback);
	
	autoheading_subscriber = nh.subscribe("/rov/autohead_ros", 1, subscribeAutoHeading);
	autodepth_subscriber = nh.subscribe("/rov/autodepth_ros", 1, subscribeAutoDepth);
	autoaltitude_subscriber = nh.subscribe("/rov/autoalt_ros", 1, subscribeAutoAltitude);
	
	while(ros::ok()){
	if(flag_goal_pose && flag_estimated_pose && new_waypoints){
	
	rotateMatrix();
	
	if (flag_autoaltitude || flag_autodepth)
		reference(2,current_wp) = p_hat(2,0);
		
	if (flag_autoheading)
		reference(5,current_wp) = psi_hat;
		
	
	computation_error();
	dexrov_ctrl();
	writeDAC();
	
	}
	error_msg.header.stamp = ros::Time::now();
	error_publisher.publish(error_msg);	
	ros::spinOnce();
	loop_rate.sleep();
	
	}
	
}

