#include "ros/ros.h"
#include <nav_msgs/Odometry.h>
#include <gazebo_msgs/ModelStates.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_broadcaster.h>


 
geometry_msgs::PoseStamped pose;
geometry_msgs::Twist twist;

int flag = 0;

void callback(const gazebo_msgs::ModelStatesPtr& msg){

int id = -1;

for(int i=0;i<msg->name.size();i++){

	if(msg->name[i] == "dexrov_rov"){
		
		id = i;
		
	}

}


pose.pose = msg->pose[id];
twist = msg -> twist[id];

flag = 1;

}



int main(int argc, char **argv)
{

  ros::init(argc, argv, "pub_pose");

  ros::NodeHandle n;


  ros::Publisher pose_pub = n.advertise<geometry_msgs::PoseStamped>("/rov/pose", 1);
  ros::Publisher pose_pub2 = n.advertise<geometry_msgs::PoseStamped>("/rov/pose_high", 1);
  ros::Publisher twist_pub = n.advertise<geometry_msgs::Twist>("/rov/twist", 1);
  ros::Subscriber sub = n.subscribe("/gazebo/model_states", 1, callback);
  
  
  ros::Rate loop_rate(1000);


  geometry_msgs::PoseStamped msg;
  
  
  int counter = 0;
  
  while (ros::ok())
  {

   counter++;

   if(flag==1){
   
   	msg = pose;
   	msg.header.frame_id = "odom";
   	
   	if(counter == 100){
   	
   	pose_pub.publish(msg);
   	counter = 0;
   	
   	}
   	
   	static tf::TransformBroadcaster br;
  	tf::Transform transform, transform2;
  	
  	tf::Quaternion q(pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z, pose.pose.orientation.w);
  	tf::Quaternion q2(0.0, -0.7071, 0.7071, 0.0);
  	
  	
  	transform.setOrigin( tf::Vector3(pose.pose.position.x, pose.pose.position.y, pose.pose.position.z) );
  	transform.setRotation(q); 
  	
  	transform2.setRotation(q2);
  	transform2.setOrigin(tf::Vector3(0.95, 0.2, -0.5));
  	
  	br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "base_link"));
  	br.sendTransform(tf::StampedTransform(transform2, ros::Time::now(), "base_link", "arm_link1"));
  	
   	pose_pub2.publish(msg);
   	twist_pub.publish(twist);
   }
   
 
    
   

    ros::spinOnce();

    loop_rate.sleep();
   
  }


  return 0;
}
