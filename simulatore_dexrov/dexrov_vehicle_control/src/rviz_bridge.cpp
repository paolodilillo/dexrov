#include "ros/ros.h"
#include "dexrov_msgs/Waypoints.h"

int flag=0;
dexrov_msgs::Waypoints wayp_msg;

void waypCB(const geometry_msgs::PoseStamped::ConstPtr& msg){



  wayp_msg.waypoints_list.resize(1);

  wayp_msg.waypoints_list[0].pose.position.x = msg->pose.position.x;
  wayp_msg.waypoints_list[0].pose.position.y = msg->pose.position.y;
  wayp_msg.waypoints_list[0].pose.position.z = 4.0;
  
  wayp_msg.waypoints_list[0].pose.orientation.x = msg->pose.orientation.x;
  wayp_msg.waypoints_list[0].pose.orientation.y = msg->pose.orientation.y;
  wayp_msg.waypoints_list[0].pose.orientation.z = msg->pose.orientation.z;
  wayp_msg.waypoints_list[0].pose.orientation.w = msg->pose.orientation.w;


flag=1;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "rviz_rov_bridge");

  ros::NodeHandle n;


  ros::Publisher wayp_pub = n.advertise<dexrov_msgs::Waypoints>("/rov/waypoint", 1);
  ros::Subscriber sub = n.subscribe("/move_base_simple/goal", 1, waypCB);

  ros::Rate loop_rate(1);


  
  while (ros::ok())
  {

if(flag==1){


   wayp_pub.publish(wayp_msg);
   flag=0;
}

    ros::spinOnce();

    loop_rate.sleep();
   
  }


  return 0;
}
