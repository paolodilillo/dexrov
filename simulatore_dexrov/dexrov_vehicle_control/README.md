# Dexrov vehicle control package

## Description
This package allows to control the vehicle in the Gazebo simulator via ROS topic.

## Installation
In `dexrov_docker/dexrov_simulation/src` clone this repository

`git clone git@gitlab.spaceapplications.com:dexrov/dexrov_vehicle_control.git`

move to `dexrov_docker` and build the code:

`./build.sh`

## Usage

Open a terminal in the simulation container:

`./console.sh simulation`

run the package:

`roslaunch dexrov_vehicle_control rov_control.launch`

The launch file will run:

1. The `control_law` node
2. The `dexrov_Pose` node for getting the desired and current pose from topic
3. The `pose_publisher` node that reads the current pose from the `gazebo/model_states` topic and re-publishes it for the `dexrov_Pose` node. This node can be turned off when there will be a working navigation package.
4. The `rviz_bridge` node for setting a desired waypoint from Rviz

It is possible to send a desired pose for the vehicle publishing a `geometry_msgs/PoseStamped` on the `rov/waypoint` topic or via Rviz 2D Nav Goal tab.

![alt text](https://gitlab.spaceapplications.com/dexrov/dexrov_vehicle_control/raw/master/pics/screen_rviz.png)



