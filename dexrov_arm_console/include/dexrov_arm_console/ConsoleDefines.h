/*
 * Defines.h
 *
 *  Created on: May 10, 2018
 *      Author: fw
 */

#ifndef _DEXROV_ARM_CONSOLE_DEFINES_H_
#define _DEXROV_ARM_CONSOLE_DEFINES_H_

#include <string>
#include <map>

const int NumJoints = 6;


/**
 * Remember to change also the CommandNames!!!
 */
enum class CommandType
    : int16_t
    {
    	foldArm,   				 //!< unfoldArm
		unfoldArm,				 //!< foldArm
        jointPos,                //!< jointPos
        jointVel,                //!< jointVel
        cartPos,                 //!< cartPos
        cartRelPos,              //!< cartRelPos
        cartVel,                 //!< cartVel
        stopArm,                 //!< stopArm
        openGrip,                //!< openGrip
        gripPlane,               //!< gripPlane
        enumSize                 //!< enumSize
};

/// Short command names version for terminal input
const std::map<CommandType, std::string> commandNamesShort =
        {
          { CommandType::foldArm, "foldarm" },
		  { CommandType::unfoldArm, "unfoldarm" },
          { CommandType::jointPos, "jointpos" },
          { CommandType::jointVel, "jointvel" },
          { CommandType::cartPos, "cartpos" },
          { CommandType::cartRelPos, "cartrelpos" },
          { CommandType::cartVel, "cartvel" },
          { CommandType::stopArm, "stop" },
          { CommandType::openGrip, "opengrip" },
          { CommandType::gripPlane, "gripplane" },
        };



#endif /* DEXROV_ARM_CONSOLE_INCLUDE_DEXROV_ARM_CONSOLE_DEFINES_H_ */
