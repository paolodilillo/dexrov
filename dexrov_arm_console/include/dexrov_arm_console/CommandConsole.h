/*
 * command_console.h
 *
 *  Created on: Mar 3, 2016
 *     Author: Francesco Wanderlingh
 *      		GRAAL Lab (DIBRIS)
 *      		Università Degli Studi di Genova
 */

#ifndef COMMAND_CONSOLE_H_
#define COMMAND_CONSOLE_H_

#include <iostream>
#include <vector>
#include <eigen3/Eigen/Dense>
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "dexrov_gripper_ctrl/GripperCommand.h"
#include "dexrov_gripper_ctrl/GripperState.h"
#include "Futils.h"
#include "ConsoleDefines.h"

using std::cout;
using std::endl;

//std::map<std::string, int> armStr2Int = { { "L", 0 }, { "R", 1 } };
//std::map<int, std::string> armInt2Str = { { 0, "L" }, { 1, "R" } };

class CommandConsole
{

public:
	CommandConsole(int rate);
	virtual ~CommandConsole();

	enum class ParsRet
	{
		Ok, CmdNotRecognized, InvalidArm, InvalidInputVals
	};

	const std::map<ParsRet, std::string> ParsRetNames = {
	        { ParsRet::Ok, "Ok" },
	        { ParsRet::InvalidArm, "Invalid Arm" },
	        { ParsRet::CmdNotRecognized, "Command Not Recognized" },
			{ ParsRet::InvalidInputVals, "Invalid Input Values" }};

	void PrintCommandList();
	ParsRet ParseInput(const std::string input);//, CommandType &cmd, std::vector<double> &values);
	bool ValuesAreValid(const CommandType &cmd, const std::vector<double> &values);
	void ExecuteCommandLoop();
	void SendCommand(const CommandType cmd, const std::vector<double> &cmdValues,
	        const ros::Publisher& pub);

private:
	void EEPoseCB(const geometry_msgs::PoseStamped::ConstPtr& msg);
	geometry_msgs::PoseStamped XYZRPY2Pose(const std::vector<double>& xyzrpy);
	void RegisterPubsAndSubs(const std::string& arm);


	ros::NodeHandlePtr n;
	ros::Subscriber ee_position_sub;
	ros::Publisher req_ee_pose_pub;
	ros::Publisher gripper_command_pub;
	bool feedbackValid = {false};
	geometry_msgs::PoseStamped feedback, sampledInitialPose;


	ros::Rate r;//(rate);
	std::string armStr;
	std::vector<double> cmdValues;
	ParsRet parsRetVal;
	CommandType cmdRead;

	futils::Dotter myDotter;//(4);
	futils::Spinner mySpinner;//(7);
	futils::Timer timeoutTimer;
	double timeout = {5.0};
	bool timeoutReached = {false};
	bool justOnce = {false};
};

#endif /* CONTROL_BAXTER_INCLUDE_COMMAND_CONSOLE_H_ */
