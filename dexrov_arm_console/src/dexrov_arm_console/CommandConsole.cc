/*
 * command_console.cc
 *
 *  Created on: Mar 3, 2016
 *     Author: Francesco Wanderlingh
 *      		GRAAL Lab (DIBRIS)
 *      		Università Degli Studi di Genova
 */

#include <cstdlib>
#include <cstdio>
#include <sstream>
#include <iterator>

#include "dexrov_arm_console/CommandConsole.h"

CommandConsole::CommandConsole(int rate) :
		r(rate), myDotter(5), mySpinner(5)
{

	n = ros::NodeHandlePtr(new ros::NodeHandle);
	cmdRead = CommandType::enumSize;
	parsRetVal = ParsRet::CmdNotRecognized;

}

CommandConsole::~CommandConsole()
{
	// TODO Auto-generated destructor stub
}

void CommandConsole::RegisterPubsAndSubs(const std::string& arm)
{

	std::string topic_ee_pose_hf, topic_req_ee_pose, topic_gripper_state, topic_gripper_cmd;

	topic_ee_pose_hf = std::string("/arm/") + arm + ("/end_effector_pose_hf");
	topic_req_ee_pose = std::string("/cog_proxy/") + arm + ("/desired_end_effector_pose");
	topic_gripper_state = std::string("/gripper/") + arm + ("/state");
	topic_gripper_cmd = std::string("/gripper/") + arm + ("/command");

	ee_position_sub = n->subscribe(topic_ee_pose_hf, 1, &CommandConsole::EEPoseCB, this);
	req_ee_pose_pub = n->advertise<geometry_msgs::PoseStamped>(topic_req_ee_pose, 1, true);
	gripper_command_pub = n->advertise<dexrov_gripper_ctrl::GripperCommand>(topic_gripper_cmd, 1,
	        true);
}

void CommandConsole::PrintCommandList()
{
	std::string jCmdStr = "< arm j1 ... jn > ";
	std::string cartCmdStr = "< arm x y z r p y >";
	std::string ctrlModeStr = "< arm ['vel'|'torque'] >";
	std::string holdModeStr = "< arm [0(disable)|1(enable)] >";
	std::string armIndStr = "< arm >";
	std::string notImplStr = "*not implemented yet*";
	std::string mobCartVelStr = "< arm ['world'|'tool'] >";

	cout << tc::bluL << endl;
	cout << "Command List:" << " arm=['L'|'R']" << " (ArmJoints=" << NumJoints << ")" << endl;
	cout << tc::blu << "------------------------------" << tc::bluL << endl;
	//cout << commandNamesShort.at(CommandType::foldArm) << "\t\t\t" << armIndStr << endl;
	cout << commandNamesShort.at(CommandType::unfoldArm) << "\t" << armIndStr << endl;
	//cout << commandNamesShort.at(CommandType::jointPos)  << "\t\t" << jCmdStr << endl;
	//cout << commandNamesShort.at(CommandType::jointVel)  << "\t\t" << jCmdStr << endl;
	cout << commandNamesShort.at(CommandType::stopArm) << "\t\t" << armIndStr << endl;
	cout << commandNamesShort.at(CommandType::cartPos) << "\t\t" << cartCmdStr << endl;
	cout << commandNamesShort.at(CommandType::cartRelPos) << "\t" << cartCmdStr << endl;
	//cout << commandNamesShort.at(CommandType::cartVel) 		<< "\t\t\t" << cartCmdStr << endl;
	cout << commandNamesShort.at(CommandType::openGrip) << "\t" << armIndStr << endl;
	cout << commandNamesShort.at(CommandType::gripPlane) << "\t" << armIndStr << endl;
	cout << tc::none << endl;
}

CommandConsole::ParsRet CommandConsole::ParseInput(const std::string input)	//, CommandType &cmd,
//std::vector<double> &values)
{
	bool keyFound;
	std::string cmdString, valuesString;
	cmdValues.clear();

	/**
	 * Here we find the first space which identifies the end of command string and split the string
	 */
	std::string::size_type t1 = input.find_first_of(" ");
	cmdString = input.substr(0, t1);
	std::map<CommandType, std::string>::const_iterator it;
	int key = -1;

	if (!futils::FindMapKeyByValue(commandNamesShort, cmdString, cmdRead))
		return ParsRet::CmdNotRecognized;

	/**
	 * Here we find the second space which identifies the end of arm string and split the string
	 */
	valuesString = input.substr(t1 + 1, input.size());
	std::string::size_type t2 = valuesString.find_first_of(" ");
	armStr = valuesString.substr(0, t2);
	std::string::size_type t3;

	if (!(armStr == "L") && !(armStr == "R")) {
		return ParsRet::InvalidArm;
	}
	//arm = armStr2Int.at(armStr);
	RegisterPubsAndSubs(armStr);

	std::stringstream valss;

	switch (cmdRead) {
	case CommandType::jointPos:
	case CommandType::jointVel:
	case CommandType::cartRelPos:
	case CommandType::cartPos:
	case CommandType::cartVel:

		/**
		 * Here we use the rest of the string its read values (cartesian or joints values)
		 */
		valuesString = valuesString.substr(t2 + 1, valuesString.size());
		valss.str(valuesString);

		std::copy(std::istream_iterator<double>(valss), std::istream_iterator<double>(),
		        std::back_inserter(cmdValues));
		if (!ValuesAreValid(cmdRead, cmdValues)) {
			return ParsRet::InvalidInputVals;
		}
		break;
	default:
		break;
	}
	return ParsRet::Ok;
}

bool CommandConsole::ValuesAreValid(const CommandType &cmd, const std::vector<double> &values)
{
	int valSize = values.size();
	switch (cmd) {
	case CommandType::jointPos:
	case CommandType::jointVel:
		/**
		 * Size of values must match the number of robot joints
		 */
		if (values.size() != NumJoints)
			return false;
		break;
	case CommandType::cartPos:
	case CommandType::cartRelPos:
	case CommandType::cartVel:
		/**
		 * Size of values must match 6 parameters representation [yprxyz]
		 */
		if (values.size() != 6)
			return false;
		break;
	default:
		return false;
		break;
	}

	return true;
}

void CommandConsole::EEPoseCB(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
	feedback.pose.position.x = msg->pose.position.x;
	feedback.pose.position.y = msg->pose.position.y;
	feedback.pose.position.z = msg->pose.position.z;
	feedback.pose.orientation.x = msg->pose.orientation.x;
	feedback.pose.orientation.y = msg->pose.orientation.y;
	feedback.pose.orientation.z = msg->pose.orientation.z;
	feedback.pose.orientation.w = msg->pose.orientation.w;

	feedbackValid = true;
}

void CommandConsole::SendCommand(const CommandType cmd, const std::vector<double>& cmdValues,
        const ros::Publisher& publisher)
{
	geometry_msgs::PoseStamped pos;
	dexrov_gripper_ctrl::GripperCommand gc;
	Eigen::Quaterniond fbk_quat, drot_quat, result_quat;

	switch (cmd) {
//    	case CommandType::foldArm:
//            //ctrlInterface_.FoldArm(arm);
//            break;
	case CommandType::unfoldArm:
		pos.pose.position.x = 0.907317563932;
		pos.pose.position.y = 0.0;
		pos.pose.position.z = -0.606489803527;
		pos.pose.orientation.x = 0.499800878662;
		pos.pose.orientation.y = 0.500199042049;
		pos.pose.orientation.z = 0.499800878684;
		pos.pose.orientation.w = 0.500199042071;
		publisher.publish(pos);
		break;
//        case CommandType::jointPos:
//            std::copy(cmdValues.begin(), cmdValues.end(), jointValsArr);
//            //ctrlInterface_.SetJointsPosition(jointValsArr, arm);
//            break;
//        case CommandType::jointVel:
//            std::copy(cmdValues.begin(), cmdValues.end(), jointValsArr);
//            //ctrlInterface_.SetJointVelocity(jointValsArr, arm);
//            break;
	case CommandType::cartPos:
		pos = feedback;
		pos = XYZRPY2Pose(cmdValues);
		publisher.publish(pos);
		break;
	case CommandType::cartRelPos:
		pos = XYZRPY2Pose(cmdValues);
		pos.pose.position.x = sampledInitialPose.pose.position.x + pos.pose.position.x;
		pos.pose.position.y = sampledInitialPose.pose.position.y + pos.pose.position.y;
		pos.pose.position.z = sampledInitialPose.pose.position.z + pos.pose.position.z;

		fbk_quat.x() = sampledInitialPose.pose.orientation.x;
		fbk_quat.y() = sampledInitialPose.pose.orientation.y;
		fbk_quat.z() = sampledInitialPose.pose.orientation.z;
		fbk_quat.w() = sampledInitialPose.pose.orientation.w;
		drot_quat.x() = pos.pose.orientation.x;
		drot_quat.y() = pos.pose.orientation.y;
		drot_quat.z() = pos.pose.orientation.z;
		drot_quat.w() = pos.pose.orientation.w;
		result_quat = drot_quat * fbk_quat;
		pos.pose.orientation.x = result_quat.x();
		pos.pose.orientation.y = result_quat.y();
		pos.pose.orientation.z = result_quat.z();
		pos.pose.orientation.w = result_quat.w();
		publisher.publish(pos);
		break;
	case CommandType::stopArm:
		publisher.publish(feedback);
		break;
//        case CommandType::cartVel:

//            break;
	case CommandType::openGrip:
		gc.ID = dexrov_gripper_ctrl::GripperCommand::Open;
		publisher.publish(gc);
		break;
	case CommandType::gripPlane:
		gc.ID = dexrov_gripper_ctrl::GripperCommand::Grip;
		gc.grip_shape = dexrov_gripper_ctrl::GripperCommand::Plane;
		publisher.publish(gc);
		break;
	}
}

geometry_msgs::PoseStamped CommandConsole::XYZRPY2Pose(const std::vector<double>& xyzrpy)
{
	geometry_msgs::PoseStamped pose;
	pose.pose.position.x = xyzrpy[0];
	pose.pose.position.y = xyzrpy[1];
	pose.pose.position.z = xyzrpy[2];

	Eigen::Matrix3d n;
	n = Eigen::AngleAxisd(xyzrpy[5], Eigen::Vector3d::UnitZ())
	        * Eigen::AngleAxisd(xyzrpy[4], Eigen::Vector3d::UnitY())
	        * Eigen::AngleAxisd(xyzrpy[3], Eigen::Vector3d::UnitX());
	auto quat = Eigen::Quaterniond(n);
	pose.pose.orientation.x = quat.x();
	pose.pose.orientation.y = quat.y();
	pose.pose.orientation.z = quat.z();
	pose.pose.orientation.w = quat.w();

	return pose;
}

void CommandConsole::ExecuteCommandLoop()
{
	cout << tc::greenL << "Command Read: '" << commandNamesShort.at(cmdRead);
	cout << "', arm: '" << armStr << "', values: [";
	futils::PrintSTLVector(cmdValues, ',');
	cout << " ]" << tc::none << endl;

	if (cmdRead != CommandType::openGrip && cmdRead != CommandType::gripPlane) {

		std::cout << "Waiting for feedback for " << timeout << " seconds..." << std::endl;

		timeoutTimer.Start();
		while (n->ok() && !timeoutReached && !feedbackValid) {
			ros::spinOnce();

			if (timeoutTimer.Elapsed() > timeout && !feedbackValid) {

				timeoutReached = true;
				std::cout << tc::red << "Error: Timeout Reached while waiting for feedback"
				        << tc::none << std::endl;
			}

			if (feedbackValid) {
				sampledInitialPose = feedback;
				std::cout << "Feedback Received!" << std::endl;
			}
			myDotter();
			r.sleep();
		}

		while (n->ok() && feedbackValid) {

			SendCommand(cmdRead, cmdValues, req_ee_pose_pub);
			mySpinner();
			ros::spinOnce();
			r.sleep();
		}
	} else {
		SendCommand(cmdRead, cmdValues, gripper_command_pub);
		r.sleep();
		std::cout << tc::yellow << "Command Sent!" << tc::none << std::endl;

	}
}
