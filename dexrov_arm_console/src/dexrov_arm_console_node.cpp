/*
 * main_console.cpp
 *
 *  Created on: May 10, 2018
 *      Author: fw
 */

#include <thread>

#include "dexrov_arm_console/CommandConsole.h"
#include "Futils.h"

int main(int argc, char *argv[])
{
	ros::init(argc, argv, "dexrov_arm_console");
	ros::NodeHandle n;

	int rate;
	ros::NodeHandle pvt_n("~");
	pvt_n.param("rate", rate, int(5));

	std::string input;
	CommandConsole console(rate);


	/// Copy all arguments into a string, except from executable name
	for (int i = 1; i < argc; i++) {
		input = input + argv[i];
		if (i < argc - 1)
			input = input + " ";
	}

	CommandConsole::ParsRet parsRetVal = console.ParseInput(input);

	if (parsRetVal == CommandConsole::ParsRet::Ok) {

		console.ExecuteCommandLoop();

	} else if (input.find("help") != std::string::npos) {
		console.PrintCommandList();
	} else {
		std::cerr << tc::red << "Error Parsing '" << input << "': "
		        << console.ParsRetNames.at(parsRetVal) << tc::none << std::endl;
		std::cerr << tc::bluL << "For help type: help" << tc::none << endl;
	}
	ros::shutdown();
	return 0;
}




