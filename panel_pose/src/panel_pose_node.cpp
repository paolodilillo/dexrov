#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <gazebo_msgs/LinkStates.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf/exceptions.h>

using namespace std;


double x,y,z;
bool init = false;
double panel_x, panel_y, panel_z;



void dummyCB(const gazebo_msgs::LinkStates msg){



int id = -1;
int id2 = -1;

  static tf::TransformBroadcaster br;
  tf::Transform transform;
  

for(int i=0;i<msg.name.size();i++){

	transform.setOrigin( tf::Vector3(msg.pose[i].position.x, msg.pose[i].position.y, msg.pose[i].position.z) );
  	tf::Quaternion q(msg.pose[i].orientation.x, msg.pose[i].orientation.y, msg.pose[i].orientation.z, msg.pose[i].orientation.w);
  	transform.setRotation(q);
  	br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", msg.name[i]));
  
	if(msg.name[i] == "dexrov_panel::link_rotating_turn_c3"){
		
		id = i;
		
	}

	if(msg.name[i] == "dexrov_rov::arm_link7"){
		
		id2 = i;
		
	}

}

x = msg.pose[id].position.x;
y = msg.pose[id].position.y;
z = msg.pose[id].position.z;

//cout << "Posa ee: " << msg.pose[id2].position.x << "\t" << msg.pose[id2].position.y << "\t" << msg.pose[id2].position.z << "\n";

for(int i=0;i<msg.name.size();i++){


	if(msg.name[i] == "dexrov_panel::panel_base_link"){
		
		id = i;
		
	}

}


panel_x = msg.pose[id].position.x;
panel_y = msg.pose[id].position.y;
panel_z = msg.pose[id].position.z;

if(id != -1)

	init = true;
	


}

int main(int argc, char* argv[]){

ros::init(argc, argv, "dummy");
ros::NodeHandle n;
ros::Rate loop_rate(100);

ros::Publisher dummy_pub = n.advertise<geometry_msgs::PoseStamped>("/cog_proxy/L/desired_end_effector_pose", 1);
ros::Publisher panel_pub = n.advertise<geometry_msgs::PoseStamped>("/panel_pose", 1);
ros::Subscriber dummy_sub = n.subscribe("/gazebo/link_states",1, dummyCB);

geometry_msgs::PoseStamped msg;
geometry_msgs::PoseStamped msgpanel;

while(ros::ok()){

if(init){

msg.pose.position.x = x -0.35;
msg.pose.position.y = y;
msg.pose.position.z = z;

msg.pose.orientation.x = 0;
msg.pose.orientation.y = 0.7071;
msg.pose.orientation.z = 0;
msg.pose.orientation.w = 0.7071;

msgpanel.pose.position.x = panel_x;
msgpanel.pose.position.y = panel_y;
msgpanel.pose.position.z = panel_z;


panel_pub.publish(msgpanel);
//dummy_pub.publish(msg);

}


ros::spinOnce();
loop_rate.sleep();

}



}
