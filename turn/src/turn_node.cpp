#include "ros/ros.h"
#include "std_msgs/String.h"
#include <geometry_msgs/PoseStamped.h>
#include <Eigen/Core>
#include <ExtendedTaskHierarchy.h>

using namespace std;
using namespace Eigen;

VectorXd pose(7);
bool init = false;

void chatterCallback(const geometry_msgs::PoseStamped msg)
{
  
  if(!init){
  pose(0) = msg.pose.position.x;
  pose(1) = msg.pose.position.y;
  pose(2) = msg.pose.position.z;
  pose(3) = msg.pose.orientation.x;
  pose(4) = msg.pose.orientation.y;
  pose(5) = msg.pose.orientation.z;
  pose(6) = msg.pose.orientation.w;
  init=true;
  }
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "turn");

  ros::NodeHandle n;

  ros::Publisher chatter_pub = n.advertise<geometry_msgs::PoseStamped>("/cog_proxy/L/desired_end_effector_pose", 1);
  ros::Subscriber sub = n.subscribe("/arm/L/end_effector_pose_hf", 1, chatterCallback);
  ros::Rate loop_rate(100);

  geometry_msgs::PoseStamped msg;
  double x,y,z,w, dx,dy,dz,dw,ddx,ddy,ddz,ddw;
  double t = 0.0;
  double ts = 0.01;
  
  VectorXd rpy(3);
  
  while (ros::ok())
  {

    if(init){
  	
//  	trapezoidal(pose(4), -0.5, 20, t,x,dx,ddx);
//  	trapezoidal(pose(6), -0.5, 20, t,y,dy,ddy);
  	trapezoidal(pose(2), pose(2)+1.5, 10, t,z,dz,ddz);
  	
  	msg.pose.position.x = pose(0);
  	msg.pose.position.y = pose(1);
  	msg.pose.position.z = z;

  	msg.pose.orientation.x = pose(3);
  	msg.pose.orientation.y = pose(4);
  	msg.pose.orientation.z = pose(5);
  	msg.pose.orientation.w = pose(6);
    	chatter_pub.publish(msg);
    	t += ts;
    
    }   
  
    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
