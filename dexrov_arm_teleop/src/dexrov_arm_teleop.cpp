#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/JointState.h>
#include <ExtendedTaskHierarchy.h>

#define POSITION 1
#define ORIENTATION 2

using namespace std;
using namespace Eigen;

Vector3d angles;

double x, y, z;
double dx, dy, dz, dalfa, dbeta, dgamma;
Matrix3d R;
VectorXd qGazebo(6), q(6);

int state = POSITION;

bool pose_init = false;
bool joy_init = false;


void joyCB(const sensor_msgs::Joy msg){

if(msg.buttons[4] == 1){

	state = ORIENTATION;
	cout << "Controlling orientation\n";

}

else if (msg.buttons[5] == 1){


	state = POSITION;
	cout << "Controlling position\n";

}

if(state == POSITION){

	dx = msg.axes[1] * 0.2;
	dy = msg.axes[0] * 0.2;
	dz = msg.axes[4] * 0.2;
	
	
	dalfa = 0.0;
	dbeta = 0.0;
	dgamma = 0.0;

}


else if (state == ORIENTATION){


	dalfa = msg.axes[1] * 0.7;
	dbeta = msg.axes[0] * 0.7;
	dgamma = msg.axes[4] * 0.7;

	dx = 0.0;
	dy = 0.0;
	dz = 0.0;
}


joy_init = true;

}

void poseCB(const geometry_msgs::PoseStamped msg){

VectorXd quat(4);


if(!pose_init){

x = msg.pose.position.x;
y = msg.pose.position.y;
z = msg.pose.position.z;


quat(0) = msg.pose.orientation.x;
quat(1) = msg.pose.orientation.y;
quat(2) = msg.pose.orientation.z;
quat(3) = msg.pose.orientation.w;

R = quat2rot(quat);


pose_init = true;

}


else{


	if(state == POSITION){
	
		x = msg.pose.position.x;
		y = msg.pose.position.y;
		z = msg.pose.position.z;
	
	}
	
	else if(state == ORIENTATION){
	
	
		quat(0) = msg.pose.orientation.x;
		quat(1) = msg.pose.orientation.y;
		quat(2) = msg.pose.orientation.z;
		quat(3) = msg.pose.orientation.w;

		R = quat2rot(quat);
	
	}


}


}

/*
void jointCB(const sensor_msgs::JointState::ConstPtr& msg){

/*

Callback for acquiring the current joint positions



qGazebo(0) = msg->position[0];
qGazebo(1) = msg->position[1];
qGazebo(2) = msg->position[2];
qGazebo(3) = msg->position[3];
qGazebo(4) = msg->position[4];
qGazebo(5) = msg->position[5];


qGazebo(0) = msg->position[0];
qGazebo(1) = msg->position[2];
qGazebo(2) = msg->position[4];
qGazebo(3) = msg->position[6];
qGazebo(4) = msg->position[8];
qGazebo(5) = msg->position[10];



q = mask_Gazebo2DH_sim(qGazebo, qGazebo.size());

MatrixXd T(4,4);


q_initialized = true;



}

*/


int main(int argc, char* argv[]){

ros::init(argc, argv, "dexrov_arm_teleop");
ros::NodeHandle n;
ros::Rate loop_rate(100);

ros::Publisher pose_pub = n.advertise<geometry_msgs::PoseStamped>("/cog_proxy/L/desired_end_effector_pose", 1);
//ros::Publisher pose_pub = n.advertise<geometry_msgs::PoseStamped>("/desired_position", 1);

ros::Subscriber joy_sub = n.subscribe("/joy",1, joyCB);
ros::Subscriber pose_sub = n.subscribe("/arm/L/end_effector_pose_hf",1, poseCB);
//ros::Subscriber pose_sub = n.subscribe("/ee_position",1, poseCB);

geometry_msgs::PoseStamped posemsg;

VectorXd quat_new(4);

Matrix3d Rz, Ry, Rx, R_new;

Rx.setIdentity();
Ry.setIdentity();
Rz.setIdentity();

while(ros::ok()){

if  (pose_init && joy_init){
posemsg.header.stamp = ros::Time::now();

posemsg.pose.position.x = x + dx;
posemsg.pose.position.y = y + dy;
posemsg.pose.position.z = z + dz;

Rz << cos(dalfa), -sin(dalfa), 0,
       sin(dalfa), cos(dalfa),0,
       0,0,1;
       
Ry << cos(dbeta), 0, sin(dbeta),
       0, 1, 0,
       -sin(dbeta), 0, cos(dbeta);
       
Rx << 1, 0, 0,
       0, cos(dgamma), -sin(dgamma),
       0, sin(dgamma), cos(dgamma);

R_new = R * Rz * Ry * Rx;

quat_new = rot2quat(R_new);

posemsg.pose.orientation.x = quat_new(0);
posemsg.pose.orientation.y = quat_new(1);
posemsg.pose.orientation.z = quat_new(2);
posemsg.pose.orientation.w = quat_new(3);

/*
posemsg.pose.orientation.x = -0.5;
posemsg.pose.orientation.y = 0.5;
posemsg.pose.orientation.z = -0.5;
posemsg.pose.orientation.w = 0.5;
*/

/*
posemsg.pose.orientation.x = 0;
posemsg.pose.orientation.y = 0.9239;
posemsg.pose.orientation.z = 0;
posemsg.pose.orientation.w = 0.3827;
*/

pose_pub.publish(posemsg);

}

ros::spinOnce();
loop_rate.sleep();

}



}
