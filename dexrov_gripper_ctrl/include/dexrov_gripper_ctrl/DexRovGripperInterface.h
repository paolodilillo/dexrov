/*
 * GripperInterface.h
 *
 *  Created on: Apr 3, 2018
 *      Author: fw
 */

#ifndef _GRIPPERINTERFACE_H_
#define _GRIPPERINTERFACE_H_

#include <string>
#include <unordered_map>

// ROS includes.
#include "ros/ros.h"
#include "ros/time.h"
#include "dexrov_msgs/GripperJointState.h"
#include "dexrov_msgs/GripperJointPosReq.h"
#include "dexrov_gripper_ctrl/GripperCommand.h"
#include "dexrov_gripper_ctrl/GripperState.h"
#include "Futils.h"

// Dynamic reconfigure includes.
#include <dynamic_reconfigure/server.h>
// Auto-generated from cfg/ directory.
#include "dexrov_gripper_ctrl/gripper_ctrl_paramsConfig.h"

using namespace dexrov_gripper_ctrl;

class DexRovGripperInterface {

	typedef std::map<int, std::pair<int,int>> FingerMapType;
public:

	//enum class GripperState { Idle, Open, Closed, Opening, Closing, ReachingGrip, Gripping };
	const std::unordered_map<GripperState::_ID_type, const char*> GripperState2String =
	{{GripperState::Idle, "Idle"},
     {GripperState::Open, "Open"},
	 {GripperState::Closed, "Closed"},
	 {GripperState::Opening, "Opening"},
	 {GripperState::Closing, "Closing"},
	 {GripperState::ReachingGrip, "ReachingGrip"},
	 {GripperState::Gripping, "Gripping"}};

	const std::unordered_map<GripperState::_ID_type, const char*> GripperCommand2String =
	{{GripperCommand::Stop, "Stop"},
	 {GripperCommand::Open, "Open"},
	 {GripperCommand::Close, "Close"},
	 {GripperCommand::Grip, "Grip"}};

	const std::unordered_map<GripperState::_ID_type, const char*> GripperCommandShape2String =
	{{GripperCommand::Cylinder, "Cylinder"},
	 {GripperCommand::Plane, "Plane"}};

	const FingerMapType FingersMap =
	{{0, std::make_pair(1,2)},
	 {1, std::make_pair(3,4)},
	 {2, std::make_pair(5,6)}};

    std::vector<double> MinJointRange_;// = 0.0 * M_PI / 180.0;
	//std::vector<double> MaxJointRangeLower_;// = 65.0 * M_PI / 180.0;
	std::vector<double> MaxJointRange_;// = 0.0 * M_PI / 180.0;
	//std::vector<double> MaxJointRangeUpper_;// = 110.0 * M_PI / 180.0;


	static const int NumFingers = 3;
	static const int NumJoints = 7;
	static const int NumTouchSensors = 4;
	static const int PalmIndex = 3;

	const double velocityTimeGain = 0.15;
	const double positionCtrlGain = 0.4;

	const double MaxConsecutiveOverCurrentLoops = 10;
	const double MaxConsecutiveOverForceLoops = 10;

	static constexpr double J0Center = 55.0 * M_PI / 180.0;

	DexRovGripperInterface();
	virtual ~DexRovGripperInterface();

	bool CheckFinger(int finger);

	//! Publish the message.
	void PublishControl();

	void PublishState();

	//! Callback function for subscriber.
	void FeedbackCallback(const dexrov_msgs::GripperJointState::ConstPtr &msg);

	void CommandCallback(const dexrov_gripper_ctrl::GripperCommand::ConstPtr &msg);

	//!Callback function for dynamic reconfigure server.
	void ConfigCallback(dexrov_gripper_ctrl::gripper_ctrl_paramsConfig &config, uint32_t level);

	void CheckForCommand();

	bool IsIdle();

	void ComputeControl();

	void Stop();

	void StartGrip();

	void OpenFinger(int finger);

	void CloseFinger(int finger);

	void OpenHand();

	void CloseHand();

	double GetFingerForce(int finger);

	double GetPalmForce();

	bool IsPosErrorBelowThresh();

	void PositionControl();

	void GripShapeControl();

	uint8_t GetState() const
	{
		return state_.ID;
	}

	const dexrov_msgs::GripperJointState& GetFeedback() const
	{
		return fbk_;
	}

	const dexrov_msgs::GripperJointPosReq& GetControl() const
	{
		return ctrl_;
	}

	int GetGrippingPhase() const
	{
		return grippingPhase_;
	}

private:
	ros::NodeHandlePtr nh_;
	ros::Subscriber subHandState_;
	ros::Subscriber subGripCmd_;
	ros::Publisher pubHandCtrl_;
	ros::Publisher pubGripState_;
	dexrov_msgs::GripperJointState fbk_;
	dexrov_msgs::GripperJointState initial_;
	dexrov_msgs::GripperJointPosReq ctrl_;
	dexrov_msgs::GripperJointPosReq temp_ctrl_;
	GripperCommand command_;
	GripperState state_;
	bool checkForIncomingCommand_ = {false};
	std::vector<double> pos_error_;
	//std::string message_ = {""};
	double positionErrorThreshold_ = {0.0};
	double currentTouchThreshold_ = {0.0};
	double forceTouchThreshold_ = {0.0};
	int grippingPhase_ = {0};
	std::vector<int> overCurrentCounter_, overForceCounter_;
	futils::Timer rampTimer_, gripTimer_;

	std::vector<double> JLowerAngleOffset_, JUpperAngleOffset_;


	double PressingAngleOffset;
};

#endif /* _GRIPPERINTERFACE_H_ */
