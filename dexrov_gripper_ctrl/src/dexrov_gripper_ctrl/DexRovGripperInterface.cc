/*
 * GripperInterface.cc
 *
 *  Created on: Apr 3, 2018
 *      Author: fw
 */

#include <algorithm>

#include "DexRovGripperInterface.h"
#include "Futils.h"

inline double l2_norm(std::vector<double> const& u)
{
	double accum = 0.;
	for (int i = 0; i < u.size(); ++i) {
		accum += u[i] * u[i];
	}
	return sqrt(accum);
}

DexRovGripperInterface::DexRovGripperInterface()
{
	std::string topic_ctrl, topic_fbk, topic_cmd, topic_state;

	nh_ = ros::NodeHandlePtr(new ros::NodeHandle);

	while (!nh_->ok()) {}

	nh_->param("topic_joint_state", topic_fbk, std::string("/gripper/L/joint_state"));
	nh_->param("topic_joint_control", topic_ctrl, std::string("/gripper/L/req_joint_position"));
	nh_->param("topic_gripper_state", topic_state, std::string("/gripper/L/state"));
	nh_->param("topic_gripper_command", topic_cmd, std::string("/gripper/L/command"));

	pubHandCtrl_ = nh_->advertise<dexrov_msgs::GripperJointPosReq>(topic_ctrl.c_str(), 10);
	subHandState_ = nh_->subscribe(topic_fbk.c_str(), 1, &DexRovGripperInterface::FeedbackCallback,
	        this);

	subGripCmd_ = nh_->subscribe(topic_cmd.c_str(), 1, &DexRovGripperInterface::CommandCallback,
	        this);
	pubGripState_ = nh_->advertise<dexrov_gripper_ctrl::GripperState>(topic_state.c_str(), 10);

	state_.ID = GripperState::Idle;

	ctrl_.position.resize(NumJoints, 0.0);
	ctrl_.position.at(0) = J0Center;
	temp_ctrl_ = ctrl_;
	fbk_.position.resize(NumJoints, -1.0);
	fbk_.current.resize(NumJoints, -1.0);
	fbk_.force.resize(NumTouchSensors, -1.0);
	initial_ = fbk_;
	pos_error_.resize(NumJoints, -1.0);
	overCurrentCounter_.resize(NumJoints, 0);
	overForceCounter_.resize(NumJoints, 0);

	/// JOINT RANGES ///

	MinJointRange_.resize(NumJoints, 0.0);
	MaxJointRange_.resize(NumJoints, 0.0);

	MinJointRange_.at(0) = 0.0 * M_PI / 180.0;
	MaxJointRange_.at(0) = 110.0 * M_PI / 180.0;


	MinJointRange_.at(FingersMap.at(0).first)  = 0.0 * M_PI / 180.0;
	MinJointRange_.at(FingersMap.at(0).second) = 0.0 * M_PI / 180.0;
	MinJointRange_.at(FingersMap.at(1).first)  = 0.0 * M_PI / 180.0;
	MinJointRange_.at(FingersMap.at(1).second) = 0.0 * M_PI / 180.0;
	MinJointRange_.at(FingersMap.at(2).first)  = 0.0 * M_PI / 180.0;
	MinJointRange_.at(FingersMap.at(2).second) = 0.0 * M_PI / 180.0;


	MaxJointRange_.at(FingersMap.at(0).first)  = 65.0 * M_PI / 180.0;
	MaxJointRange_.at(FingersMap.at(0).second) = 110.0 * M_PI / 180.0;
	MaxJointRange_.at(FingersMap.at(1).first)  = 65.0 * M_PI / 180.0;
	MaxJointRange_.at(FingersMap.at(1).second) = 110.0 * M_PI / 180.0;
	MaxJointRange_.at(FingersMap.at(2).first)  = 65.0 * M_PI / 180.0;
	MaxJointRange_.at(FingersMap.at(2).second) = 110.0 * M_PI / 180.0;

	/// JOINT ANGLE OFFSETS ///

	JLowerAngleOffset_.resize(3, 0.0);
	JUpperAngleOffset_.resize(3, 0.0);

	nh_->getParam("LowerAngleOffset", JLowerAngleOffset_);
	nh_->getParam("UpperAngleOffset", JUpperAngleOffset_);

	futils::PrintSTLVector(JLowerAngleOffset_, ',', "lower");
	std::cout << "" << std::endl;
	futils::PrintSTLVector(JUpperAngleOffset_, ',', "upper");
	std::cout << "" << std::endl;
	/*
	JLowerAngleOffset_.at(0) = M_PI_2 - 30.0 * M_PI / 180.0;
	JLowerAngleOffset_.at(1) = M_PI_2 - 75.0 * M_PI / 180.0;
	JLowerAngleOffset_.at(2) = M_PI_2 - 45.0 * M_PI / 180.0;

	JUpperAngleOffset_.at(0) = -29.0 * M_PI / 180.0;
	JUpperAngleOffset_.at(1) = -25.0 * M_PI / 180.0;
	JUpperAngleOffset_.at(2) = -21.0 * M_PI / 180.0;
*/
	PressingAngleOffset = 5.0 * M_PI / 180.0;

	/*std::cout << "ctrl_.position: " << std::endl;
	 for (int i = 0; i < NumJoints; ++i) {
	 std::cout << ctrl_.position.at(i) << " ";
	 }
	 std::cout << std::endl;*/
}

DexRovGripperInterface::~DexRovGripperInterface()
{
}

bool DexRovGripperInterface::CheckFinger(int finger)
{
	if (finger < NumFingers) {
		//std::cout << "FINGER INDEX=" << finger << std::endl;
		return true;
	} else {
		ROS_ERROR_STREAM(
		        "[DexRovGripperInterface::IsFingerTouching] Finger index out of bounds! (was " << finger << "while range is 0-" << NumFingers - 1);
		return false;
	}
}

void DexRovGripperInterface::PublishControl()
{
	ctrl_.position.at(FingersMap.at(1).first) = 0.0;
	ctrl_.position.at(FingersMap.at(1).second) = 0.0;
	pubHandCtrl_.publish(ctrl_);
}

void DexRovGripperInterface::PublishState()
{
	pubGripState_.publish(state_);
}

/**
 * Callback function for subscriber.
 */
void DexRovGripperInterface::FeedbackCallback(const dexrov_msgs::GripperJointState::ConstPtr &msg)
{
	fbk_.position = msg->position;
	fbk_.current = msg->current;
	fbk_.force = msg->force;
}

/**
 * Callback function for command.
 */
void DexRovGripperInterface::CommandCallback(
        const dexrov_gripper_ctrl::GripperCommand::ConstPtr &msg)
{
	std::stringstream ss;
	ss << "[CommandCallback] Received Command [ ID = " << GripperCommand2String.at(msg->ID);
	if (msg->ID == GripperCommand::Grip) {
		ss << ", grip_shape = " << GripperCommandShape2String.at(msg->grip_shape);
	}
	ROS_WARN_STREAM(ss.str() << " ]");

	if (!IsIdle()) {
		ROS_WARN_STREAM("Gripper *not* Idle, change of command");
	}

	command_.ID = msg->ID;
	command_.grip_shape = msg->grip_shape;
	checkForIncomingCommand_ = true;
}

/**
 * Callback function for dynamic reconfigure server.
 */
void DexRovGripperInterface::ConfigCallback(dexrov_gripper_ctrl::gripper_ctrl_paramsConfig &config,
        uint32_t level)
{
	// Set class variables to new values. They should match what is input at the dynamic reconfigure GUI.
	positionErrorThreshold_ = config.posErrThresh;
	currentTouchThreshold_ = config.currTouchThresh;
	forceTouchThreshold_ = config.forceTouchThresh;

	PressingAngleOffset = config.pressingAngleOffset * M_PI / 180.0;

	std::cout << "positionErrorThreshold_:" << positionErrorThreshold_ << std::endl;
	std::cout << "currentTouchThreshold_:" << currentTouchThreshold_ << std::endl;
	std::cout << "forceTouchThreshold_:" << forceTouchThreshold_ << std::endl;
	std::cout << "- Angles:" << std::endl;
	std::cout << "   PressingAngleOffset:" << PressingAngleOffset << std::endl;

}

bool DexRovGripperInterface::IsIdle()
{
	switch (state_.ID) {
	case GripperState::Idle:
	case GripperState::Open:
	case GripperState::Closed:
	case GripperState::Gripping:
		return true;
		break;
	default:
		return false;
		break;
	}
}

void DexRovGripperInterface::CheckForCommand()
{
	if (checkForIncomingCommand_) {
		rampTimer_.Reset();
		switch (command_.ID) {
		case GripperCommand::Stop:
			Stop();
			break;
		case GripperCommand::Open:
			OpenHand();
			break;
		case GripperCommand::Close:
			CloseHand();
			break;
		case GripperCommand::Grip:
			StartGrip();
			break;
		default:
			break;
		}

		checkForIncomingCommand_ = false;
	}
}

void DexRovGripperInterface::Stop()
{
	switch (state_.ID) {
	case GripperState::Open:
	case GripperState::Closed:
		break;
	default:
		state_.ID = GripperState::Idle;
		break;
	}
}

void DexRovGripperInterface::StartGrip()
{
	state_.ID = GripperState::ReachingGrip;
	grippingPhase_ = 0;
}

void DexRovGripperInterface::ComputeControl()
{
	switch (state_.ID) {
	case GripperState::Opening:
	case GripperState::Closing:
		PositionControl();
		break;
	case GripperState::ReachingGrip:
		GripShapeControl();
		break;
	default:
		break;
	}
}

void DexRovGripperInterface::OpenFinger(int finger)
{
	if (CheckFinger(finger)) {
		ctrl_.position.at(FingersMap.at(finger).first) = MinJointRange_.at(FingersMap.at(finger).first);
		ctrl_.position.at(FingersMap.at(finger).second) = MinJointRange_.at(FingersMap.at(finger).first);
	}
}

void DexRovGripperInterface::CloseFinger(int finger)
{
	if (CheckFinger(finger)) {
		ctrl_.position.at(FingersMap.at(finger).first) = MaxJointRange_.at(FingersMap.at(finger).first);
		ctrl_.position.at(FingersMap.at(finger).second) = MaxJointRange_.at(FingersMap.at(finger).first);
	}
}

void DexRovGripperInterface::OpenHand()
{
	for (auto const& finger : FingersMap) { // access by forwarding reference
		OpenFinger(finger.first);
	}
	state_.ID = GripperState::Opening;
}

void DexRovGripperInterface::CloseHand()
{
	for (auto const& finger : FingersMap) { // access by forwarding reference
		CloseFinger(finger.first);
	}
	state_.ID = GripperState::Closing;
}

double DexRovGripperInterface::GetFingerForce(int finger)
{
	double touch(0.0);

	if (CheckFinger(finger)) {
		touch = fbk_.force.at(finger);
	}
	return touch;
}

double DexRovGripperInterface::GetPalmForce()
{
	return fbk_.force.at(PalmIndex);
}

bool DexRovGripperInterface::IsPosErrorBelowThresh()
{
	bool belowThresh = true;
	for (int i = 1; i < NumJoints; ++i) {
		pos_error_.at(i) = fbk_.position.at(i) - ctrl_.position.at(i);
		if (std::fabs(pos_error_.at(i)) > positionErrorThreshold_) {
			belowThresh = false;
		}
	}
	return belowThresh;
}

void DexRovGripperInterface::PositionControl()
{

	if (IsPosErrorBelowThresh()) {
		ROS_WARN_STREAM("Position Reached!");
		switch (state_.ID) {
		case GripperState::Opening:
			state_.ID = GripperState::Open;
			break;
		case GripperState::Closing:
			state_.ID = GripperState::Closed;
			break;
		default:
			break;
		}
	}
}

void DexRovGripperInterface::GripShapeControl()
{
	switch (command_.grip_shape) {
	case GripperCommand::Cylinder:
		/*
		 * 1) Close firstly base phalanges
		 */
		if (grippingPhase_ == 0) {
			for (auto const& fingers : FingersMap) {
				ctrl_.position.at(fingers.second.first) = MaxJointRange_.at(fingers.second.first);
			}
			grippingPhase_ = 1;
		}

		/*
		 * 2) Check if current goes above level
		 */
		if (grippingPhase_ == 1) {
			grippingPhase_ = 2;
			for (auto const& fingers : FingersMap) {
				if (fbk_.current.at(fingers.second.first) < currentTouchThreshold_) {
					grippingPhase_ = 1;
				}
			}
		}

		/*
		 * 3) Close second phalanges
		 */
		if (grippingPhase_ == 2) {
			for (auto const& fingers : FingersMap) {
				ctrl_.position.at(fingers.second.second) = MaxJointRange_.at(fingers.second.second);
			}
			grippingPhase_ = 3;
		}

		/*
		 * 4) Check until current raises again
		 */
		if (grippingPhase_ == 3) {
			grippingPhase_ = 4;
			for (auto const& fingers : FingersMap) {
				if (fbk_.current.at(fingers.second.second) < currentTouchThreshold_) {
					grippingPhase_ = 3;
				}
			}
		}

		/*
		 * 5) Stop
		 */
		if (grippingPhase_ == 4) {
			ROS_WARN_STREAM("Cylinder Grasped!");
			state_.ID = GripperState::Gripping;
		}

		break;
	case GripperCommand::Plane:
		/*
		 * 1) Align the second phalange to be parallel to base (i.e. angle wrt to base equal to pi/2)
		 */
		if (grippingPhase_ == 0) {
			for (auto const& fingers : FingersMap) {

				ctrl_.position.at(fingers.second.first) = M_PI_2
				        - (JLowerAngleOffset_.at(fingers.first));

				ctrl_.position.at(fingers.second.second) = M_PI_2
				        - (JLowerAngleOffset_.at(fingers.first)
				                + ctrl_.position.at(fingers.second.first)
				                + JUpperAngleOffset_.at(fingers.first));
			}
			grippingPhase_ = 1;
			gripTimer_.Start();
		}

		/*
		 * 2) Check till position is reached then start closing the first phalanges
		 */
		else if (grippingPhase_ == 1) {
			if (gripTimer_.Elapsed() > 2.5) {
				rampTimer_.Start();
				initial_ = fbk_;
				grippingPhase_ = 2;
				std::fill(overCurrentCounter_.begin(), overCurrentCounter_.end(), 0);
			}
		}

		/*
		 * 3) Check until current raises above threshold, keeping the second phalanges parallel to base
		 */
		else if (grippingPhase_ == 2) {
			//grippingPhase_ = 3;
			for (auto const& fingers : FingersMap) {

				// Move with a ramp reference
				ctrl_.position.at(fingers.second.first) = std::min(
				        initial_.position.at(fingers.second.first) + rampTimer_.Elapsed() * 0.15,
				        MaxJointRange_.at(fingers.second.first));

				// Generate a related feedback for the upper joint to remain parallel
				/*temp_ctrl_.position.at(fingers.second.second) = M_PI_2
				 - (JLowerAngleOffset_.at(fingers.first) + ctrl_.position.at(fingers.second.first)
				 + JUpperAngleOffset_.at(fingers.first));

				 // Close the loop with an additional error term
				 pos_error_.at(fingers.second.second) = temp_ctrl_.position.at(fingers.second.second) - fbk_.position.at(fingers.second.second);

				 ctrl_.position.at(fingers.second.second) = temp_ctrl_.position.at(fingers.second.second) +
				 positionCtrlGain * pos_error_.at(fingers.second.second);
				 */

				ctrl_.position.at(fingers.second.second) = M_PI_2
				        - (JLowerAngleOffset_.at(fingers.first)
				                + ctrl_.position.at(fingers.second.first)
				                + JUpperAngleOffset_.at(fingers.first));

			}

			for (int i = 0; i < NumJoints; ++i) {
				if (fbk_.current.at(i) > currentTouchThreshold_) {
					++overCurrentCounter_.at(i);
					if (overCurrentCounter_.at(i) == MaxConsecutiveOverCurrentLoops) {
						grippingPhase_ = 3;
					}
				} else {
					--overCurrentCounter_.at(i);
					if (overCurrentCounter_.at(i) < 0) {
						overCurrentCounter_.at(i) = 0;
					}

				}
			}

			if (grippingPhase_ == 3) {
				for (auto const& fingers : FingersMap) {
					ctrl_.position.at(fingers.second.second) = M_PI_2
					        - (JLowerAngleOffset_.at(fingers.first) - PressingAngleOffset);

					ctrl_.position.at(fingers.second.second) = M_PI_2
					        - (JLowerAngleOffset_.at(fingers.first)
					                + ctrl_.position.at(fingers.second.first)
					                + JUpperAngleOffset_.at(fingers.first)) + PressingAngleOffset;
				}
				gripTimer_.Start();
			}
		}

		else if (grippingPhase_ == 3) {
			if (gripTimer_.Elapsed() > 2.5) {
				std::fill(overForceCounter_.begin(), overForceCounter_.end(), 0);
				gripTimer_.Start();
				grippingPhase_ = 4;
			}
		} else if (grippingPhase_ == 4) {
			/*
			 * 4) Stop and if touch sensor are touching trigger a successful grasp
			 */
			for (int i = 0; i < NumTouchSensors; ++i) {
				if (fbk_.force.at(i) > forceTouchThreshold_) {
					++overForceCounter_.at(i);
				}
			}

			if (gripTimer_.Elapsed() > 2.5) {
				bool successfulGrasp = false;

				for (int i = 0; i < NumTouchSensors; ++i) {
					if (overForceCounter_.at(i) > MaxConsecutiveOverForceLoops) {
						bool successfulGrasp = true;
					}
				}

				if (successfulGrasp) {
					ROS_WARN_STREAM("Plane Grasped!");
					state_.ID = GripperState::Gripping;
				} else {
					ROS_WARN_STREAM("Plane Grasp Failed!");
					state_.ID = GripperState::Idle;
				}
			}
		}
		break;
	default:
		break;
	}
}
