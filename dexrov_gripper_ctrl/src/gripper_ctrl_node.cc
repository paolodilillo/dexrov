/*
 * gripper_ctrl_node.cc
 *
 *  Created on: Apr 3, 2018
 *      Author: fw
 */

#include <iostream>

#include "Futils.h"
#include "DexRovGripperInterface.h"

enum class State {
	Init, Core, End
};

int main(int argc, char *argv[])
{
	ros::init(argc, argv, "gripper_ctrl_node");
	ros::NodeHandle n;

	DexRovGripperInterface *gripperIF = new DexRovGripperInterface();

	// Set up a dynamic reconfigure server.
	// This should be done before reading parameter server values.
	dynamic_reconfigure::Server<dexrov_gripper_ctrl::gripper_ctrl_paramsConfig> dr_srv;
	dynamic_reconfigure::Server<dexrov_gripper_ctrl::gripper_ctrl_paramsConfig>::CallbackType cb;
	cb = boost::bind(&DexRovGripperInterface::ConfigCallback, gripperIF, _1, _2);
	dr_srv.setCallback(cb);

	std::string message;
	int rate;

	// Initialize node parameters from launch file or command line.
	// Use a private node handle so that multiple instances of the node can
	// be run simultaneously while using different parameters.
	// Parameters defined in the .cfg file do not need to be initialized here
	// as the dynamic_reconfigure::Server does this for you.
	ros::NodeHandle private_node_handle_("~");
	private_node_handle_.param("rate", rate, int(1));

	// Tell ROS how fast to run this node.
	ros::Rate r(rate);
	std::cout << "rate=" << rate << std::endl;
	State currentState = State::Init;

	futils::Spinner mySpinner(5);
	futils::Timer printTimer1, printTimer2;
	printTimer1.Start();
	printTimer2.Start();

	// Main loop
	while (n.ok()) {

		mySpinner();

		if (currentState == State::Init) {
			// Compute initial operations
			gripperIF->OpenHand();
			currentState = State::Core;
		}

		if (currentState == State::Core) {
			gripperIF->CheckForCommand();

			if (!gripperIF->IsIdle()) {
				gripperIF->ComputeControl();
				gripperIF->PublishControl();

				if (printTimer1.GetCurrentLapTime() > 0.5) {
					printTimer1.Lap();
					auto fbk = gripperIF->GetFeedback();
					auto ctrl = gripperIF->GetControl();

					std::vector<double> diff(DexRovGripperInterface::NumJoints, 0.0);
					std::transform(fbk.position.begin(), fbk.position.end(), ctrl.position.begin(),
					        diff.begin(), std::minus<double>());

					ROS_DEBUG_STREAM("Pos Error = " << futils::STLVectorToString(diff, ' '));
				}
			}
		}

		if (currentState == State::End) {
			n.shutdown();
		}

		gripperIF->PublishState();

		if (printTimer2.GetCurrentLapTime() > 2) {
			printTimer2.Lap();

			ROS_INFO_STREAM(
			        "Gripper State: " << gripperIF->GripperState2String.at(gripperIF->GetState()));

			if (gripperIF->GetState() == dexrov_gripper_ctrl::GripperState::ReachingGrip) {
				ROS_INFO_STREAM(
				        "Gripping Phase: " << gripperIF->GetGrippingPhase());
			}
		}

		ros::spinOnce();
		r.sleep();
	}

	return 0;
}

