/*
 * dummy_gripper.cc
 *
 *  Created on: Apr 3, 2018
 *      Author: fw
 */

#include <iostream>
#include <atomic>
#include <algorithm>

#include "DexRovGripperInterface.h"
#include "Futils.h"

dexrov_msgs::GripperJointPosReq ctrl_message;
std::atomic<bool> onChange(false);
const double errorThreshold = 0.01;
const double K_i = 0.01;

void controlCallback(const dexrov_msgs::GripperJointPosReq::ConstPtr &msg);

int main(int argc, char *argv[])
{
	ros::init(argc, argv, "dummy_gripper");
	ros::NodeHandle n;

	ctrl_message.position.resize(DexRovGripperInterface::NumJoints);
	std::fill(ctrl_message.position.begin(), ctrl_message.position.end(), 0.0);

	/*const int NumFingers = 3;
	 const int NumJoints = 7;
	 const int NumTouchSensors = 4;*/
	dexrov_msgs::GripperJointState fbk_message;
	int rate;
	std::string topic_ctrl, topic_fbk;

	// Initialize node parameters from launch file or command line.
	// Use a private node handle so that multiple instances of the node can
	// be run simultaneously while using different parameters.
	ros::NodeHandle private_node_handle_("~");
	private_node_handle_.param("rate", rate, int(1));
	private_node_handle_.param("topic_left_state", topic_fbk, std::string("/hand/L/joint_state"));
	private_node_handle_.param("topic_left_control", topic_ctrl,
	        std::string("/hand/L/req_joint_position"));

	ros::Publisher pub_message = n.advertise<dexrov_msgs::GripperJointState>(topic_fbk.c_str(), 10);
	ros::Subscriber sub_message = n.subscribe(topic_ctrl.c_str(), 1000, controlCallback);

	// Tell ROS how fast to run this node.
	ros::Rate r(rate);

	fbk_message.position.resize(DexRovGripperInterface::NumJoints, 0.1);
	fbk_message.current.resize(DexRovGripperInterface::NumJoints, 0.2);
	fbk_message.force.resize(DexRovGripperInterface::NumTouchSensors, false);

	while (n.ok()) {

		for (int i = 0; i < DexRovGripperInterface::NumJoints; ++i) {
			double fbkPos = fbk_message.position.at(i);
			double ctrlPos = ctrl_message.position.at(i);
			//ROS_WARN_STREAM(i << " ");

			if (std::fabs(fbkPos - ctrlPos) > errorThreshold) {
				fbkPos = fbkPos + copysign(1.0, ctrlPos - fbkPos) * K_i * rate;

				if (fbkPos <= DexRovGripperInterface::MinJointRangeUpper_.at(i)){
					fbkPos = DexRovGripperInterface::MinJointRangeUpper_;
				} else if (fbkPos >= DexRovGripperInterface::MaxJointRangeUpper_) {
					fbkPos = DexRovGripperInterface::MaxJointRangeUpper_;
				}
				fbk_message.position.at(i) = fbkPos;
			}
		}

		pub_message.publish(fbk_message);

		ros::spinOnce();
		r.sleep();
	}

	return 0;
}

void controlCallback(const dexrov_msgs::GripperJointPosReq::ConstPtr &msg)
{
	ctrl_message.position = msg->position;
	//std::cout << "[dummy] Received ctrl position:"  << futils::STLVectorToString(ctrl_message.position, ' ') << std::endl;
	//ROS_DEBUG_STREAM(
	//       "[dummy] Received ctrl position:" << futils::STLVectorToString(ctrl_message.position, ' '));
	//onChange = true;

}
