#!/bin/bash

# generates a function named $1 which:
# - executes $(which $1) [with args]
# - suppresses output lines which match $2
# e.g. adding: _supress echo "hello\|world"
# will generate this function:
# echo() { $(which echo) "$@" 2>&1 | tr -d '\r' | grep -v "hello\|world"; }
# and from now on, using echo will work normally except that lines with
# hello or world will not show at the output
# to see the generated functions, replace eval with echo below
# the 'tr' filter makes sure no spurious empty lines pass from some commands
_supress() {
  eval "$1() { \$(which $1) \"\$@\" 2>&1 | tr -d '\r' | grep -v \"$2\"; }"
}

_supress gnome-terminal "Gtk-WARNING"
#_supress gedit          "Gtk-WARNING\|connect to accessibility bus"
#_supress gnome-terminal "accessibility bus\|stop working with a future version"

echo "Starting Controller"
gnome-terminal -e "roslaunch dexrov_gripper_ctrl sim_controller.launch" &
let temp=$!
pids+=($temp)

sleep 3

echo "Starting Recorder" 
gnome-terminal -e "rosbag record -O subset /hand/L/joint_state /hand/L/req_joint_position /hand/L/command /hand/L/state" &
let temp=$!
pids+=($temp)

echo ${#pids[*]} ' modules are running'


#echo -e '\e[1;33mType q to exit program, do NOT do it during movimentation\e[0m'
while true
do
	read q
	if [ "$q" = "q" ]; then
		  
		for i in ${pids[*]}
		do
			echo killing process $i	
			#to csv
			kill -INT $i

		done
		exit
	else
		echo 'Invalid command'
	fi
done

