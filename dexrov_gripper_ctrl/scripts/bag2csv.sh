#!/bin/bash

# Checking for ROS
rosver=`rosversion -d`
if [ ${rosver} == "<unknown>" ]
    then
        echo "ROS not found! (did you source?)"
        exit 1
    else
        echo "ROS version: ${rosver}"
        source /opt/ros/${rosver}/setup.bash
fi

# File variable
file=""

# Logs folder 
logFolder=`echo ${HOME}/logs`

# Check if input file is given, otherwise use most recent one
if [ $# -lt 1 ]
    then
        echo "Usage: '$0 bag-to-convert' (located in ${logFolder})"
        file=`ls -Frt ${logFolder} | grep "[^/]$" | tail -n 1`
        echo -n "Since no file is provided defaulting to most recent one: "
        echo ${file}
    else
        file=$1
fi

if [ ! -f ${logFolder}/${file} ]; then
    echo "File not found! (looking in ${logFolder})"
    exit 1
fi

# Create folder for converted files
mkdir -p ${logFolder}/${file}_csv

# List the topics included in the bag file
echo "Topics:"
rostopic list -b ${logFolder}/${file}

# Exporting all topics in separate csv files with .txt extension (and replacing '/' with '_')
for topic in `rostopic list -b ${logFolder}/${file}`
    do rostopic echo -p -b ${logFolder}/${file} $topic > ${logFolder}/${file}_csv/${topic//\//_}.txt
done

echo "On success files are saved to '${logFolder}/${file}/'"


