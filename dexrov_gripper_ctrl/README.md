# DexRov Gripper Control #

This repo contains a ROS node to control the gripper for the DexRov project.

### Dependencies ###

* ROS Kinetic (http://wiki.ros.org/kinetic/Installation/Ubuntu)
* `ros-kinetic-gazebo-msgs` (installable by **apt**)
* `dexrov_msgs` (https://bitbucket.org/merosss/dexrov_msgs/src/master)

### Build and Run ###

To compile this package you need to clone it in a ROS catkin workspace, together with the `dexrov_msgs` package. Then you can execute the following commands to build and run in straight away:

    catkin_make
    source ./devel/setup.bash
    roslaunch dexrov_gripper_ctrl controller.launch

Which runs the package nodes together with the `rqt_reconfigure` (to manage the dynamic reconfigure parameters) and `rqt_console` (to view the and change the verbosity levels).

### Contact ###

* <enrico.simetti@unige.it>
* <francesco.wanderlingh@dibris.unige.it>
