#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/physics/Model.hh>
#include <gazebo/common/Plugin.hh>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include <vector>
#include <thread>
#include <sensor_msgs/JointState.h>




using namespace std;



namespace gazebo
{


  
  class JointVelocityPluginDummy : public ModelPlugin
  {
  
  physics::ModelPtr model;
  physics::JointPtr joint1;
  physics::JointPtr joint2;
  physics::JointPtr joint3;
  physics::JointPtr joint4;
  physics::JointPtr joint5;
  physics::JointPtr joint6;
  
  
  physics::LinkPtr link1;
  physics::LinkPtr link2;
  physics::LinkPtr link3;
  physics::LinkPtr link4;
  physics::LinkPtr link5;
  physics::LinkPtr link6;
  physics::LinkPtr link7;
  physics::LinkPtr link8;
  
  bool verbose = false;
  bool saturate_velocity = true;
  
  double q1,q2,q3,q4,q5,q6;
  
  double velocity[6];
  double position[6];
  bool flag_position = false;

  private: std::unique_ptr<ros::NodeHandle> rosNode;
  private: ros::Subscriber rosSub;
  private: ros::CallbackQueue rosQueue;
  private: std::thread rosQueueThread;
  private: event::ConnectionPtr updateConnection;
  
  private: ros::Subscriber rosSub_position;
  private: ros::CallbackQueue rosQueue_position;
  private: std::thread rosQueueThread_position;
 

  
    /// \brief Constructor
    public: JointVelocityPluginDummy() {}
    
  
  public: void OnRosMsg(const sensor_msgs::JointStateConstPtr &_msg)
{

	velocity[0] = _msg->velocity[0];
	velocity[1] = _msg->velocity[1];
	velocity[2] = _msg->velocity[2];
	velocity[3] = _msg->velocity[3];
	velocity[4] = _msg->velocity[4];
	velocity[5] = _msg->velocity[5];
	

}

    
  public: void OnRosMsg_position(const sensor_msgs::JointStateConstPtr &_msg)
{

	position[0] = _msg->position[0];
	position[1] = _msg->position[1];
	position[2] = _msg->position[2];
	position[3] = _msg->position[3];
	position[4] = _msg->position[4];
	position[5] = _msg->position[5];
	
	
	flag_position = true;

}


private: void QueueThread()
{
  static const double timeout = 0.01;
  while (this->rosNode->ok())
  {
    this->rosQueue.callAvailable(ros::WallDuration(timeout));
  }
}


private: void QueueThread_position()
{
  static const double timeout = 0.01;
  while (this->rosNode->ok())
  {
    this->rosQueue_position.callAvailable(ros::WallDuration(timeout));
  }
}

    

    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
    {
      
  if (_model->GetJointCount() == 0)
  {
    std::cerr << "Invalid joint count, joint velocity plugin not loaded\n";
    return;
  }
 std::cerr << "Velocity plugin dummy loaded\n";
 
  
  
  int argc = 0;
  char **argv = NULL;
        
      ros::init(argc, argv, "joint_velocity_plugin2");

         
    for(int i=0;i<6;i++)
  
  	velocity[i] = 0;
  
  std::string ns, topic_name, full_name;
  
  if (_sdf->HasElement("ns"))
  	ns = _sdf->Get<std::string>("ns");
  	
  if (_sdf->HasElement("topic"))
  	topic_name = _sdf->Get<std::string>("topic");
  	
  full_name = "/" + ns + "/" + topic_name;
  full_name = "/arm/R/desired_joint_velocity";

  this->rosNode.reset(new ros::NodeHandle("joint_velocity_plugin2"));


ros::SubscribeOptions so =
  ros::SubscribeOptions::create<sensor_msgs::JointState>(
      full_name,
      1,
      boost::bind(&JointVelocityPluginDummy::OnRosMsg, this, _1),
      ros::VoidPtr(), &this->rosQueue);
      
this->rosSub = this->rosNode->subscribe(so);

this->rosQueueThread =
  std::thread(std::bind(&JointVelocityPluginDummy::QueueThread, this));
  
 
 
 std::string name = "/arm/R/desired_joint_positions";

ros::SubscribeOptions so_position =
  ros::SubscribeOptions::create<sensor_msgs::JointState>(
      name,
      1,
      boost::bind(&JointVelocityPluginDummy::OnRosMsg_position, this, _1),
      ros::VoidPtr(), &this->rosQueue_position);
      
this->rosSub_position = this->rosNode->subscribe(so_position);

this->rosQueueThread_position =
  std::thread(std::bind(&JointVelocityPluginDummy::QueueThread_position, this));
  
  
  if (_sdf->HasElement("q1") && _sdf->HasElement("q2") && _sdf->HasElement("q3") && _sdf->HasElement("q4") && _sdf->HasElement("q5") && _sdf->HasElement("q6")){
  
  	q1 = _sdf->Get<double>("q1");		q2 = _sdf->Get<double>("q2");		q3 = _sdf->Get<double>("q3");
  	q4 = _sdf->Get<double>("q4");		q5 = _sdf->Get<double>("q5");		q6 = _sdf->Get<double>("q6");	
  	
  
  }
  
  else
  {
  
  	q1 = 0.0;		q2 = -0.7071;		q3 = 0.7071;
  	q4 = 0.0;		q5 = 0.0;		q6 = 1.57;	
  
  
  }

  
  this->model = _model;

  
  this->joint1 = _model->GetJoint("arm_joint12");
  this->joint2 = _model->GetJoint("arm_joint22");
  this->joint3 = _model->GetJoint("arm_joint32");
  this->joint4 = _model->GetJoint("arm_joint42");
  this->joint5 = _model->GetJoint("arm_joint52");
  this->joint6 = _model->GetJoint("arm_joint62");
  
  this->joint1->SetPosition(0,q1);
  this->joint2->SetPosition(0,q2);
  this->joint3->SetPosition(0,q3);
  this->joint4->SetPosition(0,q4);
  this->joint5->SetPosition(0,q5);
  this->joint6->SetPosition(0,q6);
  
  this->link1 = _model->GetLink("arm_link22");
  this->link2 = _model->GetLink("arm_link32");
  this->link3 = _model->GetLink("arm_link42");
  this->link4 = _model->GetLink("arm_link52");
  this->link5 = _model->GetLink("arm_link62");
  this->link6 = _model->GetLink("arm_link72");

  this->link1->SetGravityMode(false);
  this->link2->SetGravityMode(false);
  this->link3->SetGravityMode(false);
  this->link4->SetGravityMode(false);
  this->link5->SetGravityMode(false);
  this->link6->SetGravityMode(false);
  
 this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&JointVelocityPluginDummy::OnUpdate, this));

    }
    



 public: void OnUpdate(){
  

  
  if(std::isnan(velocity[0]) || std::isnan(velocity[1]) || std::isnan(velocity[2]) || std::isnan(velocity[3]) || std::isnan(velocity[4]) || std::isnan(velocity[5])) {
  
  	for(int i=0;i<6;i++) 
  	
  		velocity[i] = 0.0;
  	
  	
  }



    if(this->joint1->GetAngle(0) == this->joint1->GetUpperLimit(0) && velocity[0] > 0.0){
    
    		this->joint1->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 1 position limit reached\n";	
    	}
	
    else if(this->joint1->GetAngle(0) == this->joint1->GetLowerLimit(0) && velocity[0] < 0.0){
    
    		this->joint1->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 1 position limit reached\n";	
    	}
    	    		
    else if(velocity[0] > joint1->GetVelocityLimit(0) && saturate_velocity){
    
    		joint1->SetVelocity(0,joint1->GetVelocityLimit(0));
    		
    	if(verbose)
    		
    	cout << "[WARNING] Joint 1 velocity limit reached\n";
    		
    	}
    	
     else if(velocity[0] < -joint1->GetVelocityLimit(0) && saturate_velocity){
    
    		joint1->SetVelocity(0,-joint1->GetVelocityLimit(0));
    		
    	if(verbose)	
    	cout << "[WARNING] Joint 1 velocity limit reached\n";
    	}
    		
    else
    
    		this->joint1->SetVelocity(0,velocity[0]);
    		
    	
    	
    	
    	
    		
        if(this->joint2->GetAngle(0) == this->joint2->GetUpperLimit(0) && velocity[1] > 0.0){
    
    		this->joint2->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 2 position limit reached\n";	
    	}
	
    else if(this->joint2->GetAngle(0) == this->joint2->GetLowerLimit(0) && velocity[1] < 0.0){
    
    		this->joint2->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 2 position limit reached\n";	
    	}
    		
    else if(velocity[1] > joint2->GetVelocityLimit(0) && saturate_velocity){
    
    		joint2->SetVelocity(0,joint2->GetVelocityLimit(0));
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 2 velocity limit reached\n";
    		}
    
    else if(velocity[1] < -joint2->GetVelocityLimit(0) && saturate_velocity){
    
    		joint2->SetVelocity(0,-joint2->GetVelocityLimit(0));
    		
    	if(verbose)	
    		
    	cout << "[WARNING] Joint 2 velocity limit reached\n";
    		}
    		
    else
    
    		this->joint2->SetVelocity(0,velocity[1]);
    
    
    
    
    
       if(this->joint3->GetAngle(0) == this->joint3->GetUpperLimit(0) && velocity[2] > 0.0){
    
    		this->joint3->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 3 position limit reached\n";	
    	}
	
    else if(this->joint3->GetAngle(0) == this->joint3->GetLowerLimit(0) && velocity[2] < 0.0){
    
    		this->joint3->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 3 position limit reached\n";	
    	}
    
    else if(velocity[2] > joint3->GetVelocityLimit(0) && saturate_velocity){
    
    		joint3->SetVelocity(0,joint3->GetVelocityLimit(0));
    		
    		
    	if(verbose)	
    	cout << "[WARNING] Joint 3 velocity limit reached\n";
    }
    
    else if(velocity[2] < -joint3->GetVelocityLimit(0) && saturate_velocity){
    
    		joint3->SetVelocity(0,-joint3->GetVelocityLimit(0));
    		
    	if(verbose)
    		
    	cout << "[WARNING] Joint 3 velocity limit reached\n";
  }    
    
    
    else
    
    		this->joint3->SetVelocity(0,velocity[2]);
    		
    		
    				
    		
        if(this->joint4->GetAngle(0) == this->joint4->GetUpperLimit(0) && velocity[3] > 0.0){
    
    		this->joint4->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 4 position limit reached\n";	
    	}
	
    else if(this->joint4->GetAngle(0) == this->joint4->GetLowerLimit(0) && velocity[3] < 0.0){
    
    		this->joint4->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 4 position limit reached\n";	
    	}
    		
    else if(velocity[3] > joint4->GetVelocityLimit(0) && saturate_velocity){
    
    		joint4->SetVelocity(0,joint4->GetVelocityLimit(0));
    		
    	if(verbose)	
    
    	cout << "[WARNING] Joint 4 velocity limit reached\n";
    }
    
    else if(velocity[3] < -joint4->GetVelocityLimit(0) && saturate_velocity){
    
    		joint4->SetVelocity(0,-joint4->GetVelocityLimit(0));
    
    	if(verbose)
    	
    	cout << "[WARNING] Joint 4 velocity limit reached\n";
    }
    
    
    else
    
    		this->joint4->SetVelocity(0,velocity[3]);
    
    
    
    
        if(this->joint5->GetAngle(0) == this->joint5->GetUpperLimit(0) && velocity[4] > 0.0){
    
    		this->joint5->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 5 position limit reached\n";	
    	}
	
    else if(this->joint5->GetAngle(0) == this->joint5->GetLowerLimit(0) && velocity[4] < 0.0){
    
    		this->joint5->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 5 position limit reached\n";	
    	}
    		
    else if(velocity[4] > joint5->GetVelocityLimit(0) && saturate_velocity){
    
    		joint5->SetVelocity(0,joint5->GetVelocityLimit(0));
    	
    	if(verbose)
    	cout << "[WARNING] Joint 5 velocity limit reached\n";
    }
    
    else if(velocity[4] < -joint5->GetVelocityLimit(0) && saturate_velocity){
    
    		joint5->SetVelocity(0,-joint5->GetVelocityLimit(0));
    	
    	if(verbose)
    	cout << "[WARNING] Joint 5 velocity limit reached\n";
    }
    
    
    else
    
    		this->joint5->SetVelocity(0,velocity[4]);
    		
    		
    		
    		
        if(this->joint6->GetAngle(0) == this->joint6->GetUpperLimit(0) && velocity[5] > 0.0){
    
    		this->joint6->SetVelocity(0,0);
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 6 position limit reached\n";	
    	}
	
    else if(this->joint6->GetAngle(0) == this->joint6->GetLowerLimit(0) && velocity[5] < 0.0){
    
    		this->joint6->SetVelocity(0,0);
    		
    		
    		if(verbose)
    		
    		cout << "[WARNING] Joint 6 position limit reached\n";	
    	}
    
    else if(velocity[5] > joint6->GetVelocityLimit(0) && saturate_velocity){
    
    		joint6->SetVelocity(0,joint6->GetVelocityLimit(0));
    	
    	if(verbose)
    	cout << "[WARNING] Joint 6 velocity limit reached\n";
    }
    
    else if(velocity[5] < -joint6->GetVelocityLimit(0) && saturate_velocity){
    
    		joint6->SetVelocity(0,-joint6->GetVelocityLimit(0));
    	
    	if(verbose)
    	cout << "[WARNING] Joint 6 velocity limit reached\n";		
    }
    
    			
    else
    
    		this->joint6->SetVelocity(0,velocity[5]);
    		/*
    this->joint12->SetVelocity(0,0);
    this->joint22->SetVelocity(0,0);
    this->joint32->SetVelocity(0,0);
    this->joint42->SetVelocity(0,0);
    this->joint52->SetVelocity(0,0);
    this->joint62->SetVelocity(0,0);
    			*/
    			
    		
 if(flag_position){
 
  this->joint1->SetPosition(0,position[0]);
  this->joint2->SetPosition(0,position[1]);
  this->joint3->SetPosition(0,position[2]);
  this->joint4->SetPosition(0,position[3]);
  this->joint5->SetPosition(0,position[4]);
  this->joint6->SetPosition(0,position[5]);
 
 flag_position = false;
 
 }

}




    
  };

  // Tell Gazebo about this plugin, so that Gazebo can call Load on this plugin.
  GZ_REGISTER_MODEL_PLUGIN(JointVelocityPluginDummy)
}
