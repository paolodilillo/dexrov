#include <ros/ros.h>
#include "ExtendedTaskHierarchy.h"
#include <Eigen/Core>
#include <sensor_msgs/JointState.h>
#include <vector>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <dexrov_msgs/CartesianError.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf/exceptions.h>

using namespace std;
using namespace Eigen;


VectorXd q(6), qGazebo(6);

bool q_initialized = false;
bool pose_initialized = false;

vector<double> pos_d(3), ori_d(4), des(7);



void eeCB(const geometry_msgs::PoseStamped msg){


	VectorXd quat(4);
	Matrix3d R;
	MatrixXd T(4,4);
	MatrixXd T_out(4,4);
	VectorXd quat_d(4);
	
	quat << msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w; 
	R = quat2rot(quat);	
	Vector3d translation;
	translation << msg.pose.position.x, msg.pose.position.y, msg.pose.position.z;

	
	T.block(0,0,3,3) = R;
	T.block(0,3,3,1) = translation;
	T.row(3) << 0,0,0,1;
	
	T_out = armbase2ik(T);
	
	quat_d = rot2quat(T_out.block(0,0,3,3));
	
	pos_d[0] = T_out(0,3);
	pos_d[1] = T_out(1,3);
	pos_d[2] = T_out(2,3);
	
	ori_d[0] = quat_d(0);
	ori_d[1] = quat_d(1);
	ori_d[2] = quat_d(2);;
	ori_d[3] = quat_d(3);;
	
	des[0] = quat_d(0);
	des[1] = quat_d(1);
	des[2] = quat_d(2);
	des[3] = quat_d(3);
	des[4] = T_out(0,3);
	des[5] = T_out(1,3);
	des[6] = T_out(2,3);
	

	
	pose_initialized = true;

	/*

	pos_d[0] = msg.pose.position.x;
	pos_d[1] = msg.pose.position.y;
	pos_d[2] = msg.pose.position.z;
	
	ori_d[0] = msg.pose.orientation.x;
	ori_d[1] = msg.pose.orientation.y;
	ori_d[2] = msg.pose.orientation.z;
	ori_d[3] = msg.pose.orientation.w;
	
	des[0] = msg.pose.orientation.x;
	des[1] = msg.pose.orientation.y;
	des[2] = msg.pose.orientation.z;
	des[3] = msg.pose.orientation.w;
	des[4] = msg.pose.position.x;
	des[5] = msg.pose.position.y;
	des[6] = msg.pose.position.z;
	
	pose_initialized = true;
*/

	
	
}

void jointCB(const sensor_msgs::JointState::ConstPtr& msg){

/*

Callback for acquiring the current joint positions

*/

qGazebo(0) = msg->position[0];
qGazebo(1) = msg->position[1];
qGazebo(2) = msg->position[2];
qGazebo(3) = msg->position[3];
qGazebo(4) = msg->position[4];
qGazebo(5) = msg->position[5];


q = mask_Gazebo2DH(qGazebo, qGazebo.size());

//cout << q << "\n\n";

q_initialized = true;



}


int main(int argc, char* argv[]){



 ros::init(argc, argv, "inverse_kinematics_ros_iface_hf");

 ros::NodeHandle n;
 tf::TransformListener mytf;
 sleep(3);
  
 
 ros::Publisher ee_pub_r = n.advertise<geometry_msgs::PoseStamped>("/arm/R/end_effector_pose", 1);
 ros::Publisher ee_pub_l = n.advertise<geometry_msgs::PoseStamped>("/arm/L/end_effector_pose", 1);
 ros::Publisher ee_error_pub_r = n.advertise<dexrov_msgs::CartesianError>("/arm/R/cartesian_error", 1);
 ros::Publisher ee_error_pub_l = n.advertise<dexrov_msgs::CartesianError>("/arm/L/cartesian_error", 1);
 
 ros::Subscriber joint_sub = n.subscribe("/arm/L/fbk_joint_position", 1, jointCB);
 ros::Subscriber ee_sub = n.subscribe("/cog/desired_end_effector_pose", 1, eeCB);

 ros::Rate loop_rate(1);
  
  
 Vector3d position;
 VectorXd orientation(4);
 Vector3d quat_error;
 VectorXd quat_des(4);
 
 VectorXd quat_armbase(4);
 Matrix3d R_armbase;
 MatrixXd T(4,4);
 MatrixXd T_out(4,4);
 VectorXd quat_out(4);
 Vector3d pos_out;
 geometry_msgs::PoseStamped p;
 
 MatrixXd T_ik(4,4), T_world(4,4);
 MatrixXd R_ik(3,3);
 VectorXd quat_ik(4);

 dexrov_msgs::CartesianError ee_error_msg_r;
 ee_error_msg_r.components.resize(6);
 
 dexrov_msgs::CartesianError ee_error_msg_l;
 ee_error_msg_l.components.resize(6);
 
 geometry_msgs::PoseStamped ee_pose_msg_r;
 geometry_msgs::PoseStamped ee_pose_msg_l;
 
  
  while (ros::ok())
  {
  int dummy=sign(2);
  
  if(q_initialized){
  
  	position = GetEEPosition(q);
	orientation = GetEEQuaternion(q);

  
  	if(pose_initialized){
 		  
		  ee_error_msg_r.components[0] = des[4] - position(0);
		  ee_error_msg_r.components[1] = des[5] - position(1);
		  ee_error_msg_r.components[2] = des[6] - position(2);
		
		  quat_des << des[0], des[1], des[2], des[3];
		  quat_error = quatError(quat_des, orientation);  
		  
		  ee_error_msg_r.components[3] = quat_error(0);
		  ee_error_msg_r.components[4] = quat_error(1);
		  ee_error_msg_r.components[5] = quat_error(2);
		  
		  ee_error_msg_l.components[0] = 0;
		  ee_error_msg_l.components[1] = 0;
		  ee_error_msg_l.components[2] = 0;
		  ee_error_msg_l.components[3] = 0;
		  ee_error_msg_l.components[4] = 0;
		  ee_error_msg_l.components[5] = 0;
		 
		  ee_error_pub_r.publish(ee_error_msg_r);		// Publish end-effector position/orientation error
  		  ee_error_pub_l.publish(ee_error_msg_l);
		  
		  }
		  
		  
	R_armbase = quat2rot(orientation);	  

	T.block(0,0,3,3) = R_armbase;
	T(0,3) = value_pos(q)(0);
	T(1,3) = value_pos(q)(1);
	T(2,3) = value_pos(q)(2);
  	T.row(3) << 0,0,0,1;
  	
  	T_out = ik2armbase(T);
  	
  	quat_out = rot2quat(T.block(0,0,3,3));
  	
  	p.header.stamp = ros::Time::now();
  
  
 	p.header.frame_id = "arm_base";
  	
  	p.pose.position.x = T_out(0,3);
  	p.pose.position.y = T_out(1,3);
  	p.pose.position.z = T_out(2,3);
  	
  	p.pose.orientation.x = quat_out(0);
  	p.pose.orientation.y = quat_out(1);
  	p.pose.orientation.z = quat_out(2);
  	p.pose.orientation.w = quat_out(3);
  	
  	try{
  
  	mytf.waitForTransform("arm_base", "world", ros::Time::now(), ros::Duration(1));
  	
  
  	}
  	
  	catch(tf::TransformException e){
  		
  		usleep(1 * 1000000);
  		
  		continue;
  		
  	}
  	
  	
  	mytf.transformPose("world", p, ee_pose_msg_r);
  	
  	ee_pose_msg_l.pose.position.x = 0;
  	ee_pose_msg_l.pose.position.y = 0;
  	ee_pose_msg_l.pose.position.z = 0;
  	ee_pose_msg_l.pose.orientation.x = 0;
  	ee_pose_msg_l.pose.orientation.y = 0;
  	ee_pose_msg_l.pose.orientation.z = 0;
  	ee_pose_msg_l.pose.orientation.w = 0;
  	
	ee_pub_r.publish(ee_pose_msg_r);
	ee_pub_l.publish(ee_pose_msg_l);
	
	}	  
 
  
  ros::spinOnce();
  loop_rate.sleep();
  
}

}
