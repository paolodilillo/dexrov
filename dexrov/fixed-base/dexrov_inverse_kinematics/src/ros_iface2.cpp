#include <ros/ros.h>
#include "ExtendedTaskHierarchy.h"
#include <Eigen/Core>
#include <sensor_msgs/JointState.h>
#include <vector>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <dexrov_msgs/CartesianError.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf/exceptions.h>

using namespace std;
using namespace Eigen;


VectorXd q(6), qGazebo(6);

bool q_initialized = false;
bool pose_initialized = false;

vector<double> pos_d(3), ori_d(4), des(7);



void eeCB(const geometry_msgs::PoseStamped msg){


	Vector3d pose_in_odom;
	
	pose_in_odom(0) = msg.pose.position.x;
	pose_in_odom(1) = msg.pose.position.y;
	pose_in_odom(2) = msg.pose.position.z;
	
	Vector3d pose_in_ik;
	
	pose_in_ik = odom2ik(pose_in_odom);
	
	pos_d[0] = pose_in_ik(0);
	pos_d[1] = pose_in_ik(1);
	pos_d[2] = pose_in_ik(2);
	
	
	pose_initialized = true;

	
	
	
	
}

void jointCB(const sensor_msgs::JointState::ConstPtr& msg){

/*

Callback for acquiring the current joint positions

*/

qGazebo(0) = msg->position[0];
qGazebo(1) = msg->position[1];
qGazebo(2) = msg->position[2];
qGazebo(3) = msg->position[3];
qGazebo(4) = msg->position[4];
qGazebo(5) = msg->position[5];


q = mask_Gazebo2DH(qGazebo, qGazebo.size());

//cout << q << "\n\n";

q_initialized = true;



}


int main(int argc, char* argv[]){



 ros::init(argc, argv, "inverse_kinematics_ros_iface2");

 ros::NodeHandle n;
 tf::TransformListener mytf;
 sleep(3);
  
 
 ros::Publisher ee_pub_r = n.advertise<geometry_msgs::PoseStamped>("/arm/R/end_effector_pose", 1);
 ros::Publisher ee_pub_l = n.advertise<geometry_msgs::PoseStamped>("/arm/L/end_effector_pose", 1);
 ros::Publisher ee_error_pub_r = n.advertise<dexrov_msgs::CartesianError>("/arm/R/cartesian_error", 1);
 ros::Publisher ee_error_pub_l = n.advertise<dexrov_msgs::CartesianError>("/arm/L/cartesian_error", 1);
 
 ros::Subscriber joint_sub = n.subscribe("/arm/joint_states", 1, jointCB);
 ros::Subscriber ee_sub = n.subscribe("/cog/desired_end_effector_pose", 1, eeCB);

 ros::Rate loop_rate(1);
  
  
 Vector3d position;
 VectorXd orientation(4);
 Vector3d quat_error;
 VectorXd quat_des(4);
 
 VectorXd quat_armbase(4);
 Matrix3d R_armbase;
 MatrixXd T(4,4);
 MatrixXd T_out(4,4);
 VectorXd quat_out(4);
 Vector3d pos_out;
 geometry_msgs::PoseStamped p;
 
 MatrixXd T_ik(4,4), T_world(4,4);
 MatrixXd R_ik(3,3);
 VectorXd quat_ik(4);

 dexrov_msgs::CartesianError ee_error_msg_r;
 ee_error_msg_r.components.resize(6);
 
 dexrov_msgs::CartesianError ee_error_msg_l;
 ee_error_msg_l.components.resize(6);
 
 geometry_msgs::PoseStamped ee_pose_msg_r;
 geometry_msgs::PoseStamped ee_pose_msg_l;
 
 Vector3d position_odom;
 VectorXd quaternion_odom(4);
  
  while (ros::ok())
  {
  int dummy=sign(2);
  
  if(q_initialized){
  
  	position = GetEEPosition(q);
	orientation = GetEEQuaternion(q);

  
  	if(pose_initialized){
 		  
		  ee_error_msg_r.components[0] = pos_d[0] - position(0);
		  ee_error_msg_r.components[1] = pos_d[1] - position(1);
		  ee_error_msg_r.components[2] = pos_d[2] - position(2);
		
		  quat_des << des[0], des[1], des[2], des[3];
		  quat_error = quatError(quat_des, orientation);  
		  
		  ee_error_msg_r.components[3] = quat_error(0);
		  ee_error_msg_r.components[4] = quat_error(1);
		  ee_error_msg_r.components[5] = quat_error(2);
		  
		  ee_error_msg_l.components[0] = pos_d[0] - position(0);
		  ee_error_msg_l.components[1] = pos_d[1] - position(1);
		  ee_error_msg_l.components[2] = pos_d[2] - position(2);
		  
		  
		  ee_error_msg_l.components[3] = quat_error(0);
		  ee_error_msg_l.components[4] = quat_error(1);
		  ee_error_msg_l.components[5] = quat_error(2);
		  
		
		  ee_error_pub_r.publish(ee_error_msg_r);		// Publish end-effector position/orientation error
  		  ee_error_pub_l.publish(ee_error_msg_l);
		  
		  }
		  
	
  	
	position_odom = ik2odom(position);
	quaternion_odom = ik2odom_rotation(orientation);
  	
  	ee_pose_msg_l.pose.position.x = position_odom(0);
  	ee_pose_msg_l.pose.position.y = position_odom(1);
  	ee_pose_msg_l.pose.position.z = position_odom(2);
  	ee_pose_msg_l.pose.orientation.x = quaternion_odom(0);
  	ee_pose_msg_l.pose.orientation.y = quaternion_odom(1);
  	ee_pose_msg_l.pose.orientation.z = quaternion_odom(2);
  	ee_pose_msg_l.pose.orientation.w = quaternion_odom(3);
  	
	ee_pub_r.publish(ee_pose_msg_r);
	ee_pub_l.publish(ee_pose_msg_l);
	
	}	  
 
  
  ros::spinOnce();
  loop_rate.sleep();
  
}

}
