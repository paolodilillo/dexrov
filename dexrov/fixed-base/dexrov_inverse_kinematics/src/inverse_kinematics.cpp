/*

Code for the arm inverse kinematics computation (Position/Orientation Task)

input: 

- desired end-effector pose in arm frame
- current joint positions


output:

- desired joint velocities 
- current end-effector pose in arm frame
- current end-effector velocity in arm frame
- current pose error in arm frame

*/



#include <ros/ros.h>
#include "ExtendedTaskHierarchy.h"
#include <Eigen/Core>
#include <sensor_msgs/JointState.h>
#include <vector>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <dexrov_msgs/CartesianError.h>
#include <dexrov_msgs/Command.h>

using namespace std;
using namespace Eigen;


VectorXd q(6), qGazebo(6);

int flag;
bool q_initialized = false;
bool pose_initialized = false;
bool direct_manipulation = false;

int timer_pose = 0;
int timer_joint = 0;
double timeout = 2;

vector<double> pos_d(3), ori_d(4), des(7);

void manipulationCB(const dexrov_msgs::Command msg){


if(msg.ID == 1 && msg.command == 1){


	direct_manipulation = true;
	cout << "\nEntering teleoperation mode \n\n";

}


else if(msg.ID == 0 && msg.command == 1){


	direct_manipulation = false;
	cout << "\nEntering cognitive engine mode \n\n";
}


else{

	direct_manipulation = false;

}


}

void eeCB(const geometry_msgs::PoseStamped msg){

	Vector3d pose_in_odom;
	VectorXd quaternion_in_odom(4);
	
	pose_in_odom(0) = msg.pose.position.x;
	pose_in_odom(1) = msg.pose.position.y;
	pose_in_odom(2) = msg.pose.position.z;
	
	quaternion_in_odom(0) = msg.pose.orientation.x;
	quaternion_in_odom(1) = msg.pose.orientation.y;
	quaternion_in_odom(2) = msg.pose.orientation.z;
	quaternion_in_odom(3) = msg.pose.orientation.w;
	
	Vector3d pose_in_ik;
	VectorXd quaternion_in_ik(4);
	
	pose_in_ik = odom2ik(pose_in_odom);
	quaternion_in_ik = odom2ik_rotation(quaternion_in_odom);
	
	if(direct_manipulation == false){
	
	pos_d[0] = pose_in_ik(0);
	pos_d[1] = pose_in_ik(1);
	pos_d[2] = pose_in_ik(2);

	des[0] = quaternion_in_ik(0);
	des[1] = quaternion_in_ik(1);
	des[2] = quaternion_in_ik(2);
	des[3] = quaternion_in_ik(3);
	des[4] = pose_in_ik(0);
	des[5] = pose_in_ik(1);
	des[6] = pose_in_ik(2);
	

	timer_pose = 0;
//	pose_initialized = check_boundaries_left(pose_in_odom);
	pose_initialized = true;

	}
	
}


void direct_eeCB(const geometry_msgs::PoseStamped msg){

	Vector3d pose_in_odom;
	VectorXd quaternion_in_odom(4);
	
	pose_in_odom(0) = msg.pose.position.x;
	pose_in_odom(1) = msg.pose.position.y;
	pose_in_odom(2) = msg.pose.position.z;
	
	quaternion_in_odom(0) = msg.pose.orientation.x;
	quaternion_in_odom(1) = msg.pose.orientation.y;
	quaternion_in_odom(2) = msg.pose.orientation.z;
	quaternion_in_odom(3) = msg.pose.orientation.w;
	
	
	Vector3d pose_in_ik;
	VectorXd quaternion_in_ik(4);
	
	pose_in_ik = odom2ik(pose_in_odom);
	quaternion_in_ik = odom2ik_rotation(quaternion_in_odom);
	if(direct_manipulation == true){
	
	pos_d[0] = pose_in_ik(0);
	pos_d[1] = pose_in_ik(1);
	pos_d[2] = pose_in_ik(2);
	
	des[0] = quaternion_in_ik(0);
	des[1] = quaternion_in_ik(1);
	des[2] = quaternion_in_ik(2);
	des[3] = quaternion_in_ik(3);
	des[4] = pose_in_ik(0);
	des[5] = pose_in_ik(1);
	des[6] = pose_in_ik(2);
	
	cout << pose_in_odom << "\n\n" << quaternion_in_odom << "\n\n";

	timer_pose = 0;
	pose_initialized = true;
	
	}

	
}

void jointCB(const sensor_msgs::JointState::ConstPtr& msg){

/*

Callback for acquiring the current joint positions

*/

	qGazebo(0) = msg->position[0];
	qGazebo(1) = msg->position[2];
	qGazebo(2) = msg->position[4];
	qGazebo(3) = msg->position[6];
	qGazebo(4) = msg->position[8];
	qGazebo(5) = msg->position[10];
/*	
	qGazebo(0) = msg->position[0];
	qGazebo(1) = msg->position[1];
	qGazebo(2) = msg->position[2];
	qGazebo(3) = msg->position[3];
	qGazebo(4) = msg->position[4];
	qGazebo(5) = msg->position[5];
*/
	q = mask_Gazebo2DH_sim(qGazebo, qGazebo.size());
//	q = mask_Gazebo2DH(qGazebo, qGazebo.size());

//	cout << q << "\n\n";
	
	timer_joint = 0;
	q_initialized = true;

}


int main(int argc, char* argv[]){

  ros::init(argc, argv, "dexrov_inverse_kinematics_left");
  ros::NodeHandle n;
 
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("/arm/L/req_joint_velocity", 1);
  ros::Publisher joint_pub_sim = n.advertise<sensor_msgs::JointState>("/arm/L/desired_joint_velocity", 1);
 
  ros::Publisher ee_pose_pub = n.advertise<geometry_msgs::PoseStamped>("/arm/L/end_effector_pose_hf",1);
  ros::Publisher ee_error_pub = n.advertise<geometry_msgs::PoseStamped>("/debug/arm/L/pose_error",1);
  ros::Publisher manip_pub = n.advertise<geometry_msgs::PoseStamped>("/debug/arm/L/manipulability",1);

 ros::Subscriber joint_sub_sim = n.subscribe("/arm/joint_states", 1, jointCB);
//  ros::Subscriber joint_sub = n.subscribe("/arm/L/fbk_joint_position", 1, jointCB);
  
  ros::Subscriber ee_sub = n.subscribe("/cog_proxy/L/desired_end_effector_pose", 1, eeCB);
  ros::Subscriber direct_ee_sub = n.subscribe("/mcc/L/direct_manipulation", 1, direct_eeCB);
  ros::Subscriber manipulation_sub = n.subscribe("/mcc/manipulation_master", 1, manipulationCB);

  ros::Rate loop_rate(100);
  
  
  bool controlOrientation;
  double kp, ko;
  
  n.getParam("inverse_kinematics_left/controlOrientation",controlOrientation);
  n.getParam("inverse_kinematics_left/positionGain",kp);
  n.getParam("inverse_kinematics_left/orientationGain",ko);
  
  
  VectorXd dq(6);
  Vector3d position;
  VectorXd orientation(4);
  VectorXd qdot(6);
  VectorXd qdot_sim(6);
  geometry_msgs::PoseStamped ee_pose_msg_armbase;
  
  sensor_msgs::JointState vel_msg, vel_msg_sim;
  vel_msg.velocity.resize(6);
  vel_msg_sim.velocity.resize(6);
  
  vector<double> joint1_limit(2), joint2_limit(2),joint3_limit(2),joint4_limit(2),joint5_limit(2),joint6_limit(2); 
  
  joint1_limit[0] = 0;
  joint1_limit[1] = 3.09;

  joint2_limit[0] = -0.25;
  joint2_limit[1] = 1.15;
  
  joint3_limit[0] = -4;
  joint3_limit[1] = -1.62;
  
  joint4_limit[0] = -0.1;
  joint4_limit[1] = 5.9;
  
  joint5_limit[0] = -1.7;
  joint5_limit[1] = 1.85;
  
  joint6_limit[0] = -3;
  joint6_limit[1] = 3;
  

  MatrixXd Kt(6,6), K_joint(1,1);  
  Matrix3d K_pos;

  //////////// GAINS  ////////////////////////////
  K_pos << kp,0,0,   
           0,kp,0,
           0,0,kp;


  Kt << kp,0,0,0,0,0,
	0,kp,0,0,0,0,
	0,0,kp,0,0,0,
	0,0,0,ko,0,0,
	0,0,0,0,ko,0,
	0,0,0,0,0,ko;

  K_joint << 0.1;
  
  vector<double> par_wall(12), par_wall2(12), par_wall3(12), par_wall4(12), par_wall5(12), par_wall6(12);
MatrixXd K_wall(1,1);

K_wall << 2;


// Z FORWARD//
par_wall[0] = 0.1;
par_wall[1] = 999;

par_wall[2] = 0;	par_wall[5] = 1;	par_wall[8] = 0;
par_wall[3] = 0;	par_wall[6]= 0;		par_wall[9] = 1;
par_wall[4] = 1.2;	par_wall[7] = 1.2;	par_wall[10] = 1.2;
par_wall[11] = 2;

// Z BACKWARD//
par_wall2[0] = 0.1;
par_wall2[1] = 999;

par_wall2[2] = 0;	par_wall2[5] = 1;	par_wall2[8] = 0;
par_wall2[3] = 0.0;	par_wall2[6]= 0.0;	par_wall2[9] = 1.0;
par_wall2[4] = 0.7;	par_wall2[7] = 0.7;	par_wall2[10] = 0.7;
par_wall2[11] = 1;


// Y DOWN//
par_wall3[0] = 0.1;
par_wall3[1] = 999;

par_wall3[2] = 0;	par_wall3[5] = 1;	par_wall3[8] = 0;
par_wall3[3] = -0.0;	par_wall3[6]=  -0.0;	par_wall3[9] = 0.0;
par_wall3[4] = 0.0;	par_wall3[7] = 0.0;	par_wall3[10] = 1.0;
par_wall3[11] = 1;

// Y UP//

par_wall4[0] = 0.1;
par_wall4[1] = 999;

par_wall4[2] = 0;	par_wall4[5] = 1;	par_wall4[8] = 0;
par_wall4[3] = -0.6;	par_wall4[6]=  -0.6;	par_wall4[9] = -0.6;
par_wall4[4] = 0.0;	par_wall4[7] = 0.0;	par_wall4[10] = 1.0;
par_wall4[11] = 2;

// X left//

par_wall5[0] = 0.1;
par_wall5[1] = 999;

par_wall5[2] = -0.6;	par_wall5[5] = -0.6;	par_wall5[8] = -0.6;
par_wall5[3] = -0.0;	par_wall5[6]= 0.0;	par_wall5[9] = 1.0;
par_wall5[4] = 0.0;	par_wall5[7] = 1.0;	par_wall5[10] = 0.0;
par_wall5[11] = 2;


// X right//
par_wall6[0] = 0.1;
par_wall6[1] = 999;

par_wall6[2] = 0.05;	par_wall6[5] = 0.05;	par_wall6[8] = 0.05;
par_wall6[3] = 0.0;	par_wall6[6]=  1.0;	par_wall6[9] = 0.0;
par_wall6[4] = 0.0;	par_wall6[7] = 0.0;	par_wall6[10] = 1.0;
par_wall6[11] = 2;



vector<double> par_wall7(13);
par_wall7[0] = 0.1;
par_wall7[1] = 999;

par_wall7[2] = 0.1;	par_wall7[5] = 0.1;	par_wall7[8] = 0.1;
par_wall7[3] = 0.0;	par_wall7[6]=  1.0;	par_wall7[9] = 0.0;
par_wall7[4] = 0.0;	par_wall7[7] = 0.0;	par_wall7[10] = 1.0;
par_wall7[11] = 2;	par_wall7[12] = 2;


vector<double> par_wall8(12);
par_wall8[0] = 0.1;
par_wall8[1] = 999;

par_wall8[2] = 0.1;	par_wall8[5] = 0.1;	par_wall8[8] = 0.1;
par_wall8[3] = 0.0;	par_wall8[6]=  0.0;	par_wall8[9] = 1.0;
par_wall8[4] = 0.0;	par_wall8[7] = 1.0;	par_wall8[10] = 0.0;
par_wall8[11] = 0;

  ////////////////////////////////////////////////////

  ExtendedTaskHierarchy ext_hierarchy;
 
  Task joint1(1,K_joint, joint1_limit);
  Task joint2(2,K_joint, joint2_limit);
  Task joint3(3,K_joint, joint3_limit);
  Task joint4(4,K_joint, joint4_limit);
  Task joint5(5,K_joint, joint5_limit);
  Task joint6(6,K_joint, joint6_limit);
  
  Task wall(VIRTUAL_WALL,K_wall, par_wall);
  Task wall2(VIRTUAL_WALL,K_wall, par_wall2);
  Task wall3(VIRTUAL_WALL,K_wall, par_wall3);
  Task wall4(VIRTUAL_WALL,K_wall, par_wall4);
  Task wall5(VIRTUAL_WALL,K_wall, par_wall5);
  Task wall6(VIRTUAL_WALL,K_wall, par_wall6);
  
  Task wall_joint3(26,K_wall, par_wall7);
  Task wall_jointvirtual(27,K_wall, par_wall8);
  
  vector <double> par_manip(2);
  par_manip[0] = 0.04;
  par_manip[1] = 999;
  Task manipulability(ARM_MANIPULABILITY, K_wall, par_manip);
  
  Task ori_pos(10,Kt,des);
  Task pos(8,K_pos,pos_d);
  
  TaskHierarchy hierarchy;
  
 
//  hierarchy.insert(wall_joint3);
//  hierarchy.insert(wall_jointvirtual);
  	
  hierarchy.insert(joint1);    	
//  hierarchy.insert(joint2);
//  hierarchy.insert(joint3);
//  hierarchy.insert(joint4); 	
//  hierarchy.insert(joint5);
//  hierarchy.insert(joint6);

//hierarchy.insert(manipulability);


  hierarchy.insert(wall);
  hierarchy.insert(wall2);
  hierarchy.insert(wall3);
  hierarchy.insert(wall4);
  hierarchy.insert(wall5);
  hierarchy.insert(wall6);
 
 
 if(!controlOrientation){
 
  	hierarchy.insert(pos);
  	cout << "\n\nCONTROLLING POSITION (LEFT ARM)\n\n";
  	}
  else{
  
  	hierarchy.insert(ori_pos);
	cout << "\n\nCONTROLLING POSITION AND ORIENTATION (LEFT ARM)\n\n";
  }

  ext_hierarchy.setTaskHierarchy(hierarchy);
  
  Vector3d position_ik, position_odom;
  VectorXd quaternion_ik, quaternion_odom;
  geometry_msgs::PoseStamped pose_msg;
  geometry_msgs::PoseStamped errormsg;
  geometry_msgs::PoseStamped manipmsg;
  
  ros::Time start,end;
  
  while (ros::ok())
  {
//  int dummy=sign(2);

  if(q_initialized && pose_initialized){
  
        if(!controlOrientation){
        
  		  ext_hierarchy.setTaskParameters(8,pos_d);
		  errormsg.header.stamp = ros::Time::now();
		  errormsg.pose.position.x = ext_hierarchy.getTaskError(8,q)(0);
		  errormsg.pose.position.y = ext_hierarchy.getTaskError(8,q)(1);
		  errormsg.pose.position.z = ext_hierarchy.getTaskError(8,q)(2);

        }
	
	else{
	
		  ext_hierarchy.setTaskParameters(10,des);	
		  
		  errormsg.header.stamp = ros::Time::now();
		  errormsg.pose.position.x = ext_hierarchy.getTaskError(10,q)(0);
		  errormsg.pose.position.y = ext_hierarchy.getTaskError(10,q)(1);
		  errormsg.pose.position.z = ext_hierarchy.getTaskError(10,q)(2);
		  errormsg.pose.orientation.x = ext_hierarchy.getTaskError(10,q)(3);
		  errormsg.pose.orientation.y = ext_hierarchy.getTaskError(10,q)(4);
		  errormsg.pose.orientation.z = ext_hierarchy.getTaskError(10,q)(5);

       }
	
	start = ros::Time::now();
        dq = ext_hierarchy.computeDq(q);
 	end = ros::Time::now();
//	cout << "tempo: " << end-start << endl;
        ee_error_pub.publish(errormsg);

  }
  
  

  else{
  
	dq << 0,0,0,0,0,0;
	
  }
  
   
  dq = VelocitySaturation(dq);  
  qdot = mask_DH_Gazebo_velocity(dq,dq.size());
  qdot_sim = mask_DH_Gazebo_velocity_sim(dq,dq.size());
       
  vel_msg.velocity[0] = qdot(0);
  vel_msg.velocity[1] = qdot(1);
  vel_msg.velocity[2] = qdot(2);
  vel_msg.velocity[3] = qdot(3);
  vel_msg.velocity[4] = qdot(4);
  vel_msg.velocity[5] = qdot(5);


  vel_msg_sim.header.stamp = ros::Time::now();
  vel_msg_sim.velocity[0] = qdot_sim(0);
  vel_msg_sim.velocity[1] = qdot_sim(1);
  vel_msg_sim.velocity[2] = qdot_sim(2);
  vel_msg_sim.velocity[3] = qdot_sim(3);
  vel_msg_sim.velocity[4] = qdot_sim(4);
  vel_msg_sim.velocity[5] = qdot_sim(5);
  
  
//  joint_pub.publish(vel_msg);
  joint_pub_sim.publish(vel_msg_sim);
 		 
  timer_pose++;
  
  if(timer_pose >= timeout * 100){
  
  	pose_initialized = false;
  //	cout << "Watchdog desired pose: " << timer_pose << "\n";
  		
  }
  	
  timer_joint++;
  
  if(timer_joint >= timeout * 100){
  
  	q_initialized = false;
  	
   }

  if(q_initialized){
  
	  position_ik = GetEEPosition(q);
	  quaternion_ik = GetEEQuaternion(q);
	  
	  position_odom = ik2odom(position_ik);
	  quaternion_odom = ik2odom_rotation(quaternion_ik);
	  
	  pose_msg.header.stamp = ros::Time::now();
	  pose_msg.pose.position.x = position_odom(0);
	  pose_msg.pose.position.y = position_odom(1);
	  pose_msg.pose.position.z = position_odom(2);
	  
	  pose_msg.pose.orientation.x = quaternion_odom(0);
	  pose_msg.pose.orientation.y = quaternion_odom(1);
	  pose_msg.pose.orientation.z = quaternion_odom(2);
	  pose_msg.pose.orientation.w = quaternion_odom(3);
	   
 	  ee_pose_pub.publish(pose_msg);
 	  
// 	  cout << "LEFT   Position: " << position_odom.transpose() << "\tRPY" << (quat2rpy(quaternion_odom)).transpose() << "\n";
 	 
 	  manipmsg.header.stamp = ros::Time::now();
 	  manipmsg.pose.position.x = manipulability.taskValue(q)(0);
 	  manip_pub.publish(manipmsg);
    
  }

  
  ros::spinOnce();
  loop_rate.sleep();
    

  
  }


}
