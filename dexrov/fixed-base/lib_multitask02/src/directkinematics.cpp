#include "LLkinematics.h"


using namespace Eigen;
using namespace std;

MatrixXd directkinematics(VectorXd q){

/* 
	Direct kinematics for the Jaco2 (7 DoF) arm
	
	input:
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::MatrixXd T_out	dim: 4x4	homogeneous transform matrix from the end-effector to the base

*/

    int i=0, z=0, j=0;
    int joints = q.size();

    double *q1 = VectorXdtoarray1d(q);

    double ***T, ***T0;
    
    double **DH;
    
    if(joints == 6)
    
    	DH = readDH1(joints);
    	
    else if(joints == 7)
    
    	DH = readDH2(joints);
    	
    DH = elaborateDH(joints, q1, DH);
    T = elaborateT(DH, joints);
    T0 = elaborateT0(T, joints);

    MatrixXd T_out(4,4);
    
    for(i=0;i<4;i++)
    	for(j=0;j<4;j++)
    	
    		T_out(i,j) = T0[i][j][joints-1];
    		
    delete[] q1;
    deallocateArray3d(4,4,T);
    deallocateArray3d(4,4,T0);
    deallocateArray2d(joints,DH);
    
    
    return T_out;
}

MatrixXd directkinematics(VectorXd q, int id){

/* 
	Direct kinematics for the Jaco2 (7 DoF) arm
	
	input:
		Eigen::VectorXd q	dim: nx1	joint positions
		int id			dim: 1		joint id
		
	output:
	
		Eigen::MatrixXd T_out	dim: 4x4	homogeneous transform matrix from the link "id" to the base

*/

if(id >= q.size()){

	cout << "\n[directkinematics function]: the system has " << q.size() << " joints, requested id = " << id+1 << endl; 
	exit(0);

}


    int i=0, z=0, j=0;
    int joints = q.size();

    double *q1 = VectorXdtoarray1d(q);

    double ***T, ***T0;
    double **DH;

    if(joints == 6)
    
    	DH = readDH1(joints);
    	
    else if(joints == 7)
    
    	DH = readDH2(joints);


    DH = elaborateDH(joints, q1, DH);
    T = elaborateT(DH, joints);
    T0 = elaborateT0(T, joints);

    MatrixXd T_out(4,4);
    
    for(i=0;i<4;i++)
    	for(j=0;j<4;j++)
    	
    		T_out(i,j) = T0[i][j][id];
    		
    delete[] q1;
    deallocateArray3d(4,4,T);
    deallocateArray3d(4,4,T0);
    deallocateArray2d(joints,DH);
    

    return T_out;
}

MatrixXd directkinematics_dexrov(VectorXd q){

/* 
	Direct kinematics for the Jaco2 (7 DoF) arm
	
	input:
		Eigen::VectorXd q	dim: nx1	joint positions
		int id			dim: 1		joint id
		
	output:
	
		Eigen::MatrixXd T_out	dim: 4x4	homogeneous transform matrix from the link "id" to the base

*/



    int i=0, z=0, j=0;
    int joints = q.size();

    double *q1 = VectorXdtoarray1d(q);

    double ***T, ***T0;
    double **DH;

    DH = allocateArray2d(joints, 4);

	DH[0][0] = 0.0;
	DH[1][0] = 0.431;
	DH[2][0] = 0.0;
	DH[3][0] = 0.0;
	DH[4][0] = 0.0;
	DH[5][0] = 0.0;


	DH[0][1] = 90.0/180.0*PI;
	DH[1][1] = 0.0/180.0*PI;
	DH[2][1] = -90.0/180.0*PI;
	DH[3][1] = 90.0/180.0*PI;
	DH[4][1] = -90.0/180.0*PI;
	DH[5][1] = 0.0/180.0*PI;


	DH[0][2] =  0.3065;
	// DH[0][2] =  0.0;
	DH[1][2] =  0.0;
	DH[2][2] =  0.0;
	DH[3][2] =  0.437;
	DH[4][2] =  0.0;
	DH[5][2] =  0.1695;
	
	
	double q3[6];
	q3[0] = q1[0];
	q3[1] = q1[1] - 1.1961 + 1.57;
	q3[2] = q1[2];
	q3[3] = q1[3];
	q3[4] = q1[4];
	q3[5] = q1[5];
	
    DH = elaborateDH(joints, q3, DH);
    T = elaborateT(DH, joints);
    T0 = elaborateT0(T, joints);

    MatrixXd T_out(4,4);
    
    for(i=0;i<4;i++)
    	for(j=0;j<4;j++)
    	
    		T_out(i,j) = T0[i][j][2];
    		
    delete[] q1;
    deallocateArray3d(4,4,T);
    deallocateArray3d(4,4,T0);
    deallocateArray2d(joints,DH);
    

    return T_out;
}

