#include <iostream>
#include <Eigen/Core>
#include "LLkinematics.h"
#include <Task.h>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>


Task::Task(){

}

Task::~Task(){

//cout << "Distruttore task " << type << "\n";
}


Task::Task(int type, MatrixXd K, vector<double> params){

/*

Overloaded Task constructor:
	
	input:
	
		int type		dim: 1				type of task (see nsb.h for the definitions)
		MatrixXd K		dim: mxm (m = task dimension)	gain matrix for the inverse kinematics algorithm
		vector<double> params	dim: variable			task parameters vector

*/

this->type=type;
this->K=K;
this->params = params;
this->set_based = set_based;
this->number = rand();


if(type>=1 && type <=7){

	task_dim = 1;
	set_based = 1;
	}


if (type == 8){

	task_dim =3;
	set_based = 0;
	
	}

if(type == 9){

	task_dim =3;
	set_based = 0;
	
	}

if(type == 10){

	task_dim=6;
	set_based = 0;
	}

if(type == 11){
	
	task_dim=1;
	set_based = 1;
	
	}

if (type == 12 || type == 13 || type == 23 || type == 24 || type == 25){

	task_dim=1;
	set_based = 1;
	}
	
	
	/* TEMP */
if (type == 14){

	task_dim = 1;
	set_based = 0;

}	


if(type==15 || type == 16 || type == 18 || type==19){


	task_dim = 3;
	set_based = 0;
}

if(type == 20){

	task_dim = 2;
	set_based = 0;
}


if(type==17){

	task_dim = 6;
	set_based = 0;
}

if(type==21){

	task_dim = 1;
	set_based = 0;
}

if(type == 22){


	task_dim = 1;
	set_based = 1;
	
}

if(type == 26){


	task_dim = 1;
	set_based = 1;
	
}

if(type == 27){

	task_dim = 1;
	set_based = 1;

}



}


void Task::print(){

/*
	Print task informations
*/


cout << "\n\ntype: " << type <<"\nparameters: \n\n";

for(int i=0;i<params.size();i++)

	cout << params[i] << "\t";

cout << "\n\nGain K\n\n" << K << "\n\n";




}

int Task::getNumber(){

return this->number;

}

int Task::isSetBased(){


	return set_based;
}


int Task::getThresholdViolated(){

	return threshold_violated;
}


void Task::setThresholdViolated(int t){


threshold_violated = t;

}


int Task::getTaskDim(){

/*
	Get task dimension
*/

return this->task_dim;

}

MatrixXd Task::Jacobian(VectorXd q){

/*

	Compute task Jacobian
	
	input:
	
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::MatrixXd J 	dim: mxn	Jacobian matrix of the m-dimensional task

*/


MatrixXd J;
Vector3d eta_ee1;
Vector3d p_obst, p1, p2, p3, p_d;
int caso;
int jointID;

	MatrixXd I(7,7);
	
	MatrixXd N(7,7);
	VectorXd qt(7);
	MatrixXd JJ(6,7);
	VectorXd kl(7);
	VectorXd dummy(3);
	
switch(this->type)
{

case 1:		// joint 1 mechanical limit

	J = J_jointlimit(q,0);
	
	break;

case 2:		// joint 2 mechanical limit

	J = J_jointlimit(q,1);
	break;
case 3:		// joint 3 mechanical limit

	J = J_jointlimit(q,2);
	
	
	break;
case 4:		// joint 4 mechanical limit

	J = J_jointlimit(q,3);

	break;
case 5:		// joint 5 mechanical limit

	J = J_jointlimit(q,4);

	break;
case 6:		// joint 6 mechanical limit

	J = J_jointlimit(q,5);

	break;
case 7:		// joint 7 mechanical limit

	J = J_jointlimit(q,6);
	
	break;

case 8:		// ee position

	J = J_pos(q);
	


	break;

case 9:		// ee orientation

	J = J_ori(q);

	break;

case 10:	// ee configuration

	J = J_oripos(q);

	
	break;

case 11:	// arm manipulability


	J = J_manipulability(q);

	break;

case 12:	// obstacle avoidance at wrist

	
	
	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];
	
	J = J_obst_wrist(p_obst, q);


	break;

case 13:	// virtual wall

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	caso = params[11];

	J = J_wall(p1,p2,p3,q, caso);
	
//	I.setIdentity();
//	N = I - mypinv(J)*J;
//	qt << 1,1,1,1,1,1,1;
//	kl << 1,1,1,1,1,1,1;
//	JJ = J_pos(qt);
//	cout << JJ*mypinv(J) * 0.2 << endl << endl;
//	cout <<"Soluzione filtrata:\n" << JJ*N*kl<< "\n\n\n";
//	cout << "Soluzione non filtrata:\n" << JJ*kl << "\n\n\n";
//	cout << N << endl << endl;
	break;
	

case 14:	// fov


	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];

	J = J_fov(p1,q);

	break;
	
case 15:	// vehicle pos


	J = J_vehicle_pos(q);

	break;
	
case 16:	// vehicle ori


	J = J_vehicle_ori(q);

	break;
	
case 17:	// vehicle pos

	
	J = J_vehicle_oripos(q);
	

	break;
	
case 18:

	
	J = J_uvms_ee_pos(q);

	break;
	
case 20: 
	
	J = J_vehicle_attitude(q);
	
	break;
	
case 21:

	J = J_vehicle_altitude(q);
	
	break;
	
case 22:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	J = J_vehicle_obstacle(q, p_obst);
	break;
	
case 19:

	J = J_uvms_vehicle_pos(q);
	
	break;

case 23:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	J = J_obst_elbow(p_obst,q,3);

	break;

case 24:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	J = J_obst_elbow(p_obst,q,5);

	break;

case 25:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	J = J_obst_elbow(p_obst,q,6);

	break;
	
case 26:

        p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	jointID = params[11];
	caso = params[12];
		
	J = J_wall_joint(p1,p2,p3,q, jointID, caso);


	break;
	
case 27:

        p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	
	caso = params[11];
		
	J = J_wall_joint_virtual(p1,p2,p3,q, caso);


	break;
	
default:
	
	cout << "\n\nNo jacobian for a task of type " << this->type << "\n\n";

	exit(0);
}


return J;
}




VectorXd Task::taskValue(VectorXd q){

/*

	Compute task value.
	
	input:
		
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::VectorXd value	dim: mx1	task value
*/


VectorXd value(task_dim);

if(type==9)
value.resize(4);

if(type==10)
value.resize(7);



MatrixXd J;
Vector3d eta_ee1;
Vector3d p_obst, p1, p2, p3, p_d;
int caso;
int jointID;

switch(type){


case 1:
	
	value = value_jointlimit(q,0);
		
	
	break;

case 2:

	value = value_jointlimit(q,1);	
	
	break;

case 3:

	value = value_jointlimit(q,2);
	
	break;

case 4:

	value = value_jointlimit(q,3);
	
	break;

case 5:

	value = value_jointlimit(q,4);
	
	break;

case 6:

	value = value_jointlimit(q,5);
	
	break;

case 7:

	value = value_jointlimit(q,6);	
	
	break;

case 8:

	value = value_pos(q);

	break;


case 9:

	value = value_ori(q);
	


	break;
	
case 10:

	value = value_oripos(q);


	
	break;
	
	

case 11:
	
	value = value_manipulability(q);

	break;

case 12:

	
	
	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	value = value_obst_wrist(p_obst, q);
	
	break;

case 13:

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	
	
	value = value_wall(p1,p2,p3,q);
	
	break;
	
	
case 14:

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	value = value_fov(p1,q);

	break;

case 15:


	value = value_vehicle_pos(q);

	break;


case 16:


	value = value_vehicle_ori(q);

	break;


case 17:


	value = value_vehicle_oripos(q);

	break;
	
case 20: 

	value = value_vehicle_attitude(q);
	
	break;
	
case 21:

	value = value_vehicle_altitude(q);
	
	break;
	
case 22:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];
	
	value = value_vehicle_obstacle(q, p_obst);
	break;

case 18:



	value = value_uvms_ee_pos(q);
	
	break;
	
case 19:

	value = value_uvms_vehicle_pos(q);
	
	break;

case 23:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	value = value_obst_elbow(p_obst, q,3);
	break;

case 24:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	value = value_obst_elbow(p_obst, q,5);
	break;

case 25:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	value = value_obst_elbow(p_obst, q,6);
	break;
	
case 26:

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	jointID = params[11];
	caso = params[12];
	
	value = value_wall_joint(p1,p2,p3,q, jointID, caso);


	break;
	
	
case 27:

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	
	
	value = value_wall_joint_virtual(p1,p2,p3,q);
	break;

}




 return value;
}




VectorXd Task::Error(VectorXd q){

/*
	Compute task error.
	
	input:
		
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::VectorXd error	dim: mx1	task error
*/


VectorXd error(task_dim);

Vector3d eta_ee1;
VectorXd ee_quat(4), quat_d(4);
Vector3d p_obst, p1,p2,p3, p_d;
Matrix3d R;
Vector3d rpy,pos_d, pos_d_body;
int jointID, caso;


switch(type)
{
case 1:
	
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,0)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,0)(0);
	
	
	break;

case 2:

		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,1)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,1)(0);

	break;

case 3:
	
	
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,2)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,2)(0);

	break;

case 4:
	
	

		
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,3)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,3)(0);

	
	break;
	
case 5:

	

		
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,4)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,4)(0);

	

		
	break;

case 6:

	

		
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,5)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,5)(0);

	
	break;

case 7:

	

	
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,6)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,6)(0);

	
	
	break;

case 8:

	
	eta_ee1 = value_pos(q);
  
	error(0) = params[0] - eta_ee1(0);
	error(1) = params[1] - eta_ee1(1);
	error(2) = params[2] - eta_ee1(2);	
	

	break;


case 9:


	ee_quat = value_ori(q);   //quaternion

	quat_d(0) = params[0];
	quat_d(1) = params[1];
	quat_d(2) = params[2];
	quat_d(3) = params[3];


	error = quatError(quat_d,ee_quat);
	
	

	break;

case 10: 


      
        eta_ee1 = value_pos(q);
        ee_quat = value_ori(q);
         
        quat_d(0) = params[0];
        quat_d(1) = params[1];
        quat_d(2) = params[2];
        quat_d(3) = params[3];

	error(0) = params[4] - eta_ee1(0);
	error(1) = params[5] - eta_ee1(1);
	error(2) = params[6] - eta_ee1(2);	


	error.tail(3) = quatError(quat_d,ee_quat);

	
	break;

case 11:

	error(0) = params[0] - 0.01 - value_manipulability(q)(0);
	
	break;

case 12:

	
	
	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	

	error(0) = params[0] -  0.05 - taskValue(q)(0);

	
	break;

	
case 13:


	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	

	
	error(0) = params[0] - 0.05 - value_wall(p1,p2,p3,q)(0);

	
	break;
	
case 14:

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];

	
	error(0) = params[0] - value_fov(p1,q)(0); 
	
	break;
	
case 15:

	
	pos_d << params[0], params[1], params[2];
	
	error = pos_d - value_vehicle_pos(q);
	

	
	
	break;
	
case 16:

	error(0) = params[0] - value_vehicle_ori(q)(2);
	error(1) = params[1] - value_vehicle_ori(q)(1);
	error(2) = params[2] - value_vehicle_ori(q)(0); 
	
	break;
	
case 17:

	

	pos_d << params[0], params[1], params[2];
	
	
	error.head(3) = pos_d - value_vehicle_oripos(q).head(3);
	
	
	error(3) = params[3] - value_vehicle_oripos(q)(3); 
	error(4) = params[4] - value_vehicle_oripos(q)(4); 
	error(5) = params[5] - value_vehicle_oripos(q)(5); 
	 
	 	
	break;
	
case 20: 



	error(0) = params[0] - value_vehicle_attitude(q)(0);
	error(1) = params[1] - value_vehicle_attitude(q)(1);
	

	break;
	
case 21:


	error(0) = params[0] - params[1];//value_vehicle_altitude(q)(0);
	
	break;
	
case 22:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];
	
	error(0) = params[0] - 0.05 - value_vehicle_obstacle(q, p_obst)(0);
	
	break;
	
case 18:

	
	error(0) = params[0] - value_uvms_ee_pos(q)(0);
	error(1) = params[1] - value_uvms_ee_pos(q)(1);
	error(2) = params[2] - value_uvms_ee_pos(q)(2);
	 
	break;
	
case 19:

	error(0) = params[0] - value_uvms_vehicle_pos(q)(0);
	error(1) = params[1] - value_uvms_vehicle_pos(q)(1);
	error(2) = params[2] - value_uvms_vehicle_pos(q)(2);
	
	
	break;

case 23:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	error(0) = params[0] -  0.05 - taskValue(q)(0);

	break;

case 24:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	error(0) = params[0] -  0.05 - taskValue(q)(0);

	break;

case 25:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	error(0) = params[0] -  0.05 - taskValue(q)(0);

	break;
	
	
case 26:


	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	jointID = params[11];
	caso = params[12];
	error(0) = params[0] - 0.02 - value_wall_joint(p1,p2,p3,q,jointID, caso)(0);

	
	break;
	
case 27:


	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	
	error(0) = params[0] - 0.05 - value_wall_joint_virtual(p1,p2,p3,q)(0);

	
	break;

}

	

return error;

}


MatrixXd Task::NullJ(VectorXd q){

/*
	Compute the null of the task Jacobian matrix.
	
	input:
	
		Eigen::VectorXd q 	dim: nx1	joint positions
		
	output:
	
		Eigen::MatrixXd nullJ	dim: nxn	null of the task Jacobian matrix

*/

int joints = q.size();
MatrixXd J = Jacobian(q);
MatrixXd nullJ(joints,joints);
MatrixXd I(joints,joints);

I.setIdentity();


MatrixXd W(joints,joints);
W.setIdentity();

double error_norm = Error(q).norm(); 
double max_out = 2;
int flag_sigma = 0;
double d, l, sigma_min;
int algorithm = 2;


//nullJ = I - (mypinv1(J, W, algorithm, error_norm,  max_out, flag_sigma, d, l, sigma_min)*J);
nullJ = I - dls_pinv(J,W,error_norm, max_out, flag_sigma) * J;
	
//nullJ = I - (mypinv(J)*J);
return nullJ;
}


VectorXd Task::computeSolution(VectorXd q){

/*

	Compute the target joint velocities that fulfill the task
	
	input:
		
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::VectorXd dq	dim: nx1	desired joint velocities
*/


int joints = q.size();
VectorXd dq(joints);


MatrixXd J = Jacobian(q);



MatrixXd W(joints,joints);
W.setIdentity();



double error_norm = Error(q).norm(); 

double max_out = 20;
int flag_sigma = 0;
double d, l, sigma_min;
int algorithm = 2;

double threshold = 0.01;
double lambda = 0.001;


//dq = dls_pinv(J,W,error_norm, max_out, flag_sigma) * K * Error(q);

//dq = dls_pinv(Jac,W,error_norm, max_out, flag_sigma) * K * err;

dq = eig_pinv(J, threshold, lambda) * K * Error(q);
//dq = mypinv(J) * K * Error(q);


return dq;
}

int Task::getType(){

/*
	Get the task type
*/


	return this->type;

}

MatrixXd Task::getK(){

/*

	Get the task gain
*/


	return this->K;
}



vector<double> Task::getParams(){

/*

	Get the task parameters vector
*/


	return params;
}

void Task::setParams(vector<double> p){

/*

	Set the task parameters vector
*/

	params=p;
}

void Task::initJ(VectorXd q){

	
	this->Jac = Jacobian(q);
	this->err = Error(q);
	
}

void Task::setJ(VectorXd q){
	
	cout << Jac << "\n\n";
	
}
