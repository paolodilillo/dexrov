#include "ros/ros.h"
#include <geometry_msgs/PoseStamped.h>
#include <Eigen/Core>

using namespace Eigen;
using namespace std;

Vector3d position;
bool init = false;

void chatterCallback(const geometry_msgs::PoseStamped msg)
{
 
 if(init == false){
 
 	position(0) = msg.pose.position.x;
 	position(1) = msg.pose.position.y;
 	position(2) = msg.pose.position.z;
 	
 	init = true;
 }
 
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "test02");

  ros::NodeHandle n;

  ros::Publisher chatter_pub = n.advertise<geometry_msgs::PoseStamped>("/cog_proxy/L/desired_end_effector_pose", 1);
  ros::Subscriber sub = n.subscribe("/arm/L/end_effector_pose_hf", 1, chatterCallback);
  ros::Rate loop_rate(100);

  geometry_msgs::PoseStamped msg;
  
  double a = 0.1, omega = 0.4, t= 0.0, ts = 0.01;
  
  while (ros::ok())
  {
   
    msg.pose.position.x = position(0);
    msg.pose.position.y = position(1) + a * cos(omega*t);
    msg.pose.position.z = position(2) + a *sin(omega*t);
    msg.pose.orientation.x = 0.5;
    msg.pose.orientation.y = 0.5;
    msg.pose.orientation.z = 0.5;
    msg.pose.orientation.w = 0.5;
    chatter_pub.publish(msg);
    
    t += ts;

    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
