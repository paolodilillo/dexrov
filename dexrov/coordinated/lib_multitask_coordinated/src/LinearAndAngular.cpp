#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include "LLkinematics.h"

using namespace std;

const int Japp_row = 6;

/* Auxiliary function for the Jacobian matrix computation */

double *LinearAndAngular(double **p_J, double **z, int joint){

	double *Japp = new double[Japp_row];
	
	Japp[0] = z[1][joint]*p_J[2][joint] - z[2][joint]*p_J[1][joint];
	Japp[1] = z[2][joint]*p_J[0][joint] - z[0][joint]*p_J[2][joint];
	Japp[2] = z[0][joint]*p_J[1][joint] - z[1][joint]*p_J[0][joint];
	
	Japp[3] = z[0][joint];
	Japp[4] = z[1][joint];
	Japp[5] = z[2][joint];
	

	return Japp;

}

