#include "LLkinematics.h"

VectorXd value_oripos(VectorXd q){

/* Value of the end-effector configuration task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	oripos		dim: 7			vector stacking [position, quaternion]
	

*/



MatrixXd T(4,4);

T = directkinematics(q);

Vector3d pos;
VectorXd ori(4);
VectorXd oripos(7);
Matrix3d R;

pos = T.block(0,3,3,1);

R = T.block(0,0,3,3);
ori = rot2quat(R);

oripos.head(3) = pos;
oripos.tail(4) = ori;

return oripos;

}

VectorXd value_pos(VectorXd q){

/* Value of the end-effector position task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	pos		dim: 3			e-e position
	

*/


MatrixXd T(4,4);

T = directkinematics(q);


Vector3d pos;

pos = T.block(0,3,3,1);

return pos;

}

VectorXd value_ori(VectorXd q){

/* Value of the end-effector orientation task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	ori		dim: 4			e-e quaternion
	

*/


MatrixXd T(4,4);
Matrix3d R;
VectorXd ori(4);

T = directkinematics(q);

R = T.block(0,0,3,3);

ori = rot2quat(R);

return ori;


}

VectorXd value_obst_wrist(Vector3d p_obst, VectorXd q){

/* Value of the end-effector obstacle avoidance at wrist task.

 input: 
 	
 	Vector3d p_obst			dim: 3x1		obstacle position
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	value		dim: 1			distance between the obstacle and the wrist
	

*/


MatrixXd T(4,4);
T = directkinematics(q);
Vector3d pos;

pos = T.block(0,3,3,1);
VectorXd value(1);

value(0) = sqrt((p_obst - pos).transpose() * (p_obst - pos));

return value;
}

VectorXd value_obst_elbow(Vector3d p_obst, VectorXd q, int id){

/* Value of the end-effector obstacle avoidance at elbow task.

 input: 
 	
 	Vector3d p_obst			dim: 3x1		obstacle position
	Eigen::VectorXd q		dim: nx1		joint positions
	int id				dim: 1			index of the elbow joint
	
 output:
 
 	Eigen::VectorXd	value		dim: 1			distance between the obstacle and the elbow
	

*/


MatrixXd T(4,4);
T = directkinematics(q, id);
Vector3d pos;

pos = T.block(0,3,3,1);
VectorXd value(1);

value(0) = sqrt((p_obst - pos).transpose() * (p_obst - pos));

return value;

}

VectorXd value_jointlimit(VectorXd q, int id){

/* Value of the joint limit task.

 input: 
 	
	Eigen::VectorXd q		dim: nx1		joint positions
	int id				dim: 1			index of the joint
	
 output:
 
 	Eigen::VectorXd	value		dim: 1			joint value
	

*/


VectorXd value(1);

if(id >= q.size()){

	cout << "\n[value_jointlimit function]: the system has " << q.size() << " joints, requested id = " << id+1 << endl; 
	exit(0);

}


value(0) = q(id);

return value;

}


VectorXd value_manipulability_coordinated(Vector3d vehicle_pos, Matrix3d vehicle_rot,VectorXd q){

/* Task value of the arm manipulability  task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	VectorXd w			dim: 1			manipulability measure
	

*/


        int i=0, j=0, joints = q.size();
        MatrixXd J(6,joints);
        J = J_oripos_vehicle_manipulator(vehicle_pos, vehicle_rot,q.tail(6));
        
        MatrixXd Jac(6,joints);

        
        
        double det = (J*J.transpose()).determinant();
        VectorXd  w(1);
        w(0)	 = sqrt(det);
        return w;
    }

VectorXd value_manipulability(VectorXd q){

/* Task value of the arm manipulability  task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	VectorXd w			dim: 1			manipulability measure
	

*/

        int i=0, j=0, joints = q.size();
        MatrixXd J(6,joints);
        
       
        J = J_oripos(q);
        MatrixXd Jac(6,joints);

        

        double det = (J*J.transpose()).determinant();
        VectorXd  w(1);
        w(0)	 = sqrt(det);
        return w;
    }

    
VectorXd value_wall(Vector3d p1, Vector3d p2, Vector3d p3, Vector3d p_d, VectorXd q){

/* Value of the virtual plane task.

 input: 
 
 	Eigen::Vector3d p1		dim: 3x1		x,y,z coordinates of a point belonging to the plane
 	Eigen::Vector3d p2		dim: 3x1		x,y,z coordinates of a point belonging to the plane
 	Eigen::Vector3d p3		dim: 3x1		x,y,z coordinates of a point belonging to the plane
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	value		dim: 1			distance between the wall and the end-effector	

*/


MatrixXd T(4, 4);
Vector3d pos;
VectorXd value(1);
 
T = directkinematics(q);
pos = T.block(0,3,3,1);

Vector3d plane_normal;
plane_normal << ((p2-p1).cross(p3-p1)) / ((p2-p1).cross(p3-p1)).norm();

value(0) = abs(plane_normal.transpose() *(pos - p2));



return value;

}



VectorXd value_fov(Vector3d p, VectorXd q){


VectorXd value(1);

MatrixXd T = directkinematics(q);
Matrix3d R = T.block(0,0,3,3);
Vector3d p_ee = T.block(0,3,3,1);
VectorXd axis = R2axis(R);
Vector3d a;

a << axis(1), axis(2), axis(3);

Vector3d ad;

ad = (p - p_ee) / (p - p_ee).norm();


value(0) = sqrt( (ad - a).transpose() * (ad - a));

return value;

}


VectorXd value_vehicle_pos(VectorXd q){

Vector3d value;

Vector3d rpy, pos;
rpy << q(5), q(4), q(3);

Matrix3d R = rpy2rot(rpy);
pos << q(0), q(1), q(2);


value = pos;


return value;


}


VectorXd value_vehicle_ori(VectorXd q){

Vector3d value;

value << q(3), q(4), q(5);

return value;


}


VectorXd value_vehicle_oripos(VectorXd q){

VectorXd value(6);

Vector3d rpy, pos;
rpy << q(5), q(4), q(3);

Matrix3d R = rpy2rot(rpy);
pos << q(0), q(1), q(2);


value.head(3) = value_vehicle_pos(q);

value.tail(3) = value_vehicle_ori(q);

return value;


}

VectorXd value_vehicle_attitude(VectorXd q){

Vector3d value;

value << q(5), q(4);

return value;

}

VectorXd value_vehicle_altitude(VectorXd q){

Vector3d value;

value << q(2);

return value;

}


VectorXd value_vehicle_obstacle(VectorXd q,Vector3d  p_obst){

VectorXd value(1);

value(0) = sqrt((p_obst - value_vehicle_pos(q)).transpose() * (p_obst - value_vehicle_pos(q)));

return value;

}

VectorXd value_uvms_ee_pos(VectorXd q){

int arm_joints = q.size() - 6;
int joints = q.size();

VectorXd value(3);

MatrixXd T_arm(4,4), T_out(4,4), T_vehicle(4,4), T_tran(4,4);
Matrix3d R_vehicle;
Vector3d rpy;

T_arm = directkinematics(q.head(arm_joints));

T_tran = T_uvms();

rpy << q( q.size() - 1), q( q.size() - 2), q( q.size() - 3);
R_vehicle = rpy2rot(rpy);
T_vehicle.block(0,0,3,3) = R_vehicle;
T_vehicle.block(0,3,3,1) << q(q.size() - 6), q(q.size() - 5), q(q.size() - 4);
T_vehicle.row(3) << 0,0,0,1; 

T_out = T_vehicle * T_tran * T_arm;

value = T_out.block(0,3,3,1);


return value;

}


VectorXd value_uvms_vehicle_pos(VectorXd q){


int arm_joints = q.size() - 6;
int joints = q.size();


Vector3d value;

Vector3d rpy, pos;
rpy << q( q.size() - 1), q( q.size() - 2), q( q.size() - 3);

Matrix3d R = rpy2rot(rpy);
pos << q(q.size() - 6), q(q.size() - 5), q(q.size() - 4);


value = R*pos;



return value;

}


VectorXd value_pos_arm_floating_base(Vector3d vehicle_p,Matrix3d vehicle_rot,VectorXd q, int arm_id){
    /*
        EE arm_id position for the Seville sys quadrotor (4 DoF) with dual arm (4 DoF/arm)

        input:
            Eigen::Vector3d vehicle_p	dim: 3x1	vehicle positions
            Eigen::Matrix3d vehicle_rot	dim: 3x3	vehicle rotational matrix
            Eigen::VectorXd q           dim: nx1	joint positions
            int             arm_id      dim 1       1=arm 1, 2=arm 2

        output:

            Eigen::Vector3d ee_pos      dim: 3x1    end-effecor arm_id position

        Author: E.Cataldi, 2017, SevilleExp

    */
   VectorXd pos(3);
   Matrix4d T = directkinematics(vehicle_p,vehicle_rot,q,arm_id);
   pos = T.block(0,3,3,1);


   return pos;
}

VectorXd value_ori_arm_floating_base(Vector3d vehicle_p,Matrix3d vehicle_rot,VectorXd q, int arm_id){
    /*
        EE arm_id orientation for the Seville sys quadrotor (4 DoF) with dual arm (4 DoF/arm)

        input:
            Eigen::Vector3d vehicle_p	dim: 3x1	vehicle positions
            Eigen::Matrix3d vehicle_rot	dim: 3x3	vehicle rotational matrix
            Eigen::VectorXd q           dim: nx1	joint positions
            int             arm_id      dim 1       1=arm 1, 2=arm 2

        output:

            Eigen::Vector4d ee_ori     dim: 4x1    end-effecor arm_id quaternion

        Author: E.Cataldi, 2017, SevilleExp

    */
    Matrix3d R;
    VectorXd ori(4);

    Matrix4d T = directkinematics(vehicle_p,vehicle_rot,q,arm_id);

    R = T.block(0,0,3,3);
    //if (arm_id==1)
    //    std::cout<<"value_ori Rot_ee \n"<<R<<std::endl;

    ori = rot2quat(R);

    return ori;
}

VectorXd value_oripos_arm_floating_base(Vector3d vehicle_p,Matrix3d vehicle_rot,VectorXd q, int arm_id){

VectorXd value(7);

value.head(3) = value_pos_arm_floating_base(vehicle_p, vehicle_rot, q, arm_id);
value.tail(4) = value_ori_arm_floating_base(vehicle_p, vehicle_rot, q, arm_id);




return value;

}




