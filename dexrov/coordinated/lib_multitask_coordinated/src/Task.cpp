#include <iostream>
#include <Eigen/Core>
#include "LLkinematics.h"
#include <Task.h>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>


Task::Task(){

}

Task::~Task(){

//cout << "Distruttore task " << type << "\n";
}


Task::Task(int type, MatrixXd K, vector<double> params,vector<double> paramsState){

/*

Overloaded Task constructor:
	
	input:
	
		int type		dim: 1				type of task (see nsb.h for the definitions)
		MatrixXd K		dim: mxm (m = task dimension)	gain matrix for the inverse kinematics algorithm
		vector<double> params	dim: variable			task parameters vector

*/

this->type=type;
this->K=K;
this->params = params;
this->set_based = set_based;
this->paramsState = paramsState;
this->number = rand();

if(type>=1 && type <=7){

	task_dim = 1;
	set_based = 1;
	}


if (type == 8){

	task_dim =3;
	set_based = 0;
	
	}

if(type == 9){

	task_dim =3;
	set_based = 0;
	
	}

if(type == 10){

	task_dim=6;
	set_based = 0;
	}

if(type == 11){
	
	task_dim=1;
	set_based = 1;
	
	}

if (type == 12 || type == 13 ){

	task_dim=1;
	set_based = 1;
	}
	
	
	/* TEMP */
if (type == 14){

	task_dim = 1;
	set_based = 0;

}	


if(type==15 || type == 16 || type == 18 || type==19){


	task_dim = 3;
	set_based = 0;
}

if(type == 16){

	task_dim = 1;
	set_based = 0;

}

if(type == 20){

	task_dim = 2;
	set_based = 0;
}


if(type==17){

	task_dim = 6;
	set_based = 0;
}

if(type==21){

	task_dim = 1;
	set_based = 0;
}

if(type == 22){


	task_dim = 1;
	set_based = 1;
	
}


if(type == 23){		// vehicle-manipulator oripos

	task_dim = 6;
	set_based = 0;

}

if(type == 24){		// vehicle-manipulator pos

	task_dim = 3;
	set_based = 0;

}

if(type == 25){		// vehicle-manipulator ori

	task_dim = 3;
	set_based = 0;

}


if(type == 26){		// vehicle-manipulator manipulability

	task_dim = 1;
	set_based = 1;

}

if(type == 27){		// vehicle-manipulator oripos only manipulator

	task_dim = 6;
	set_based = 0;
}


}


Task::Task(int type, MatrixXd K, vector<double> params,vector<double> paramsState, VectorXd q){

/*

Overloaded Task constructor:
	
	input:
	
		int type		dim: 1				type of task (see nsb.h for the definitions)
		MatrixXd K		dim: mxm (m = task dimension)	gain matrix for the inverse kinematics algorithm
		vector<double> params	dim: variable			task parameters vector

*/

this->type=type;
this->K=K;
this->params = params;
this->set_based = set_based;
this->paramsState = paramsState;
this->number = rand();

if(type>=1 && type <=7){

	task_dim = 1;
	set_based = 1;
	}


if (type == 8){

	task_dim =3;
	set_based = 0;
	
	}

if(type == 9){

	task_dim =3;
	set_based = 0;
	
	}

if(type == 10){

	task_dim=6;
	set_based = 0;
	}

if(type == 11){
	
	task_dim=1;
	set_based = 1;
	
	}

if (type == 12 || type == 13 ){

	task_dim=1;
	set_based = 1;
	}
	
	
	/* TEMP */
if (type == 14){

	task_dim = 1;
	set_based = 0;

}	


if(type==15 || type == 16 || type == 18 || type==19){


	task_dim = 3;
	set_based = 0;
}

if(type == 16){

	task_dim = 1;
	set_based = 0;

}

if(type == 20){

	task_dim = 2;
	set_based = 0;
}


if(type==17){

	task_dim = 6;
	set_based = 0;
}

if(type==21){

	task_dim = 1;
	set_based = 0;
}

if(type == 22){


	task_dim = 1;
	set_based = 1;
	
}


if(type == 23){		// vehicle-manipulator oripos

	task_dim = 6;
	set_based = 0;

}

if(type == 24){		// vehicle-manipulator pos

	task_dim = 3;
	set_based = 0;

}

if(type == 25){		// vehicle-manipulator ori

	task_dim = 3;
	set_based = 0;

}


if(type == 26){		// vehicle-manipulator manipulability

	task_dim = 1;
	set_based = 1;

}

if(type == 27){		// vehicle-manipulator oripos only manipulator

	task_dim = 6;
	set_based = 0;
}

J = Jacobian(q);


}


void Task::print(){

/*
	Print task informations
*/


cout << "\n\ntype: " << type <<"\nparameters: \n\n";

for(int i=0;i<params.size();i++)

	cout << params[i] << "\t";

cout << "\n\nGain K\n\n" << K << "\n\n";




}

int Task::isSetBased(){


	return set_based;
}


int Task::getThresholdViolated(){

	return threshold_violated;
}


void Task::setThresholdViolated(int t){


threshold_violated = t;

}


int Task::getTaskDim(){

/*
	Get task dimension
*/

return this->task_dim;

}

MatrixXd Task::Jacobian(VectorXd q){

/*

	Compute task Jacobian
	
	input:
	
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::MatrixXd J 	dim: mxn	Jacobian matrix of the m-dimensional task

*/


MatrixXd J;
Vector3d eta_ee1;
Vector3d p_obst, p1, p2, p3, p_d;

VectorXd qtail(6);
Matrix3d dummyRot;
Vector3d qhead;
VectorXd qtemp(6);

	MatrixXd I(7,7);
	
	MatrixXd N(7,7);
	VectorXd qt(7);
	MatrixXd JJ(6,7);
	VectorXd kl(7);
	VectorXd dummy(3);
	
switch(this->type)
{

case 1:		// joint 1 mechanical limit

	J = J_jointlimit(q,4);
	
	break;

case 2:		// joint 2 mechanical limit

	J = J_jointlimit(q,5);
	break;
case 3:		// joint 3 mechanical limit

	J = J_jointlimit(q,6);
	
	
	break;
case 4:		// joint 4 mechanical limit

	J = J_jointlimit(q,7);

	break;
case 5:		// joint 5 mechanical limit

	J = J_jointlimit(q,8);

	break;
case 6:		// joint 6 mechanical limit

	J = J_jointlimit(q,9);

	break;
case 7:		// joint 7 mechanical limit

	J = J_jointlimit(q,10);
	
	break;

case 8:		// ee position

	J = J_pos(q);
	


	break;

case 9:		// ee orientation

	J = J_ori(q);

	break;

case 10:	// ee configuration

	J = J_oripos(q);

	
	break;

case 11:	// arm manipulability

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);

	J.resize(1,10);
	J.block(0,0,1,4) = MatrixXd::Zero(1,4);
	J.block(0,4,1,6) = J_manipulability(qtail);

	break;

case 12:	// obstacle avoidance at wrist

	
	
	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];
	
	J = J_obst_wrist(p_obst, q);


	break;

case 13:	// virtual wall


qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	p_d(0) = params[11];
	p_d(1) = params[12];
	p_d(2) = params[13];


	J.resize(1,10);
	J.block(0,0,1,4) = MatrixXd::Zero(1,4);
	
	J.block(0,4,1,6) = J_wall(p1,p2,p3,p_d,qtail);
	
	
	
//	I.setIdentity();
//	N = I - mypinv(J)*J;
//	qt << 1,1,1,1,1,1,1;
//	kl << 1,1,1,1,1,1,1;
//	JJ = J_pos(qt);
//	cout << JJ*mypinv(J) * 0.2 << endl << endl;
//	cout <<"Soluzione filtrata:\n" << JJ*N*kl<< "\n\n\n";
//	cout << "Soluzione non filtrata:\n" << JJ*kl << "\n\n\n";
//	cout << N << endl << endl;
	break;
	

case 14:	// fov


	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];

	J = J_fov(p1,q);

	break;
	
case 15:	// vehicle pos

	
	qtemp(0) = paramsState[9];
	qtemp(1) = paramsState[10];
	qtemp(2) = paramsState[11];
	qtemp(3) = paramsState[12];
	qtemp(4) = paramsState[13];
	qtemp(5) = paramsState[14];
	
	J.resize(3,10);
	
	J.block(0,0,3,3) = J_vehicle_pos(qtemp).block(0,0,3,3);
	
	J.block(0,3,3,7) = MatrixXd::Zero(3,7);
	

	break;
	
case 16:	// vehicle ori
/*
	qtemp(0) = paramsState[9];
	qtemp(1) = paramsState[10];
	qtemp(2) = paramsState[11];
	qtemp(3) = paramsState[12];
	qtemp(4) = paramsState[13];
	qtemp(5) = paramsState[14];
	
	J.resize(1,10);
	J.block(0,3,1,1) = J_vehicle_ori(qtemp).block(0,2,1,1);
	J.block(0,0,1,3) = MatrixXd::Zero(1,3);
	J.block(0,4,1,7) = MatrixXd::Zero(1,7);
	
	
*/	
	J.resize(1,10);
	J << 0,0,0,1,0,0,0,0,0,0;
	
	break;
	
case 17:	// vehicle pos

	
	J = J_vehicle_oripos(q);

	break;
	
case 18:

	
	J = J_uvms_ee_pos(q);

	break;
	
case 20: 
	
	J = J_vehicle_attitude(q);
	
	break;
	
case 21:

	J = J_vehicle_altitude(q);
	
	break;
	
case 22:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];
	
	J.resize(1,10);
	

	J.block(0,0,1,3) = J_vehicle_obstacle(q, p_obst).block(0,0,1,3);
	J.block(0,3,1,7) = MatrixXd::Zero(1,7);
	break;
	
case 19:

	J = J_uvms_vehicle_pos(q);
	
	break;

case 23:

    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];


qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);

	
	J =  J_oripos_vehicle_manipulator(qhead,dummyRot,qtail);
	

	break;
	
case 24:


    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];




qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);



	J = J_pos_vehicle_manipulator(q.head(3),dummyRot,qtail);


	break;


case 25:



    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];


qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);


J = J_ori_vehicle_manipulator(qhead,dummyRot,qtail);

	break;
	
case 26:
	
	 dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];


qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);

	
       
	J = J_manipulability_coordinated(qhead, dummyRot, q);

	break;
	
	

case 27:


 dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];

	J = J_oripos_vehicle_manipulator_man(dummyRot,q);

	break;

default:
	
	cout << "\n\nNo jacobian for a task of type " << this->type << "\n\n";

	exit(0);
}


return J;
}




VectorXd Task::taskValue(VectorXd q){

/*

	Compute task value.
	
	input:
		
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::VectorXd value	dim: mx1	task value
*/


VectorXd value(task_dim);

if(type==9)
value.resize(4);

if(type==10 || type == 23)
value.resize(7);

if(type == 25)
	value.resize(4);
	
if(type == 27)
	value.resize(7);



MatrixXd J;
Vector3d eta_ee1;
Vector3d p_obst, p1, p2, p3, p_d;
Matrix3d dummyRot;
VectorXd qtail(6);
Vector3d qhead;

switch(type){


case 1:
	
	value = value_jointlimit(q,4);
		
	
	break;

case 2:

	value = value_jointlimit(q,5);	
	
	break;

case 3:

	value = value_jointlimit(q,6);
	
	break;

case 4:

	value = value_jointlimit(q,7);
	
	break;

case 5:

	value = value_jointlimit(q,8);
	
	break;

case 6:

	value = value_jointlimit(q,9);
	
	break;

case 7:

	value = value_jointlimit(q,10);	
	
	break;

case 8:

	value = value_pos(q);

	break;


case 9:

	value = value_ori(q);
	


	break;
	
case 10:

	value = value_oripos(q);


	
	break;
	
	

case 11:
	qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);

	value = value_manipulability(qtail);

	break;

case 12:

	
	
	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	value = value_obst_wrist(p_obst, q);
	
	break;

case 13:


qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	p_d(0) = params[11];
	p_d(1) = params[12];
	p_d(2) = params[13];

	value = value_wall(p1,p2,p3,p_d,qtail);
	
	break;
	
	
case 14:

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	value = value_fov(p1,q);

	break;

case 15:


	value = value_vehicle_pos(q);

	break;


case 16:


//	value = value_vehicle_ori(q);
	value(0) = paramsState[14];
	break;


case 17:


	value = value_vehicle_oripos(q);

	break;
	
case 20: 

	value = value_vehicle_attitude(q);
	
	break;
	
case 21:

	value = value_vehicle_altitude(q);
	
	break;
	
case 22:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];
	
	value = value_vehicle_obstacle(q, p_obst);
	break;

case 18:



	value = value_uvms_ee_pos(q);
	
	break;
	
case 19:

	value = value_uvms_vehicle_pos(q);
	
	break;


case 23:

//for(int h=0; h < 9; h++)
//cout << paramsState[h] << "\n";

    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];
	

qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);


   value = value_oripos_arm_floating_base(q.head(3), dummyRot,qtail,1);

    break;

case 24:


    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];
	

qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);


value = value_pos_arm_floating_base(q.head(3), dummyRot,qtail,1);

	break;
	
	
case 25:



    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];
	

qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);




value = value_ori_arm_floating_base(qhead, dummyRot,qtail,1);



	break;
	
case 26:

qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);


qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);

    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];
    



	value = value_manipulability_coordinated(qhead, dummyRot,qtail);
	
	break;
	
case 27:


    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];
	

qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);

	value = value_oripos_arm_floating_base(q.head(3), dummyRot,qtail,1);
	break;

}





 return value;
}


int Task::getNumber(){

return this->number;

}


VectorXd Task::Error(VectorXd q){

/*
	Compute task error.
	
	input:
		
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::VectorXd error	dim: mx1	task error
*/


VectorXd error(task_dim);

Vector3d eta_ee1;
VectorXd ee_quat(4), quat_d(4);
Vector3d p_obst, p1,p2,p3, p_d;
Matrix3d R;
Vector3d rpy,pos_d, pos_d_body;
Matrix3d dummyRot;
VectorXd qtail(6);
Vector3d qhead;
Vector3d QuatError;
Vector3d panel, vec;


switch(type)
{
case 1:
	
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,4)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,4)(0);
	
	
	break;

case 2:

		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,5)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,5)(0);

	break;

case 3:
	
	
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,6)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,6)(0);

	break;

case 4:
	
	

		
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,7)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,7)(0);

	
	break;
	
case 5:

	

		
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,8)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,8)(0);

	

		
	break;

case 6:

	

		
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,9)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,9)(0);

	
	break;

case 7:

	

	
		if(threshold_violated == 0)

	
			error(0) = params[0] - 0.05 - value_jointlimit(q,10)(0);
			
		else
		
			error(0) = params[1] + 0.05 - value_jointlimit(q,10)(0);

	
	
	break;

case 8:

	
	eta_ee1 = value_pos(q);
  
	error(0) = params[0] - eta_ee1(0);
	error(1) = params[1] - eta_ee1(1);
	error(2) = params[2] - eta_ee1(2);	
	

	break;


case 9:


	ee_quat = value_ori(q);   //quaternion

	quat_d(0) = params[0];
	quat_d(1) = params[1];
	quat_d(2) = params[2];
	quat_d(3) = params[3];


	error = quatError(quat_d,ee_quat);
	
	

	break;

case 10: 


      
        eta_ee1 = value_pos(q);
        ee_quat = value_ori(q);
         
        quat_d(0) = params[0];
        quat_d(1) = params[1];
        quat_d(2) = params[2];
        quat_d(3) = params[3];

	error(0) = params[4] - eta_ee1(0);
	error(1) = params[5] - eta_ee1(1);
	error(2) = params[6] - eta_ee1(2);	


	error.tail(3) = quatError(quat_d,ee_quat);

	
	break;

case 11:

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);


	error(0) = params[0] - 0.001 - value_manipulability(qtail)(0);
	
	break;

case 12:

	
	
	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];

	

	error(0) = params[0] -  0.05 - taskValue(q)(0);

	
	break;

	
case 13:


	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];
	
	
	p2(0) = params[5];
	p2(1) = params[6];
	p2(2) = params[7];
	
	
	p3(0) = params[8];
	p3(1) = params[9];
	p3(2) = params[10];
	
	p_d(0) = params[11];
	p_d(1) = params[12];
	p_d(2) = params[13];
	
	
qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);
	

	
	error(0) = params[0] - 0.05 - value_wall(p1,p2,p3, p_d,qtail)(0);
	
	
	break;
	
case 14:

	p1(0) = params[2];
	p1(1) = params[3];
	p1(2) = params[4];

	
	error(0) = params[0] - value_fov(p1,q)(0); 
	
	break;
	
case 15:

	
	pos_d << params[0], params[1], params[2];
	
	error = pos_d - value_vehicle_pos(q);
	

	
	
	break;
	
case 16:

/*
	panel(0) = params[0];
	panel(1) = params[1];
	panel(2) = params[2];
	
	
	
	if(paramsState[14] <= -3.14)
		paramsState[14] = paramsState[14] + 3.14;
	
	
	
	if(paramsState[14] >= 3.14)
		paramsState[14] = paramsState[14] - 3.14;
	
	cout << paramsState[14] << "\n\n";
	
	vec = q.head(3)-panel;
	error(0) = atan(vec(1)/vec(0)) - paramsState[14] ;
	
	if(error(0) > 1.57)
	
		error(0) = error(0) -3.14;
		
	if(error(0) < -1.57)
	
		error(0) = error(0) +3.14;
	
*/	
	error(0) = params[0] - paramsState[14];

	
	break;
	
case 17:

	

	pos_d << params[0], params[1], params[2];
	
	
	error.head(3) = pos_d - value_vehicle_oripos(q).head(3);
	
	
	error(3) = params[3] - value_vehicle_oripos(q)(3); 
	error(4) = params[4] - value_vehicle_oripos(q)(4); 
	error(5) = params[5] - value_vehicle_oripos(q)(5); 
	 
	 	
	break;
	
case 20: 



	error(0) = params[0] - value_vehicle_attitude(q)(0);
	error(1) = params[1] - value_vehicle_attitude(q)(1);
	

	break;
	
case 21:


	error(0) = params[0] - params[1];//value_vehicle_altitude(q)(0);
	
	break;
	
case 22:

	p_obst(0) = params[2];
	p_obst(1) = params[3];
	p_obst(2) = params[4];
	
	error(0) = params[0] - 0.05 - value_vehicle_obstacle(q, p_obst)(0);
	
	break;
	
case 18:

	
	error(0) = params[0] - value_uvms_ee_pos(q)(0);
	error(1) = params[1] - value_uvms_ee_pos(q)(1);
	error(2) = params[2] - value_uvms_ee_pos(q)(2);
	 
	break;
	
case 19:

	error(0) = params[0] - value_uvms_vehicle_pos(q)(0);
	error(1) = params[1] - value_uvms_vehicle_pos(q)(1);
	error(2) = params[2] - value_uvms_vehicle_pos(q)(2);
	
	
	break;



case 23:

    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];
/*
for(int k = 0;k<7;k++)
cout << params[k] << "\t";
cout <<"\n\n";
*/
        quat_d(0) = params[0];
        quat_d(1) = params[1];
        quat_d(2) = params[2];
        quat_d(3) = params[3];

qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);


	ee_quat = value_ori_arm_floating_base(q.head(3), dummyRot, qtail, 1);




	QuatError = quatError(quat_d,ee_quat);
	//cout << "\n\n" << quat_d << "\n\n";

	error(3) = QuatError(0);
error(4) = QuatError(1);
error(5) = QuatError(2);

	error(0) = params[4] - value_pos_arm_floating_base(qhead, dummyRot, qtail, 1)(0);
	error(1) = params[5] - value_pos_arm_floating_base(qhead, dummyRot, qtail, 1)(1);
	error(2) = params[6] - value_pos_arm_floating_base(qhead, dummyRot, qtail, 1)(2);




	break;


case 24:

    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];

qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);


	error(0) = params[0] - value_pos_arm_floating_base(q.head(3), dummyRot, qtail, 1)(0);
	error(1) = params[1] - value_pos_arm_floating_base(q.head(3), dummyRot, qtail, 1)(1);
	error(2) = params[2] - value_pos_arm_floating_base(q.head(3), dummyRot, qtail, 1)(2);




	
break;


case 25:


    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];



 quat_d(0) = params[0];
        quat_d(1) = params[1];
        quat_d(2) = params[2];
        quat_d(3) = params[3];

qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);



	ee_quat = value_ori_arm_floating_base(q.head(3), dummyRot, qtail, 1);

	QuatError = quatError(quat_d,ee_quat);
	//cout << "\n\n" << quat_d << "\n\n";

error(0) = QuatError(0);
error(1) = QuatError(1);
error(2) = QuatError(2);




	break;
	
	
case 26:


    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];
    
    qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);


qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);


	error(0) = params[0] - 0.001 - value_manipulability_coordinated(qhead, dummyRot, qtail)(0);


	break;
	
	
case 27:

    dummyRot(0,0) = paramsState[0];
    dummyRot(0,1) = paramsState[1];
    dummyRot(0,2) = paramsState[2];
    dummyRot(1,0) = paramsState[3];
    dummyRot(1,1) = paramsState[4];
    dummyRot(1,2) = paramsState[5];
    dummyRot(2,0) = paramsState[6];
    dummyRot(2,1) = paramsState[7];
    dummyRot(2,2) = paramsState[8];
/*
for(int k = 0;k<7;k++)
cout << params[k] << "\t";
cout <<"\n\n";
*/
        quat_d(0) = params[0];
        quat_d(1) = params[1];
        quat_d(2) = params[2];
        quat_d(3) = params[3];

qhead(0) = q(0);
qhead(1) = q(1);
qhead(2) = q(2);

qtail(0) = q(4);
qtail(1) = q(5);
qtail(2) = q(6);
qtail(3) = q(7);
qtail(4) = q(8);
qtail(5) = q(9);



	ee_quat = value_ori_arm_floating_base(q.head(3), dummyRot, qtail, 1);

	QuatError = quatError(quat_d,ee_quat);
	//cout << "\n\n" << quat_d << "\n\n";

	error(3) = QuatError(0);
error(4) = QuatError(1);
error(5) = QuatError(2);

	error(0) = params[4] - value_pos_arm_floating_base(qhead, dummyRot, qtail, 1)(0);
	error(1) = params[5] - value_pos_arm_floating_base(qhead, dummyRot, qtail, 1)(1);
	error(2) = params[6] - value_pos_arm_floating_base(qhead, dummyRot, qtail, 1)(2);




	break;
	
	
}

	

return error;

}


MatrixXd Task::NullJ(VectorXd q){

/*
	Compute the null of the task Jacobian matrix.
	
	input:
	
		Eigen::VectorXd q 	dim: nx1	joint positions
		
	output:
	
		Eigen::MatrixXd nullJ	dim: nxn	null of the task Jacobian matrix

*/

int joints = q.size();
MatrixXd J = Jacobian(q);
MatrixXd nullJ(joints,joints);
MatrixXd I(joints,joints);

I.setIdentity();


MatrixXd W(joints,joints);
W.setIdentity();

double error_norm = Error(q).norm(); 
double max_out = 10;
int flag_sigma = 0;
double d, l, sigma_min;
int algorithm = 2;


//nullJ = I - (mypinv1(J, W, algorithm, error_norm,  max_out, flag_sigma, d, l, sigma_min)*J);
nullJ = I - dls_pinv(J,W,error_norm, max_out, flag_sigma) * J;
	
//nullJ = I - (mypinv(J)*J);
return nullJ;
}


VectorXd Task::computeSolution(VectorXd q){

/*

	Compute the target joint velocities that fulfill the task
	
	input:
		
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::VectorXd dq	dim: nx1	desired joint velocities
*/


int joints = q.size();
VectorXd dq(joints);


MatrixXd J = Jacobian(q);



//FullPivLU<MatrixXd> lu(J);

//cout << "Rango: " << lu.rank() << "\n";


MatrixXd W(joints,joints);
W.setIdentity();
double scale = 0.01;
W(0,0) = scale;
W(1,1) = scale;
W(2,2) = scale;
W(3,3) = scale;
W(4,4) = 1-scale;
W(5,5) = 1-scale;
W(6,6) = 1-scale;
W(7,7) = 1-scale;
W(8,8) = 1-scale;
W(9,9) = 1-scale;


double error_norm = Error(q).norm(); 
double max_out = 0.5;
int flag_sigma = 0;
double d, l, sigma_min;
int algorithm = 2;

	
dq = dls_pinv(J,W,error_norm, max_out, flag_sigma) * K * Error(q);


double threshold = 0.01;
double lambda = 0.001;

//dq = eig_pinv(J, threshold, lambda) * K * Error(q);


return dq;
}

int Task::getType(){

/*
	Get the task type
*/


	return this->type;

}

MatrixXd Task::getK(){

/*

	Get the task gain
*/


	return this->K;
}



vector<double> Task::getParams(){

/*

	Get the task parameters vector
*/


	return params;
}

void Task::setParams(vector<double> p){

/*

	Set the task parameters vector
*/

	params=p;
}

void Task::setParamsState(vector<double> p){

/*

	Set the task parameters vector
*/

	paramsState=p;
}

MatrixXd Task::GetJ(){

	return this->J;

}

