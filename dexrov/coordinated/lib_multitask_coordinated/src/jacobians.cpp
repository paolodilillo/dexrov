#include "LLkinematics.h"

using namespace Eigen;
using namespace std;

const int J_row = 6;
const int p_row = 3;
const int p_col = 1;
const int z_row = 3;
const int z_col = 1;

MatrixXd J_oripos(VectorXd q){

/* 
	Jacobian for the end-effector configuration (position+orientation) task.
	
	input:
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::MatrixXd J	dim: 6xn	jacobian matrix

*/


	int i,j,k;
	int joints = q.size();

	double **J = allocateArray2d(J_row, joints);
	double ***T0 = jaco_directkinematics(q);
	double **p = allocateArray2d(p_row, joints+1);  //position array
	double **z = allocateArray2d(z_row, joints+1);  //z array

	z[0][0] = 0;
	z[1][0] = 0;
	z[2][0] = 1;

	p[0][0] = 0;
	p[1][0] = 0;
	p[2][0] = 0;

	for(k=1; k<=joints; k++){
		for(i=0; i<p_row; i++){

				p[i][k] = T0[i][3][k-1];
				z[i][k] = T0[i][2][k-1];

		}
	}



	double **p_J = allocateArray2d(p_row, joints);  //position difference array

	//*************Processing position difference array*************
	for(j=0; j<joints; j++){
		for(i=0; i<p_row; i++){
			p_J[i][j] = p[i][joints] - p[i][j];
		//	cout << "ee:\n\n" << p[i][j] << "\n";
		}
	}


	double *Japp = new double[J_row];  //support variable Japp

	for(i=0; i<joints; i++){

		Japp = LinearAndAngular(p_J,z, i);
		for(j=0; j<J_row; j++){
			J[j][i] = Japp[j];
		}
	}

	for(i=0; i<J_row; i++){
		for(j=0; j<joints; j++){
			if((J[i][j]>=0 && J[i][j] < pow(10, -6)) || (J[i][j]<0 && J[i][j] > -pow(10, -6)))
				J[i][j]=0;
		}
	}
	
	MatrixXd J_out(6,joints);
	
	for(i=0;i<6;i++)
		for(j=0;j<joints;j++)
		
			J_out(i,j) = J[i][j];
			
	deallocateArray2d(6, J);
	deallocateArray2d(3, p);
	deallocateArray2d(3, z);
	deallocateArray2d(3, p_J);
	deallocateArray3d(4, 4, T0);
	delete[] Japp;

 	return J_out;
}


MatrixXd J_pos(VectorXd q){

/* Jacobian of the end-effector position task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 3xn		Jacobian matrix
	

*/

    int i=0, j=0;
    int joints = q.size();
    MatrixXd Jx(3, joints);

    MatrixXd J(6,joints);
    J = J_oripos(q);


    Jx = J.block(0,0,3,joints);
//    Jx.row(0) << 0,0,0,0,0,0,0;

    return Jx;
}


MatrixXd J_ori(VectorXd q){

/* Jacobian of the end-effector orientation task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 3xn		Jacobian matrix
	

*/

     int i=0, j=0;
    int joints = q.size();
    MatrixXd Jx(3, joints);

    MatrixXd J(6,joints);
    J = J_oripos(q);

    Jx = J.block(3,0,3,joints);

    return Jx;
}




MatrixXd J_obst_wrist(Vector3d p_obst, VectorXd q){

/* Jacobian of the obstacle avoidance at wrist task.

 input: 
 
 	Eigen::Vector3d p_obst		dim: 3x1		x,y,z coordinates of the obstacle
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 1xn		Jacobian matrix
	

*/


    int joints = q.size();
    MatrixXd Jx(1, joints);
    int i=0, j=0;

    VectorXd qDH =q;

    
    MatrixXd T0(4,4);
    T0 = directkinematics(qDH);

    Vector3d eta_ee1;

    for(i=0; i<3; i++)
        eta_ee1(i) = T0(i,3);


    double dist = sqrt((p_obst - eta_ee1).transpose() * (p_obst - eta_ee1));

    MatrixXd J(3,joints);
    J = J_pos(qDH);

  
  
   if(dist < pow(10,-3))
        dist = pow(10,-3);

    Jx = - ((p_obst - eta_ee1).transpose()/dist)*J;

    return Jx;

}


MatrixXd J_obst_elbow(Vector3d p_obst, VectorXd q, int id){

/* Jacobian of the obstacle avoidance at elbow task.

 input: 
 
 	Eigen::Vector3d p_obst		dim: 3x1		x,y,z coordinates of the obstacle
	Eigen::VectorXd q		dim: nx1		joint positions
	int id				dim 1			index of the elbow link
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 1xn		Jacobian matrix
	

*/


    int joints = q.size();
    MatrixXd Jx(1, joints);
    int i=0, j=0;

    VectorXd qDH =q;

    
    MatrixXd T0(4,4);
    T0 = directkinematics(qDH,id);
    Vector3d eta_ee1;

    for(i=0; i<3; i++)
        eta_ee1(i) = T0(i,3);


    double dist = sqrt((p_obst - eta_ee1).transpose() * (p_obst - eta_ee1));

    MatrixXd J(3,joints);
    J = J_pos(qDH);

  
  
   if(dist < pow(10,-3))
        dist = pow(10,-3);

    Jx = - ((p_obst - eta_ee1).transpose()/dist)*J;

    return Jx;

}


MatrixXd J_jointlimit(VectorXd q, int id){


/* Jacobian of the Joint limit  task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	int id				dim: 1			joint index
	
	
 output:
 
 	Eigen::MatrixXd	J		dim: 1xn		Jacobian matrix
	

*/


MatrixXd J(1,q.size());

if(id >= q.size()){

	cout << "\n[J_jointlimit function]: the system has " << q.size() << " joints, requested id = " << id+1 << endl; 
	exit(0);

}


for(int i=0;i<q.size();i++){

	if(i==id)
	
		J(0,i) = 1;
		
	else
	
		J(0,i) = 0;


}



return J;

}



    
MatrixXd J_manipulability_coordinated(Vector3d vehicle_pos, Matrix3d vehicle_rot,VectorXd q){

/* Jacobian of the arm manipulability  task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	J		dim: 1xn		Jacobian matrix
	

*/





int i=0, j=0, joints = 10;

MatrixXd J(1,joints);

        VectorXd qi(joints);
        double delta_q = 0.1;
        double w = 0, wi = 0;

Vector3d vehicle_pos2;

        for(i=0; i< joints; i++){
            for(j=0; j<joints; j++){
                if(j==i)
                    qi(j) = q(j) + delta_q;
                else
                    qi(j) = q(j);
            }
          
            w = value_manipulability_coordinated(vehicle_pos, vehicle_rot,q)(0);

	    vehicle_pos2(0) = qi(0);
	    vehicle_pos2(1) = qi(1);
	    vehicle_pos2(2) = qi(2);
	    
            wi = value_manipulability_coordinated(vehicle_pos2,vehicle_rot,qi)(0);
          
		
		
		
            J(0,i) = (wi - w)/delta_q;
        }

cout << J << "\n\n";

return J;

}
    
    
MatrixXd J_manipulability(VectorXd q){

/* Jacobian of the arm manipulability  task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	J		dim: 1xn		Jacobian matrix
	

*/





int i=0, j=0, joints = q.size();

MatrixXd J(1,joints);

        VectorXd qi(joints);
        double delta_q = 0.001;
        double w = 0, wi = 0;



        for(i=0; i< joints; i++){
            for(j=0; j<joints; j++){
                if(j==i)
                    qi(j) = q(j) + delta_q;
                else
                    qi(j) = q(j);
            }
          
            w = value_manipulability(q)(0);
    
            wi = value_manipulability(qi)(0);
            J(0,i) = (wi - w)/delta_q;
        }

return J;

}





MatrixXd J_wall(Vector3d p1, Vector3d p2, Vector3d p3, Vector3d p_d, VectorXd q){


/* Jacobian of the virtual plane task.

 input: 
 
 	Eigen::Vector3d p1		dim: 3x1		x,y,z coordinates of a point belonging to the plane
 	Eigen::Vector3d p2		dim: 3x1		x,y,z coordinates of a point belonging to the plane
 	Eigen::Vector3d p3		dim: 3x1		x,y,z coordinates of a point belonging to the plane
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 1xn		Jacobian matrix
	

*/



int joints = q.size();
MatrixXd Jx(1, joints);
int i=0, j=0;

VectorXd qDH =q;


MatrixXd T(4, 4);
T = directkinematics(qDH);

Vector3d pos;

for(i=0; i<3; i++)
	pos(i) = T(i,3);


MatrixXd J(3,joints);
   
J = J_pos(qDH);
   
Vector3d plane_normal;


plane_normal << ((p2-p1).cross(p3-p1)) / ((p2-p1).cross(p3-p1)).norm();

double distance = plane_normal.transpose() * (pos - p1);


//Vector3d p_primo = abs(plane_normal.transpose()*(pos-p1))*plane_normal;




if( distance > 0){

Jx =  plane_normal.transpose() * J;
//Jx = ((pos-p_primo).transpose() / (pos-p_primo).norm()) * J;

}


else{

Jx = - plane_normal.transpose() * J;
//Jx = - ((pos-p_primo).transpose() / (pos-p_primo).norm()) * J;

}




return Jx;

}




MatrixXd J_fov(Vector3d p,  VectorXd q){

int joints = q.size();

MatrixXd J(1,joints);
MatrixXd J_orientation = J_ori(q);
MatrixXd J_position = J_pos(q);
MatrixXd T = directkinematics(q);
Matrix3d R = T.block(0,0,3,3);
Vector3d p_ee = T.block(0,3,3,1);
VectorXd axis = R2axis(R);
Vector3d a;

a << axis(1), axis(2), axis(3);

Vector3d ad;

ad = (p - p_ee) / (p - p_ee).norm();


J =  ((ad -a).transpose() / (ad -a).norm()) * (-skew(ad) * skew(p - p_ee) * J_position + skew(a) * J_orientation);

return J;

}


MatrixXd J_vehicle_ori(VectorXd q){

int joints = q.size();

double cf = cos(q(3));
double sf = sin(q(3));

double st = sin(q(4));
double ct = cos(q(4));

double tt = tan(q(4));

MatrixXd J(3,joints);



Matrix3d T;

T << 1, sf*tt, cf*tt,
     0,cf, -sf,
     0, sf/ct, cf/ct;


J.block(0,0,3,3).setZero();

J.block(0,3,3,3)  << 1,0, -st,
      		     0,cf, ct*sf,
     		     0,-sf, ct*cf;
     		     
   	     
J.block(0,3,3,3) =T;



return J;

}



MatrixXd J_vehicle_pos(VectorXd q){


int joints = q.size();
Vector3d rpy;

rpy(0) = q(3);
rpy(1) = q(4);
rpy(2) = q(5);


Matrix3d R;
R = rpy2rot(rpy);

MatrixXd J(3,joints);

J.block(0,0,3,3)  << R;

J.block(0,3,3,3).setZero();



return J;

}


MatrixXd J_vehicle_oripos(VectorXd q){

int joints = q.size();
MatrixXd J;//(6,joints);


J.block(0,0,3,6) = J_vehicle_pos(q);
J.block(3,0,3,6) = J_vehicle_ori(q);


return J;


}

MatrixXd J_vehicle_attitude(VectorXd q){

int joints = q.size();

MatrixXd J(2,joints);

J.row(0) = J_vehicle_ori(q).row(0);
J.row(1) = J_vehicle_ori(q).row(1);


return J;

}

MatrixXd J_vehicle_altitude(VectorXd q){


int joints = q.size();

MatrixXd J(1,joints);

J.row(0) = J_vehicle_pos(q).row(2);

return J;

}

MatrixXd J_vehicle_obstacle(VectorXd q, Vector3d p_obst){

int joints = q.size();

MatrixXd J(1, joints);

double dist = sqrt((p_obst - value_vehicle_pos(q)).transpose() * (p_obst - value_vehicle_pos(q)));

    MatrixXd J_p(3,joints);
    J_p = J_vehicle_pos(q);

  
  
   if(dist < pow(10,-3))
        dist = pow(10,-3);

    J = - ((p_obst - value_vehicle_pos(q)).transpose()/dist)*J_p;

return J;

}


MatrixXd inv_J_vehicle_oripos(VectorXd q){

MatrixXd J = J_vehicle_oripos(q);

MatrixXd invJ(6,6);

invJ.block(0,0,3,3) = J.block(0,0,3,3).transpose();
invJ.block(0,3,3,3).setZero();
invJ.block(3,0,3,3).setZero();
invJ.block(3,3,3,3) = J.block(3,3,3,3).inverse();

return invJ;

}


MatrixXd T_uvms(){


MatrixXd T(4,4);

T.block(0,0,3,3).setIdentity();
T.block(0,3,3,1) << 0,0,1;
T.row(3) << 0,0,0,1;

return T;

}

MatrixXd J_uvms_ee_pos(VectorXd q){

int joints = q.size();
int arm_joints = q.size() - 6;

VectorXd q_arm(arm_joints);
VectorXd q_vehicle(6);

q_arm = q.tail(6);
q_vehicle = q.head(6);

Vector3d rpy;

rpy(0) = q(3);
rpy(1) = q(4);
rpy(2) = q(5);


Matrix3d R;
R = rpy2rot(rpy);

MatrixXd T(4,4);

T = T_uvms();
Vector3d rbb0 = T.block(0,3,3,1);


MatrixXd J(3,joints);

J.block(0,0,3,3) = R;
J.block(0,3,3,3) = - ( S(R*rbb0));
/* DA FINIRE */



return J;

}

Matrix3d S(Vector3d x){

Matrix3d out;

out << 0, -x(2), x(1),
       x(2), 0, -x(0),
       -x(1), x(0), 0;

return out;

}

MatrixXd J_uvms_vehicle_pos(VectorXd q){

int joints = q.size();
int arm_joints = q.size() - 6;

MatrixXd J(3,joints);

J.block(0,0,3,arm_joints).setZero();
J.block(0,arm_joints,3,6) << J_vehicle_pos(q.tail(6));



return J;


}


MatrixXd J_oripos_arm_floating_base(Vector3d vehicle_p,Matrix3d vehicle_rot,VectorXd q,int arm_id){
    /*
        Jacobian for the arm end-effector configuration (position+orientation) task.

        input:
            Eigen::Vector3d vehicle_p	dim: 3x1	vehicle positions
            Eigen::Vector3d vehicle_rot	dim: 3x3	vehicle rotational matrix
            Eigen::VectorXd q           dim: nx1	joint positions
            int             arm_id      dim 1       1=arm 1, 2=arm 2

        output:

            Eigen::MatrixXd J	dim: 6xn	jacobian matrix

        Author: E.Cataldi, 2017, SevilleExp

    */


        int i,j,k;
        int joints = q.size();

        double **J = allocateArray2d(J_row, joints);
        //double ***T0 = jaco_directkinematics(q);
        double **p = allocateArray2d(p_row, joints+1);  //position array
        double **z = allocateArray2d(z_row, joints+1);  //z array

        Matrix4d T= directkinematics(vehicle_p,vehicle_rot,q,arm_id,-1);

        z[0][0] = T(0,2);
        z[1][0] = T(1,2);
        z[2][0] = T(2,2);

        p[0][0] = T(0,3);
        p[1][0] = T(1,3);
        p[2][0] = T(2,3);
        //std::cout<<"T_v arm"<<arm_id<<"\n"<<T<<std::endl;

        for(k=1; k<=joints; k++){
             T = directkinematics(vehicle_p,vehicle_rot,q,arm_id,k-1);
            //for(i=0; i<p_row; i++){

                    //p[i][k] = T0[i][3][k-1];
                    //z[i][k] = T0[i][2][k-1];

            //}
             z[0][k] = T(0,2);
             z[1][k] = T(1,2);
             z[2][k] = T(2,2);

             p[0][k] = T(0,3);
             p[1][k] = T(1,3);
             p[2][k] = T(2,3);
             //std::cout<<"T_"<<k-1<<"arm "<<arm_id<<"\n"<<T<<std::endl;
        }



        double **p_J = allocateArray2d(p_row, joints);  //position difference array

        //*************Processing position difference array*************
        for(j=0; j<joints; j++){
            for(i=0; i<p_row; i++){
                p_J[i][j] = p[i][joints] - p[i][j];
            //	cout << "ee:\n\n" << p[i][j] << "\n";
            }
        }


        double *Japp = new double[J_row];  //support variable Japp

        for(i=0; i<joints; i++){

            Japp = LinearAndAngular(p_J,z, i);
            for(j=0; j<J_row; j++){
                J[j][i] = Japp[j];
            }
        }

        for(i=0; i<J_row; i++){
            for(j=0; j<joints; j++){
                if((J[i][j]>=0 && J[i][j] < pow(10, -6)) || (J[i][j]<0 && J[i][j] > -pow(10, -6)))
                    J[i][j]=0;
            }
        }

        MatrixXd J_out(6,joints);

        for(i=0;i<6;i++)
            for(j=0;j<joints;j++)

                J_out(i,j) = J[i][j];

        deallocateArray2d(6, J);
        deallocateArray2d(3, p);
        deallocateArray2d(3, z);
        deallocateArray2d(3, p_J);
        //deallocateArray3d(4, 4, T0);
        delete[] Japp;

        //MatrixXd Rot(6,6);
        //Rot << vehicle_rot, Matrix3d::Zero(), Matrix3d::Zero(), vehicle_rot;

        return J_out;
}


MatrixXd J_pos_vehicle_manipulator(Vector3d vehicle_p, Matrix3d vehicle_rot,VectorXd q){

MatrixXd J(3,10);
J = J_oripos_vehicle_manipulator(vehicle_p, vehicle_rot,q).block(0,0,3,10);



return J;

}


MatrixXd J_ori_vehicle_manipulator(Vector3d vehicle_p, Matrix3d vehicle_rot,VectorXd q){


return J_oripos_vehicle_manipulator(vehicle_p, vehicle_rot,q).block(3,0,3,10);

}



MatrixXd J_oripos_vehicle_manipulator_man(Matrix3d vehicle_rot,VectorXd q){


  Matrix3d Rtot; 
    
Rtot << 0,0,1,
       -1,0,0,
       0,-1,0;    
  

  
    MatrixXd JJJ(6,12);
 
    MatrixXd JJ(6,6);
    
    JJ = J_oripos(q);
   
    
    JJJ.block(0,0,3,3) = Matrix3d::Zero();
    JJJ.block(0,3,3,3) = Matrix3d::Zero();
    JJJ.block(0,6,3,6) = vehicle_rot * Rtot * JJ.block(0,0,3,6);
    
    JJJ.block(3,0,3,3) = MatrixXd::Zero(3,3);
    JJJ.block(3,3,3,3) = Matrix3d::Zero();
    JJJ.block(3,6,3,6) = vehicle_rot * Rtot * JJ.block(3,0,3,6);
    
    MatrixXd JJJJ(6,10);
    JJJJ.block(0,0,6,3) = JJJ.block(0,0,6,3);
    JJJJ.block(0,3,6,1) = JJJ.block(0,5,6,1);
    JJJJ.block(0,4,6,6) = JJJ.block(0,6,6,6);
    
    


    return JJJJ;


}

MatrixXd J_oripos_vehicle_manipulator(Vector3d vehicle_p,Matrix3d vehicle_rot,VectorXd q){
    /*
        Jacobian for the vehicle-manipulator configuration (position-orientation) task.

        input:
            Eigen::Vector3d vehicle_p	dim: 3x1	vehicle positions
            Eigen::Vector3d vehicle_rot	dim: 3x3	vehicle rotational matrix
            Eigen::VectorXd q           dim: nx1	joint positions

        output:

            Eigen::MatrixXd J	dim: 6x6+n	orientation jacobian matrix

        Author: E.Cataldi, 2017, Planner

    */
    
    /*
    MatrixXd J(6,10);
    MatrixXd Japp(6,6);


    Japp = J_vehicle_posori_C(vehicle_p,vehicle_rot,q,1);
	
    J.block(0,0,6,3) = Japp.block(0,0,6,3);
    J.block(0,3,6,1) = Japp.block(0,5,6,1);
    
    
    

double alpha = -PI/2;	// around z
double beta = 0;	// around y
double gamma = -PI/2;	// around x


Matrix3d Rx, Ry, Rz, Rtot;

Rz << cos(alpha), -sin(alpha), 0,
      sin(alpha), cos(alpha), 0,
      0,0,1;
      
Ry << cos(beta), 0, sin(beta),
      0,1,0,
      -sin(beta),0,cos(beta);
      
Rx << 1, 0, 0,
      0, cos(gamma), -sin(gamma),
      0, sin(gamma), cos(gamma);
      
Rtot = Rz * Ry * Rx;
  
  
  
    MatrixXd Tr(6,6);
    Tr.block(0,0,3,3) = vehicle_rot*Rtot;
    Tr.block(0,3,3,3) = MatrixXd::Zero(3,3);
    Tr.block(3,0,3,3) = MatrixXd::Zero(3,3);
    Tr.block(3,3,3,3) = vehicle_rot*Rtot;
    
//    cout << vehicle_rot << "\n\n";
    J.block(0,4,6,6) =  J_oripos_arm_floating_base(vehicle_p,vehicle_rot,q,1);
    */
    
    
    
/*
double alpha = -PI/2;	// around z
double beta = 0;	// around y
double gamma = -PI/2;	// around x


Matrix3d Rx, Ry, Rz, Rtot;

Rz << cos(alpha), -sin(alpha), 0,
      sin(alpha), cos(alpha), 0,
      0,0,1;
      
Ry << cos(beta), 0, sin(beta),
      0,1,0,
      -sin(beta),0,cos(beta);
      
Rx << 1, 0, 0,
      0, cos(gamma), -sin(gamma),
      0, sin(gamma), cos(gamma);
      
Rtot = Rz * Ry * Rx;
    
  */
  
  Matrix3d Rtot; 
    
Rtot << 0,0,1,
       -1,0,0,
       0,-1,0;    
  

  
    MatrixXd JJJ(6,12);
    Vector3d rbb0;
    Matrix4d TT;
    MatrixXd JJ(6,6);
    
    JJ = J_oripos(q);
      
    TT = directkinematics(q);
    rbb0 << 0.95,0.2,-0.5;
    
    JJJ.block(0,0,3,3) = vehicle_rot;

    
    JJJ.block(0,3,3,3) = - (skew(vehicle_rot * rbb0) + skew(vehicle_rot*Rtot * TT.block(0,3,3,1))) * vehicle_rot;
    
    JJJ.block(0,6,3,6) = vehicle_rot * Rtot * JJ.block(0,0,3,6);
    
//    cout << JJ.block(0,0,3,6) << "\n\n\n"<<vehicle_rot * Rtot * JJ.block(0,0,3,6);
//    cout <<"--------" << "\n\n";
     
    JJJ.block(3,0,3,3) = MatrixXd::Zero(3,3);
    JJJ.block(3,3,3,3) = vehicle_rot;
    JJJ.block(3,6,3,6) = vehicle_rot * Rtot * JJ.block(3,0,3,6);
    
//    cout << JJJ << "\n\n";
      
    MatrixXd JJJJ(6,10);
    JJJJ.block(0,0,6,3) = JJJ.block(0,0,6,3);
    JJJJ.block(0,3,6,1) = JJJ.block(0,5,6,1);
    JJJJ.block(0,4,6,6) = JJJ.block(0,6,6,6);
    
    

 //   cout << JJJJ << "\n\n";
 
//cout << J_oripos_arm_floating_base(vehicle_p,vehicle_rot,q,1) << "\n\n";


    return JJJJ;

}


MatrixXd J_vehicle_posori_C(Vector3d vehicle_p,Matrix3d vehicle_rot,VectorXd q,int arm_id){
    /*
        Vehicle Jacobian matrix
J_pos_quad_dualarm_floating_base
        input:
            Eigen::Vector3d vehicle_p	dim: 3x1	vehicle positions
            Eigen::Matrix3d vehicle_rot	dim: 3x3	vehicle rotational matrix
            Eigen::VectorXd q           dim: nx1	joint positions
            int             arm_id      dim 1       1=arm 1, 2=arm 2

        output:

            Eigen::MatrixXd J           dim: 6x6	Jacobian matrix

        Author: E.Cataldi, 2017, SevilleExp

    */
    MatrixXd J_v(6,6),T_v(6,6);

    Matrix4d EE_tran = directkinematics(vehicle_p,vehicle_rot,q,arm_id);
    //std::cout<<"EE_tran "<<EE_tran.block(0,3,3,1).transpose()<<std::endl;

    J_v.block(0,0,3,3) = Matrix3d::Identity();
    Vector3d eta_v_ee  = EE_tran.block(0,3,3,1)-vehicle_p;
    J_v.block(0,3,3,3) = -skew(eta_v_ee);

    J_v.block(3,0,3,3) = Matrix3d::Zero();
    J_v.block(3,3,3,3) = Matrix3d::Identity();

    T_v.block(0,0,3,3) = Matrix3d::Identity();
    T_v.block(0,3,3,3) = Matrix3d::Zero();

    T_v.block(3,0,3,3) = Matrix3d::Zero();
    
    
    T_v.block(3,3,3,3) = tran_omega2drpy(rot2rpy_Eigen(vehicle_rot)); //vehicle_rot*
    //std::cout<<"J_v*T_v  \n"<<J_v*T_v<<std::endl;

    return J_v;//*T_v;
}



Matrix3d tran_omega2drpy(Vector3d rpy){
    /*
        Vehicle position Jacobian matrix

        input:
            Eigen::Vector3d rpy     dim: 3x1	orientation
        output:

            Eigen::Matrix3d T       dim: 3x3	Transformation matrix from angular velocity to Euler angles derivative
            T = [   1  0            -sin(theta)
                    0  cos(phi)  cos(theta)*sin(phi)
                    0 -sin(phi)  cos(theta)*cos(phi)    ];

        Author: E.Cataldi, 2017, SevilleExp

    */
    Matrix3d T;


Vector3d rpy2;

rpy2(0) = rpy(2);
rpy2(1) = rpy(1);
rpy2(2) = rpy(0);

    T(0,0) = 1.0;
    T(0,1) = 0.0;
    T(0,2) = -sinf(rpy(1));
    T(1,0) = 0.0;
    T(1,1) = cosf(rpy(0));
    T(1,2) = cosf(rpy(1))*sinf(rpy(0));
    T(2,0) = 0.0;
    T(2,1) = -sinf(rpy(0));
    T(2,2) = cosf(rpy(1))*cosf(rpy(0));

/*
    T(0,0) = 1.0;
    T(0,1) = 0.0;
    T(0,2) = -sinf(rpy2(1));
    T(1,0) = 0.0;
    T(1,1) = cosf(rpy2(0));
    T(1,2) = cosf(rpy2(1))*sinf(rpy2(0));
    T(2,0) = 0.0;
    T(2,1) = -sinf(rpy2(0));
    T(2,2) = cosf(rpy2(1))*cosf(rpy2(0));
*/

    return T;
}


Vector3d rot2rpy_Eigen(Matrix3d R){
    /*
        From rotational matrix to Euler angles with Eigen

        input:
            Eigen::Matriz3d R       dim: 3x3	rotational matrix
        output:
            Eigen::Vector3d rpy     dim: 3x1	Euler angles

        Author: E.Cataldi, 2017, SevilleExp

    */
    //Vector3d ypr = R.eulerAngles(2,1,0);
    return R.eulerAngles(0,1,2);//Vector3d(ypr(2),ypr(1),ypr(0));
}




MatrixXd directkinematics(Vector3d vehicle_p,Matrix3d vehicle_rot,VectorXd q,int arm_id){

/*
    Direct kinematics for the Seville sys quadrotor (4 DoF) with dual arm (4 DoF/arm)

    input:
        Eigen::Vector3d vehicle_p	dim: 3x1	vehicle positions
        Eigen::Matrix3d vehicle_rot	dim: 3x3	vehicle rotational matrix
        Eigen::VectorXd q           dim: nx1	joint positions
        int             arm_id      dim 1       1=arm 1, 2=arm 2

    output:

        Eigen::MatrixXd T_out	dim: 4x4	homogeneous transform matrix from the link ee to the vehicle

    Author: E.Cataldi, 2017, SevilleExp

*/


    int joints = q.size();

    double *q1 = VectorXdtoarray1d(q);

    double ***T,***Tva, ***T0,***T0va;
    double **DHArm;
    double **DHVehicle;

    if(arm_id == 1)

        DHArm   = readDH1(6);

    else if(arm_id == 2)

        DHArm   = readDH1(6);


    DHVehicle   = readDHSevilleVehicle(arm_id);

    DHArm       = elaborateDH(joints, q1, DHArm);

    Tva = elaborateT(DHVehicle, 2);
    T   = elaborateT(DHArm, joints);
    T0va= elaborateT0(Tva, 2);
    T0  = elaborateT0(T, joints);


	

    MatrixXd T_out(4,4),T_out_v,T_out_v_a(4,4);

    T_out_v                 = MatrixXd::Zero(4,4);
    T_out_v(3,3)            = 1.0;
    T_out_v.block(0,0,3,3)  = vehicle_rot;
    T_out_v.block(0,3,3,1)  = vehicle_p;

    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++)

            T_out_v_a(i,j) = T0va[i][j][1];



double alpha = -PI/2;	// around z
double beta = 0;	// around y
double gamma = -PI/2;	// around x

Matrix3d Rx, Ry, Rz, Rtot;

Rz << cos(alpha), -sin(alpha), 0,
      sin(alpha), cos(alpha), 0,
      0,0,1;
      
Ry << cos(beta), 0, sin(beta),
      0,1,0,
      -sin(beta),0,cos(beta);
      
Rx << 1, 0, 0,
      0, cos(gamma), -sin(gamma),
      0, sin(gamma), cos(gamma);
      
Rtot = Rz * Ry * Rx;

   T_out_v_a.block(0,3,3,1) << 0.95,0.2,-0.5;
   T_out_v_a.block(0,0,3,3)  = Rtot;
   T_out_v_a(3,3) = 1.0;

    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++)

            T_out(i,j) = T0[i][j][joints-1];

    delete[] q1;
    deallocateArray3d(4,4,T);
    deallocateArray3d(4,4,T0);
    deallocateArray3d(4,4,Tva);
    deallocateArray3d(4,4,T0va);
    deallocateArray2d(joints,DHArm);
    deallocateArray2d(2,DHVehicle);

/*
cout << "\n" << q << "\n\n\n";
cout << "T veicolo\n" << T_out_v << "\n\nT veicolo-base\n" << T_out_v_a << "\n\nT braccio\n" << T_out << "\n\n\n\n";
*/
    return T_out_v*T_out_v_a*T_out;
}


MatrixXd directkinematics(Vector3d vehicle_p,Matrix3d vehicle_rot,VectorXd q,int arm_id,int id){

/*
    Direct kinematics for the Seville sys quadrotor (4 DoF) with dual arm (4 DoF/arm)

    input:
        Eigen::Vector3d vehicle_p	dim: 3x1	vehicle positions
        Eigen::Matrix3d vehicle_rot	dim: 3x3	vehicle rotational matrix
        Eigen::VectorXd q           dim: nx1	joint positions
        int             arm_id      dim 1       1=arm 1, 2=arm 2
        int             id          dim 1       i-th joint or -1

    output:

        Eigen::MatrixXd T_out	dim: 4x4	homogeneous transform matrix from the link "id" to the vehicle if "id=-1" only vehicle

    Author: E.Cataldi, 2017, SevilleExp

*/


    if(id >= q.size()){

        cout << "\n[directkinematics function]: the system has " << q.size() << " joints, requested id = " << id+1 << endl;
        exit(0);

    }

    int joints = q.size();

    double *q1 = VectorXdtoarray1d(q);

    double ***T,***Tva, ***T0,***T0va;
    double **DHArm;
    double **DHVehicle;

    if(arm_id == 1)

        DHArm   = readDH1(6);

    else if(arm_id == 2)

        DHArm   = readDH1(6);


    DHVehicle   = readDHSevilleVehicle(arm_id);
    DHArm       = elaborateDH(joints, q1, DHArm);

    Tva = elaborateT(DHVehicle, 2);
    T   = elaborateT(DHArm, joints);
    T0va= elaborateT0(Tva, 2);
    T0  = elaborateT0(T, joints);

    MatrixXd T_out(4,4),T_out_v,T_out_v_a(4,4);

    T_out_v                 = MatrixXd::Zero(4,4);
    T_out_v(3,3)            = 1.0;
    T_out_v.block(0,0,3,3)  = vehicle_rot;
    T_out_v.block(0,3,3,1)  = vehicle_p;

    if (id==-1)
        return T_out_v;
    else {
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)

                T_out_v_a(i,j) = T0va[i][j][1];
                
                
              
double alpha = -PI/2;	// around z
double beta = 0;	// around y
double gamma = -PI/2;	// around x



Matrix3d Rx, Ry, Rz, Rtot;

Rz << cos(alpha), -sin(alpha), 0,
      sin(alpha), cos(alpha), 0,
      0,0,1;
      
Ry << cos(beta), 0, sin(beta),
      0,1,0,
      -sin(beta),0,cos(beta);
      
Rx << 1, 0, 0,
      0, cos(gamma), -sin(gamma),
      0, sin(gamma), cos(gamma);
      
Rtot = Rz * Ry * Rx;

 T_out_v_a.block(0,3,3,1) << 0.95,0.2,-0.5;
   T_out_v.block(0,0,3,3)  = Rtot;
   T_out_v(3,3) = 1.0;
   

  


        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)

                T_out(i,j) = T0[i][j][id];

        delete[] q1;
        deallocateArray3d(4,4,T);
        deallocateArray3d(4,4,T0);
        deallocateArray3d(4,4,Tva);
        deallocateArray3d(4,4,T0va);
        deallocateArray2d(joints,DHArm);
        deallocateArray2d(2,DHVehicle);


        return T_out_v*T_out_v_a*T_out;
    }
}


double **readDHSevilleArm1(){

/*
    Initialize the DH table for the Seville Arm 1

    output:

        double **DH	dim: 6x4	DH table [a alpha d theta] ?

    Author: E.Cataldi, 2017, SevilleExp
*/

    double **DH = allocateArray2d(3, 4);


    DH[0][0] = 0.0;
    DH[1][0] = 0.3;
    DH[2][0] = 0.4;

    DH[0][1] =  90.0/180.0*PI;
    DH[1][1] =   0.0/180.0*PI;
    DH[2][1] =   0.0/180.0*PI;

    DH[0][2] = 0.0;
    DH[1][2] = 0.0;
    DH[2][2] = 0.0;

    DH[0][3] = 0.0/180.0*PI;
    DH[1][3] = 0.0/180.0*PI;
    DH[2][3] = 0.0/180.0*PI;


    return DH;

}

double **readDHSevilleArm2(){

/*
    Initialize the DH table for the Seville Arm 2

    output:

        double **DH	dim: 6x4	DH table [a alpha d theta] ?

    Author: E.Cataldi, 2017, SevilleExp
*/

    double **DH = allocateArray2d(3, 4);


    DH[0][0] = 0.0;
    DH[1][0] = 0.3;
    DH[2][0] = 0.4;

    DH[0][1] =  90.0/180.0*PI;
    DH[1][1] =   0.0/180.0*PI;
    DH[2][1] =   0.0/180.0*PI;

    DH[0][2] = 0.0;
    DH[1][2] = 0.0;
    DH[2][2] = 0.0;

    DH[0][3] = 0.0/180.0*PI;
    DH[1][3] = 0.0/180.0*PI;
    DH[2][3] = 0.0/180.0*PI;

    return DH;

}
double **readDHSevilleVehicle(int id){

/*
    Initialize the DH table for the Seville Vehicle

    input:
        int     id  dim 1       1=arm 1, 2=arm 2

    output:

        double **DH	dim: 2x4	DH table [a alpha d theta] ?

    Author: E.Cataldi, 2017, SevilleExp
*/

    double **DH = allocateArray2d(2, 4);

    if (id==1){
        DH[0][0] = 0.00;
        DH[1][0] = 0.0;

        DH[0][1] = 0.0/180.0*PI;
        DH[1][1] = 0.0/180.0*PI;

        DH[0][2] = 0.0;
        DH[1][2] = 0.0;

        DH[0][3] = 0.0/180.0*PI;
        DH[1][3] = 0.0/180.0*PI;

    } else if (id==2){
        DH[0][0] = 0.00;
        DH[1][0] = 0.0;

        DH[0][1] = 0.0/180.0*PI;
        DH[1][1] = 0.0/180.0*PI;

        DH[0][2] = 0.0;
        DH[1][2] = 0.0;

        DH[0][3] = 0.0/180.0*PI;
        DH[1][3] = 0.0/180.0*PI;

    }

    return DH;

}


double **elaborateDHAdd(int joints, double *q, double **DH){

/*
    Add the fourth column to a DH table

    input:

        int joints 	dim: 1		number of DoFs
        double *q	dim: 1		joint positions array
        double **DH	dim: nx4	DH table

    output:

        double **DH	dim: nx4	DH table

    Author: E.Cataldi, 2017, SevilleExp
*/



    for(int i=0; i<joints; i++){
        DH[i][3] = DH[i][3]+q[i];
    }

    return DH;
}

