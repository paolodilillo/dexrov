/*

Code for the arm inverse kinematics computation (Position/Orientation Task)

input: 

- desired end-effector pose in arm frame
- current joint positions


output:

- desired joint velocities 
- current end-effector pose in arm frame
- current end-effector velocity in arm frame
- current pose error in arm frame



*/



#include <ros/ros.h>
#include "ExtendedTaskHierarchy.h"
#include <Eigen/Core>
#include <sensor_msgs/JointState.h>
#include <vector>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <dexrov_msgs/CartesianError.h>
#include <dexrov_msgs/Command.h>
#include <geometry_msgs/Twist.h>

using namespace std;
using namespace Eigen;


VectorXd q(10), qGazebo(6);

int flag;
bool q_initialized = false;
bool pose_initialized = false;
bool direct_manipulation = false;
bool rov_pose_init = false;

ros::Publisher ee_pose_inertial_pub;
bool Orientation = true;


int timer_pose = 0;
int timer_joint = 0;
double timeout = 2;

vector<double> vehicle_pos_d(3);
vector<double> par_vehicle_obstacle(5); 

vector<double> paramsState(15);

vector<double> pos_d(3), ori_d(4), des(7);
vector<double> vehicle_ori_d(3);




void manipulationCB(const dexrov_msgs::Command msg){


if(msg.ID == 1 && msg.command == 1){


	direct_manipulation = true;
	cout << "\nEntering teleoperation mode \n\n";

}


else if(msg.ID == 0 && msg.command == 1){


	direct_manipulation = false;
	cout << "\nEntering cognitive engine mode \n\n";
}


else{

	direct_manipulation = false;

}


}

void eeCB(const geometry_msgs::PoseStamped msg){



	Vector3d pose_in_odom;
	VectorXd quaternion_in_odom(4);
	
	pose_in_odom(0) = msg.pose.position.x;
	pose_in_odom(1) = msg.pose.position.y;
	pose_in_odom(2) = msg.pose.position.z;
	
	quaternion_in_odom(0) = msg.pose.orientation.x;
	quaternion_in_odom(1) = msg.pose.orientation.y;
	quaternion_in_odom(2) = msg.pose.orientation.z;
	quaternion_in_odom(3) = msg.pose.orientation.w;
	
	Vector3d pose_in_ik;
	VectorXd quaternion_in_ik(4);
	
	pose_in_ik = odom2ik(pose_in_odom);
	quaternion_in_ik = odom2ik_rotation(quaternion_in_odom);
	
	if(direct_manipulation == false){

	pos_d[0] = msg.pose.position.x;
	pos_d[1] = msg.pose.position.y;
	pos_d[2] = msg.pose.position.z;
	
	ori_d[0] = msg.pose.orientation.x;
	ori_d[1] = msg.pose.orientation.y;
	ori_d[2] = msg.pose.orientation.z;
	ori_d[3] = msg.pose.orientation.w;
	
	des[0] = msg.pose.orientation.x;
	des[1] = msg.pose.orientation.y;
	des[2] = msg.pose.orientation.z;
	des[3] = msg.pose.orientation.w;
	des[4] = msg.pose.position.x;
	des[5] = msg.pose.position.y;
	des[6] = msg.pose.position.z;

	pose_initialized = true;
	
}
}


void direct_eeCB(const geometry_msgs::PoseStamped msg){

	Vector3d pose_in_odom;
	
	pose_in_odom(0) = msg.pose.position.x;
	pose_in_odom(1) = msg.pose.position.y;
	pose_in_odom(2) = msg.pose.position.z;
	
	
	Vector3d pose_in_ik;
	
	pose_in_ik = odom2ik(pose_in_odom);
	
	if(direct_manipulation == true){
	
	pos_d[0] = pose_in_ik(0);
	pos_d[1] = pose_in_ik(1);
	pos_d[2] = pose_in_ik(2);


	timer_pose = 0;
	pose_initialized = true;
	
	}



	
	
}

void jointCB(const sensor_msgs::JointState::ConstPtr& msg){

/*

Callback for acquiring the current joint positions

*/

qGazebo(0) = msg->position[0];
qGazebo(1) = msg->position[2];
qGazebo(2) = msg->position[4];
qGazebo(3) = msg->position[6];
qGazebo(4) = msg->position[8];
qGazebo(5) = msg->position[10];


VectorXd qq(6);
qq = mask_Gazebo2DH_sim(qGazebo, qGazebo.size());

q(4) = qq(0);
q(5) = qq(1);
q(6) = qq(2);
q(7) = qq(3);
q(8) = qq(4);
q(9) = qq(5);

//cout << q << "\n\n";

timer_joint = 0;
q_initialized = true;



}


void rovposeCB(const geometry_msgs::PoseStamped msg){


VectorXd quaternion(4);
quaternion(0) = msg.pose.orientation.x;
quaternion(1) = msg.pose.orientation.y;
quaternion(2) = msg.pose.orientation.z;
quaternion(3) = msg.pose.orientation.w;

Matrix3d  rot = quat2rot(quaternion);

paramsState[0] = rot(0,0);
paramsState[1] = rot(0,1);
paramsState[2] = rot(0,2);
paramsState[3] = rot(1,0);
paramsState[4] = rot(1,1);
paramsState[5] = rot(1,2);
paramsState[6] = rot(2,0);
paramsState[7] = rot(2,1);
paramsState[8] = rot(2,2);


Vector3d rpy = quat2rpy(quaternion);

q(0) = msg.pose.position.x;
q(1) = msg.pose.position.y;
q(2) = msg.pose.position.z;
q(3) = rpy(0);

paramsState[9] = msg.pose.position.x;
paramsState[10] = msg.pose.position.y;
paramsState[11] = msg.pose.position.z;
paramsState[12] = rpy(2);
paramsState[13] = rpy(1);
paramsState[14] = rpy(0);


MatrixXd TT(4,4);
TT = directkinematics(q.head(3), rot, q.tail(6),1);

VectorXd quat22(4);
quat22 << 0,0,0,1;

Matrix3d R_ee;
R_ee(0,0) = TT(0,0);
R_ee(0,1) = TT(0,1);
R_ee(0,2) = TT(0,2);

R_ee(1,0) = TT(1,0);
R_ee(1,1) = TT(1,1);
R_ee(1,2) = TT(1,2);

R_ee(2,0) = TT(2,0);
R_ee(2,1) = TT(2,1);
R_ee(2,2) = TT(2,2);


quat22 = rot2quat(R_ee);


geometry_msgs::PoseStamped msgout;
msgout.pose.position.x = TT(0,3);
msgout.pose.position.y = TT(1,3);
msgout.pose.position.z = TT(2,3);
msgout.pose.orientation.x = quat22(0);
msgout.pose.orientation.y = quat22(1);
msgout.pose.orientation.z = quat22(2);
msgout.pose.orientation.w = quat22(3);

ee_pose_inertial_pub.publish(msgout);



  vehicle_pos_d[0] = msg.pose.position.x;
  vehicle_pos_d[1] = msg.pose.position.y;
  vehicle_pos_d[2] = msg.pose.position.z;
  
  

rov_pose_init = true;


}

void panelCB(const geometry_msgs::PoseStamped msg){


par_vehicle_obstacle[2] = msg.pose.position.x;
par_vehicle_obstacle[2] = msg.pose.position.y;
par_vehicle_obstacle[2] = msg.pose.position.z;

Vector3d panel;
panel(0) = msg.pose.position.x;
panel(1) = msg.pose.position.y;
panel(2) = msg.pose.position.z;
/*
vehicle_ori_d[0] = panel(0);
vehicle_ori_d[1] = panel(1);
vehicle_ori_d[2] = panel(2);
*/

vehicle_ori_d[0] = 0;
vehicle_ori_d[1] = 0;
vehicle_ori_d[2] = 0;

}


int main(int argc, char* argv[]){



  ros::init(argc, argv, "dexrov_inverse_kinematics_left");
  ros::NodeHandle n;
 
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("/arm/L/req_joint_velocity", 1);
  ros::Publisher joint_pub_sim = n.advertise<sensor_msgs::JointState>("/arm/L/desired_joint_velocity", 1);
 
  ros::Publisher ee_pose_pub = n.advertise<geometry_msgs::PoseStamped>("/arm/L/end_effector_pose_hf",1);

  ee_pose_inertial_pub = n.advertise<geometry_msgs::PoseStamped>("/arm/L/end_effector_pose_inertial_hf",1);

  ros::Publisher twist_pub = n.advertise<geometry_msgs::Twist>("/rov/cmd_vel",1);
  
  ros::Publisher error_pub = n.advertise<geometry_msgs::PoseStamped>("/debug/arm/L/pose_error", 1);
  ros::Publisher manip_pub = n.advertise<geometry_msgs::PoseStamped>("/matlab/manipulability", 1);
  
  
  
  
  ros::Subscriber joint_sub_sim = n.subscribe("/arm/joint_states", 1, jointCB);
//  ros::Subscriber joint_sub = n.subscribe("/arm/L/fbk_joint_position", 1, jointCB);
  
  ros::Subscriber ee_sub = n.subscribe("/cog_proxy/L/desired_end_effector_pose", 1, eeCB);
  ros::Subscriber direct_ee_sub = n.subscribe("/mcc/L/direct_manipulation", 1, direct_eeCB);
  ros::Subscriber manipulation_sub = n.subscribe("/mcc/manipulation_master", 1, manipulationCB);
  ros::Subscriber pose_vehicle_sub = n.subscribe("/rov/pose_high", 1, rovposeCB);
  ros::Subscriber panel_sub = n.subscribe("/panel_pose", 1, panelCB);

  ros::Rate loop_rate(100);
  
  
  bool controlOrientation = false;
  double kp = 5.0;
  double ko = 12.0;
  
  n.getParam("inverse_kinematics_left/controlOrientation",controlOrientation);
  n.getParam("inverse_kinematics_left/positionGain",kp);
  n.getParam("inverse_kinematics_left/orientationGain",ko);
  
  
  VectorXd dq(10);
  Vector3d position;
  VectorXd orientation(4);
  VectorXd qdot(6);
  VectorXd qdot_vehicle(4);
  
  
  sensor_msgs::JointState vel_msg_sim;
  vel_msg_sim.velocity.resize(6);
  
   
  MatrixXd Kt(6,6); 
  Matrix3d K;
  MatrixXd K_ori(1,1), K_manip(1,1);

K << kp,0,0,
     0,kp,0.0,
     0,0,kp;
  

K_ori << 1;
 
K_manip << 0.01;
           
  Kt << kp,0,0,0,0,0,
	0,kp,0,0,0,0,
	0,0,kp,0,0,0,
	0,0,0,ko,0,0,
	0,0,0,0,ko,0,
	0,0,0,0,0,ko;



  ExtendedTaskHierarchy ext_hierarchy;




  
vector<double> manip_d(2), manip_d2(2);

manip_d[0] = 0.03;
manip_d[1] = 999;


manip_d2[0] = 0.2;
manip_d2[1] = 999;



par_vehicle_obstacle[0] = 3;
par_vehicle_obstacle[1] = 999;

par_vehicle_obstacle[2] = 0.1;
par_vehicle_obstacle[3] = 0.1;
par_vehicle_obstacle[4] = 0.1;

vector<double> par_wall(11), par_wall2(11), par_wall3(11), par_wall4(11), par_wall5(11), par_wall6(11);
MatrixXd K_wall(1,1);

K_wall << 1;


// Z FORWARD//
par_wall[0] = 0.1;
par_wall[1] = 999;

par_wall[2] = 0;	par_wall[5] = 1;	par_wall[8] = 0;
par_wall[3] = 0;	par_wall[6]= 0;		par_wall[9] = 1;
par_wall[4] = 1.2;	par_wall[7] = 1.2;	par_wall[10] = 1.2;


// Z BACKWARD//
par_wall2[0] = 0.1;
par_wall2[1] = 999;

par_wall2[2] = 0;	par_wall2[5] = 1;	par_wall2[8] = 0;
par_wall2[3] = 0.0;	par_wall2[6]= 0.0;	par_wall2[9] = 1.0;
par_wall2[4] = 0.7;	par_wall2[7] = 0.7;	par_wall2[10] = 0.7;



// Y DOWN//
par_wall3[0] = 0.1;
par_wall3[1] = 999;

par_wall3[2] = 0;	par_wall3[5] = 1;	par_wall3[8] = 0;
par_wall3[3] = -0.05;	par_wall3[6]=  -0.05;	par_wall3[9] = -0.05;
par_wall3[4] = 0.0;	par_wall3[7] = 0.0;	par_wall3[10] = 1.0;


// Y UP//

par_wall4[0] = 0.1;
par_wall4[1] = 999;

par_wall4[2] = 0;	par_wall4[5] = 1;	par_wall4[8] = 0;
par_wall4[3] = -0.5;	par_wall4[6]=  -0.5;	par_wall4[9] = -0.5;
par_wall4[4] = 0.0;	par_wall4[7] = 0.0;	par_wall4[10] = 1.0;


// X left//

par_wall5[0] = 0.1;
par_wall5[1] = 999;

par_wall5[2] = -0.4;	par_wall5[5] = -0.4;	par_wall5[8] = -0.4;
par_wall5[3] = -0.0;	par_wall5[6]= 0.0;	par_wall5[9] = 1.0;
par_wall5[4] = 0.0;	par_wall5[7] = 1.0;	par_wall5[10] = 0.0;



// X right//
par_wall6[0] = 0.1;
par_wall6[1] = 999;

par_wall6[2] = 0.15;	par_wall6[5] = 0.15;	par_wall6[8] = 0.15;
par_wall6[3] = 0.0;	par_wall6[6]=  1.0;	par_wall6[9] = 0.0;
par_wall6[4] = 0.0;	par_wall6[7] = 0.0;	par_wall6[10] = 1.0;
  
  
  
  vector<double> joint1_limit(2), joint2_limit(2),joint3_limit(2),joint4_limit(2),joint5_limit(2),joint6_limit(2); 
  
  joint1_limit[0] = 0;
  joint1_limit[1] = 3.09;

  joint2_limit[0] = -0.25;
  joint2_limit[1] = 1.15;
  
  joint3_limit[0] = -4;
  joint3_limit[1] = -1.62;
  
  joint4_limit[0] = -0.1;
  joint4_limit[1] = 5.9;
  
  joint5_limit[0] = -1.7;
  joint5_limit[1] = 1.85;
  
  joint6_limit[0] = -3;
  joint6_limit[1] = 3;
   
   
   
MatrixXd K_joint(1,1);
K_joint << 0.1;   

  Task joint1(1,K_joint, joint1_limit,paramsState);
  Task joint2(2,K_joint, joint2_limit,paramsState);
  Task joint3(3,K_joint, joint3_limit,paramsState);
  Task joint5(5,K_joint, joint5_limit,paramsState);
  Task oripos_coordinated(23,Kt,des, paramsState);
  Task pos_coordinated(24,K,pos_d, paramsState);
  Task ori_coordinated(25,K_ori, ori_d, paramsState);
  
  Task vehicle_pos(15, K, vehicle_pos_d, paramsState);
  Task vehicle_ori(16,K_ori,vehicle_ori_d,paramsState);
  Task manipulability(11, K_manip, manip_d,paramsState);
  Task oripos_only_manipulator(27,  Kt, des, paramsState);
  
  Task wall(VIRTUAL_WALL,K_wall, par_wall, paramsState);
  Task wall2(VIRTUAL_WALL,K_wall, par_wall2, paramsState);
  Task wall3(VIRTUAL_WALL,K_wall, par_wall3, paramsState);
  Task wall4(VIRTUAL_WALL,K_wall, par_wall4, paramsState);
  Task wall5(VIRTUAL_WALL,K_wall, par_wall5, paramsState);
  Task wall6(VIRTUAL_WALL,K_wall, par_wall6, paramsState);
  
  
  Task vehicle_obstacle(22,K_wall,par_vehicle_obstacle,paramsState);
  
  

  TaskHierarchy hierarchy;


//  hierarchy.insert(manipulability);  
  hierarchy.insert(vehicle_ori);
  hierarchy.insert(joint1);
  hierarchy.insert(joint2);
  hierarchy.insert(joint3);
  hierarchy.insert(joint5);


  hierarchy.insert(wall);
  hierarchy.insert(wall2);
  
  hierarchy.insert(wall3);
  hierarchy.insert(wall4);
  hierarchy.insert(wall5);
  
  hierarchy.insert(wall6);
  
  
  if(controlOrientation){
    hierarchy.insert(oripos_coordinated);
    cout << "\nCONTROLLING POSITION AND ORIENTATION (LEFT ARM)\n";
  }
  else{
    hierarchy.insert(pos_coordinated);
    cout << "\nCONTROLLING POSITION (LEFT ARM)\n";

  }


  ext_hierarchy.setTaskHierarchy(hierarchy);
  
  Vector3d position_ik, position_odom;
  VectorXd quaternion_ik, quaternion_odom;
  geometry_msgs::PoseStamped pose_msg;
  geometry_msgs::Twist twist_msg;
  

  geometry_msgs::PoseStamped msgerror;
  geometry_msgs::PoseStamped msgmanip;
  geometry_msgs::PoseStamped ee_pose_msg_armbase;

  VectorXd eeposition_ik(3), eeposition_armbase(3);
  VectorXd eequat_ik(4), eequat_armbase(4);
  MatrixXd Rik(3,3);
  MatrixXd Rarmbase(3,3);
  MatrixXd Rtran(3,3);

  Rtran << 0,0,1,
           -1,0,0,
           0,-1,0;

  while (ros::ok())
  {
  int dummy=sign(2);
  
//cout << "\nq: " << q_initialized << "\ndesired pose: "<< pose_initialized << "\nrov pose: "<<rov_pose_init << "\n\n\n";
  if(q_initialized && pose_initialized && rov_pose_init){

  ext_hierarchy.setTaskParametersState(16,paramsState);
  ext_hierarchy.setTaskParameters(16,vehicle_ori_d);
  
  if(controlOrientation){
      ext_hierarchy.setTaskParameters(23,des);  
      ext_hierarchy.setTaskParametersState(23,paramsState);
  }
  else{

    ext_hierarchy.setTaskParameters(24,pos_d);
    ext_hierarchy.setTaskParametersState(24, paramsState);
  }
  
  ros::Time start, end;
  start = ros::Time::now();
  dq = ext_hierarchy.computeDq(q);
  end = ros::Time::now();

//cout << "Tempo: " << end-start << "\n\n";

msgerror.header.stamp = ros::Time::now();

if(controlOrientation){

  msgerror.pose.position.x = ext_hierarchy.getTaskError(23,q)(0);
  msgerror.pose.position.y = ext_hierarchy.getTaskError(23,q)(1);
  msgerror.pose.position.z = ext_hierarchy.getTaskError(23,q)(2);
  msgerror.pose.orientation.x = ext_hierarchy.getTaskError(23,q)(3);
  msgerror.pose.orientation.y = ext_hierarchy.getTaskError(23,q)(4);
  msgerror.pose.orientation.x = ext_hierarchy.getTaskError(23,q)(5);

}
else{
  msgerror.pose.position.x = ext_hierarchy.getTaskError(24,q)(0);
  msgerror.pose.position.y = ext_hierarchy.getTaskError(24,q)(1);
  msgerror.pose.position.z = ext_hierarchy.getTaskError(24,q)(2);
  msgerror.pose.orientation.x = 0.0;
  msgerror.pose.orientation.y = 0.0;
  msgerror.pose.orientation.x = 0.0;
}

msgmanip.header.stamp = ros::Time::now();

msgmanip.pose.position.x = manipulability.taskValue(q)(0);


error_pub.publish(msgerror);
manip_pub.publish(msgmanip);

}
  
  

else{
	dq << 0,0,0,0,0,0,0,0,0,0;
	
  }
 
 
//  cout << "Position error:\n" << ext_hierarchy.getTaskValue(23,q).head(3) << "\n\n";
//  cout << "Orientation error:\n" << ext_hierarchy.getTaskError(23,q).tail(3) << "\n\n\n\n";
//cout << manipulability.taskValue(q)(0) << "\n\n";
//cout << GetEEPosition(q.tail(6)) << "\n\n";
//cout << q.tail(6) << "\n\n\n\n";
  
  qdot = mask_DH_Gazebo_velocity_sim(dq.tail(6),6);
  qdot = VelocitySaturation(qdot);

  qdot_vehicle = dq.head(4);
/*  qdot_vehicle(0) = dq(0);
  qdot_vehicle(1) = dq(1);
  qdot_vehicle(2) = dq(2);
  qdot_vehicle(3) = dq(3);*/
  

  qdot_vehicle = VelocitySaturationVehicle(qdot_vehicle);
  

  
  /*
  for(int i=0;i<4;i++){
  
  	if(dq(i) > 0.2)
  	
  		dq(i) = 0.2;
  		
  	if(dq(i) < -0.2)
  	
  		dq(i) = -0.2;
  
  }
  */
  
  dq.head(4) = qdot_vehicle;
  dq.tail(6) = qdot;
	
  vel_msg_sim.velocity[0] = dq(4);
  vel_msg_sim.velocity[1] = dq(5);
  vel_msg_sim.velocity[2] = dq(6);
  vel_msg_sim.velocity[3] = dq(7);
  vel_msg_sim.velocity[4] = dq(8);
  vel_msg_sim.velocity[5] = dq(9);

  twist_msg.linear.x = dq(0);
  twist_msg.linear.y = dq(1);
  twist_msg.linear.z = dq(2);
  twist_msg.angular.z = dq(3);
  
 
  joint_pub_sim.publish(vel_msg_sim);
  twist_pub.publish(twist_msg);
  
 
  if(q_initialized){
    
    eeposition_ik = GetEEPosition(q.tail(6));
    eequat_ik =GetEEQuaternion(q.tail(6));
   
    eeposition_armbase = eeposition_ik;//rotate_position(eeposition_ik);
    eequat_armbase = eequat_ik;//rotate_orientation(eequat_ik);
    
    ee_pose_msg_armbase.header.stamp = ros::Time::now();
    ee_pose_msg_armbase.pose.position.x = eeposition_armbase(0);
    ee_pose_msg_armbase.pose.position.y = eeposition_armbase(1);
    ee_pose_msg_armbase.pose.position.z = eeposition_armbase(2);

    ee_pose_msg_armbase.pose.orientation.x = eequat_armbase(0);
    ee_pose_msg_armbase.pose.orientation.y = eequat_armbase(1);
    ee_pose_msg_armbase.pose.orientation.z = eequat_armbase(2);
    ee_pose_msg_armbase.pose.orientation.w = eequat_armbase(3);

    ee_pose_pub.publish(ee_pose_msg_armbase);

  }



  ros::spinOnce();
  loop_rate.sleep();
  
  }


}






