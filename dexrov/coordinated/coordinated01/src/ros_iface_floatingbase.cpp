#include <ros/ros.h>
#include "ExtendedTaskHierarchy.h"
#include <Eigen/Core>
#include <sensor_msgs/JointState.h>
#include <vector>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <dexrov_msgs/CartesianError.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf/exceptions.h>

using namespace std;
using namespace Eigen;


bool error_initialized = false;
bool pose_initialized = false;

VectorXd r_ee_pose(7), l_ee_pose(7);
VectorXd r_ee_error(6), l_ee_error(6);

void LeftEndEffectorPoseCB(const geometry_msgs::PoseStamped msg){

	l_ee_pose(0) = msg.pose.position.x;
	l_ee_pose(1) = msg.pose.position.y;
	l_ee_pose(2) = msg.pose.position.z;
	l_ee_pose(3) = msg.pose.orientation.x;
	l_ee_pose(4) = msg.pose.orientation.y;
	l_ee_pose(5) = msg.pose.orientation.z;
	l_ee_pose(6) = msg.pose.orientation.w;
		
	pose_initialized = true;

}

void RightEndEffectorPoseCB(const geometry_msgs::PoseStamped msg){

	r_ee_pose(0) = msg.pose.position.x;
	r_ee_pose(1) = msg.pose.position.y;
	r_ee_pose(2) = msg.pose.position.z;
	r_ee_pose(3) = msg.pose.orientation.x;
	r_ee_pose(4) = msg.pose.orientation.y;
	r_ee_pose(5) = msg.pose.orientation.z;
	r_ee_pose(6) = msg.pose.orientation.w;
		
	pose_initialized = true;

}

void LeftEndEffectorErrorCB(const geometry_msgs::PoseStamped msg){

	l_ee_error(0) = msg.pose.position.x;
	l_ee_error(1) = msg.pose.position.y;
	l_ee_error(2) = msg.pose.position.z;
	l_ee_error(3) = msg.pose.orientation.x;
	l_ee_error(4) = msg.pose.orientation.y;
	l_ee_error(5) = msg.pose.orientation.z;
		
	error_initialized = true;

}

void RightEndEffectorErrorCB(const geometry_msgs::PoseStamped msg){

	r_ee_error(0) = msg.pose.position.x;
	r_ee_error(1) = msg.pose.position.y;
	r_ee_error(2) = msg.pose.position.z;
	r_ee_error(3) = msg.pose.orientation.x;
	r_ee_error(4) = msg.pose.orientation.y;
	r_ee_error(5) = msg.pose.orientation.z;
		
	error_initialized = true;

}


int main(int argc, char* argv[]){



 ros::init(argc, argv, "inverse_kinematics_ros_iface_floatingbase");

 ros::NodeHandle n;
 //tf::TransformListener mytf;
 //sleep(3);
  
 
 ros::Publisher ee_pub_r = n.advertise<geometry_msgs::PoseStamped>("/arm/R/end_effector_pose", 1);
 ros::Publisher ee_pub_l = n.advertise<geometry_msgs::PoseStamped>("/arm/L/end_effector_pose", 1);
 ros::Publisher ee_error_pub_r = n.advertise<dexrov_msgs::CartesianError>("/arm/R/cartesian_error", 1);
 ros::Publisher ee_error_pub_l = n.advertise<dexrov_msgs::CartesianError>("/arm/L/cartesian_error", 1);
 
 ros::Subscriber ee_l_pose_sub = n.subscribe("/arm/L/end_effector_pose_inertial_hf", 1, LeftEndEffectorPoseCB);
 ros::Subscriber ee_r_pose_sub = n.subscribe("/arm/R/end_effector_pose_inertial_hf", 1, RightEndEffectorPoseCB);
 
 ros::Subscriber ee_l_error_sub = n.subscribe("/debug/arm/L/pose_error", 1, LeftEndEffectorErrorCB);
 ros::Subscriber ee_r_error_sub = n.subscribe("/debug/arm/R/pose_error", 1, RightEndEffectorErrorCB);

 ros::Rate loop_rate(1);
 
 dexrov_msgs::CartesianError ee_error_msg_r;
 ee_error_msg_r.components.resize(6);
 
 dexrov_msgs::CartesianError ee_error_msg_l;
 ee_error_msg_l.components.resize(6);
 
 geometry_msgs::PoseStamped ee_pose_msg_r;
 geometry_msgs::PoseStamped ee_pose_msg_l;
  
  while (ros::ok())
  {
//  int dummy=sign(2);
  
 
 if(pose_initialized){

	  	
  	ee_pose_msg_l.pose.position.x = l_ee_pose(0);
  	ee_pose_msg_l.pose.position.y = l_ee_pose(1);
  	ee_pose_msg_l.pose.position.z = l_ee_pose(2);
  	ee_pose_msg_l.pose.orientation.x = l_ee_pose(3);
  	ee_pose_msg_l.pose.orientation.y = l_ee_pose(4);
  	ee_pose_msg_l.pose.orientation.z = l_ee_pose(5);
  	ee_pose_msg_l.pose.orientation.w = l_ee_pose(6);
  	
  	ee_pose_msg_r.pose.position.x = r_ee_pose(0);
  	ee_pose_msg_r.pose.position.y = r_ee_pose(1);
  	ee_pose_msg_r.pose.position.z = r_ee_pose(2);
  	ee_pose_msg_r.pose.orientation.x = r_ee_pose(3);
  	ee_pose_msg_r.pose.orientation.y = r_ee_pose(4);
  	ee_pose_msg_r.pose.orientation.z = r_ee_pose(5);
  	ee_pose_msg_r.pose.orientation.w = r_ee_pose(6);
  	
  	
	ee_pub_r.publish(ee_pose_msg_r);
	ee_pub_l.publish(ee_pose_msg_l);
 
 }
 
 if(error_initialized){
 		  
	ee_error_msg_r.components[0] = r_ee_error(0);
	ee_error_msg_r.components[1] = r_ee_error(1);
	ee_error_msg_r.components[2] = r_ee_error(2);  
	ee_error_msg_r.components[3] = r_ee_error(3);
	ee_error_msg_r.components[4] = r_ee_error(4);
	ee_error_msg_r.components[5] = r_ee_error(5);
	
	ee_error_msg_l.components[0] = l_ee_error(0);
	ee_error_msg_l.components[1] = l_ee_error(1);
	ee_error_msg_l.components[2] = l_ee_error(2);  
	ee_error_msg_l.components[3] = l_ee_error(3);
	ee_error_msg_l.components[4] = l_ee_error(4);
	ee_error_msg_l.components[5] = l_ee_error(5);
			  
	ee_error_pub_r.publish(ee_error_msg_r);		// Publish end-effector position/orientation error
	ee_error_pub_l.publish(ee_error_msg_l);
		  
}
	
  	
//	position_odom = ik2odom(position);
//	quaternion_odom = ik2odom_rotation(orientation);
	

  
  ros::spinOnce();
  loop_rate.sleep();
  
}

}
