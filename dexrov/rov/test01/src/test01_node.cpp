#include "ros/ros.h"
#include <dexrov_msgs/Waypoints.h>


int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "talker");

  ros::NodeHandle n;

  ros::Publisher chatter_pub = n.advertise<dexrov_msgs::Waypoints>("/mcc/waypoints", 1);

  ros::Rate loop_rate(100);

  dexrov_msgs::Waypoints msg;
  msg.waypoints_list.resize(3);
  
  while(ros::ok()){
  msg.waypoints_list[0].pose.position.x = 1;
  msg.waypoints_list[0].pose.position.y = 1;
  msg.waypoints_list[0].pose.position.z = 5;
  msg.waypoints_list[0].pose.orientation.x = 0;
  msg.waypoints_list[0].pose.orientation.y = 0;
  msg.waypoints_list[0].pose.orientation.z = 0.7071;
  msg.waypoints_list[0].pose.orientation.w = 0.7071;
  
  
  msg.waypoints_list[1].pose.position.x = 5;
  msg.waypoints_list[1].pose.position.y = 2;
  msg.waypoints_list[1].pose.position.z = 6;
  msg.waypoints_list[1].pose.orientation.x = 0;
  msg.waypoints_list[1].pose.orientation.y = 0;
  msg.waypoints_list[1].pose.orientation.z = 0;
  msg.waypoints_list[1].pose.orientation.w = 1;
  
  
  msg.waypoints_list[2].pose.position.x = 2;
  msg.waypoints_list[2].pose.position.y = 3;
  msg.waypoints_list[2].pose.position.z = 6;
  msg.waypoints_list[2].pose.orientation.x = 0;
  msg.waypoints_list[2].pose.orientation.y = 0;
  msg.waypoints_list[2].pose.orientation.z = -0.7071;
  msg.waypoints_list[2].pose.orientation.w = 0.7071;
  
  chatter_pub.publish(msg);
  
  loop_rate.sleep();
  
  }
  
  return 0;
}
