#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <vehicle_dynamics.h>

using namespace std;
double in[4];
double current[6];

double ComputeVelocity(double volt_des, int id){

double F[2], L[2], V[2], R[2], O[2];
double minV = -5;
double maxV = 5;

F[0] = -1.75;	F[1] = 1.75;
L[0] = -1.25;	L[1] = 1.25;
V[0] = -0.75;	V[1] = 0.75;
R[0] = -120;	R[1] = 120;

switch(id)
{

	case 1:
	
	O[0] = F[0];
	O[1] = F[1];
	
	break;


	case 2:
	
	O[0] = L[0];
	O[1] = L[1];
	
	break;
	
	
	case 3:
	
	O[0] = V[0];
	O[1] = V[1];
	
	break;
	
	
	case 4:
	
	O[0] = R[0];
	O[1] = R[1];
	
	break;

}


double velocity;

velocity = ((volt_des-maxV/minV-maxV))*O[0] - ((volt_des-minV)/(minV-maxV))*O[1];
velocity = volt_des/2;
return velocity; 


}

void CurrentTwistCB(const geometry_msgs::Twist msg)
{

	current[0] = msg.linear.x;
	current[1] = msg.linear.y;
	current[2] = msg.linear.z;
	current[3] = msg.angular.x;
	current[4] = msg.angular.y;
	current[5] = msg.angular.z;

}


void VoltCB(const geometry_msgs::Twist msg)
{
  in[0] = msg.linear.x *30;
  in[1] = msg.linear.y * 30;
  in[2] = msg.linear.z * 30;
  in[3] = msg.angular.z * 30;
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "vehicle_sim");

  ros::NodeHandle n;
  
  ros::Subscriber current_twist_sub = n.subscribe("/rov/twist_vehicle", 1, CurrentTwistCB);
  ros::Publisher twist_pub = n.advertise<geometry_msgs::Twist>("/rov/cmd_vel", 1);
  ros::Subscriber sub = n.subscribe("/volt", 1, VoltCB);
  ros::Rate loop_rate(100);
  VehicleDynamics vd;
  
  geometry_msgs::Twist msg;
  VectorXd nu_c(6);
  nu_c.setZero();
  VectorXd tau(6);
  VectorXd dzita(6);
  Vector3d eta2;
  VectorXd zita(6);
  VectorXd velOut(6);
  velOut.setZero();
  
  while (ros::ok())
  {
    
    zita(0) = current[0];
    zita(1) = current[1];
    zita(2) = current[2];
    zita(3) = current[3];
    zita(4) = current[4];
    zita(5) = current[5];
    
    tau(0) = in[0];
    tau(1) = in[1];
    tau(2) = in[2];
    tau(3) = 0;
    tau(4) = 0;
    tau(5) = in[3];
    
    dzita = vd.DirectDynamics(eta2, zita, tau, nu_c);
    velOut += dzita * 0.01;
    
    msg.linear.x = velOut(0);
    msg.linear.y = velOut(1);
    msg.linear.z = velOut(2);
    msg.angular.x = velOut(3);
    msg.angular.y = velOut(4);
    msg.angular.z = velOut(5);
    
    twist_pub.publish(msg);
    cout << "\n\nUSCITA\n\n";
cout << msg<< "\n\n";
    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
