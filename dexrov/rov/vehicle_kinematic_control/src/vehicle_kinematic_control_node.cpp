#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <math.h>
#include <limits>
#include <ros/ros.h>
#include <cstdlib>
#include <sys/io.h>
#include <sys/mman.h>
#include "dexrov_msgs/Waypoints.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Bool.h"

#include <dexrov_msgs/AutoHeading.h>
#include <dexrov_msgs/AutoDepth.h>
#include <dexrov_msgs/AutoAltitude.h>
#include <ExtendedTaskHierarchy.h>

using namespace Eigen;
using namespace std;

bool new_waypoints = false;
bool flag_goal_pose = false;
bool flag_estimated_pose = false;
bool flag_autoheading = false;
bool flag_autoaltitude = false;
bool flag_autodepth = false;
bool goal_reached = false;

double lin_x_max = 1;
double lin_y_max = 1;
double lin_z_max = 1;
double ang_z_max = 1;

int current_wp = 0;

MatrixXd wayp_list(6,1);
vector<double> reference(6);
VectorXd current_pose(6);

void MsgCallback(const dexrov_msgs::Waypoints::ConstPtr& msg){

VectorXd quat_d(4);
Vector3d rpy_d;

wayp_list.resize(6,msg->waypoints_list.size());

for (int i=0;i<msg->waypoints_list.size();i++){

	wayp_list(0,i) = msg->waypoints_list[i].pose.position.x;
	wayp_list(1,i) = msg->waypoints_list[i].pose.position.y;
	wayp_list(2,i) = msg->waypoints_list[i].pose.position.z;
	
	quat_d(0) =  msg->waypoints_list[i].pose.orientation.x;
	quat_d(1) =  msg->waypoints_list[i].pose.orientation.y;
	quat_d(2) =  msg->waypoints_list[i].pose.orientation.z;
	quat_d(3) =  msg->waypoints_list[i].pose.orientation.w;
	
	rpy_d = quat2rpy(quat_d);
	
	wayp_list(3,i) = rpy_d(2);
	wayp_list(4,i) = rpy_d(1);
	wayp_list(5,i) = rpy_d(0);
	
	
}

	new_waypoints = true;
	flag_goal_pose = true;

}

void MsgCallback_estimated(const geometry_msgs::PoseStamped msg)
{

	VectorXd quat_current(4);
	Vector3d rpy_current;
	quat_current(0) = msg.pose.orientation.x;
	quat_current(1) = msg.pose.orientation.y;
	quat_current(2) = msg.pose.orientation.z;
	quat_current(3) = msg.pose.orientation.w;

	rpy_current = quat2rpy(quat_current);
	
	current_pose(0) = msg.pose.position.x;
	current_pose(1) = msg.pose.position.y;
	current_pose(2) = msg.pose.position.z;
	current_pose(3) = rpy_current(2);
	current_pose(4) = rpy_current(1);
	current_pose(5) = rpy_current(0);
	
	flag_estimated_pose = true;
	
	
}

void subscribeAutoHeading(const dexrov_msgs::AutoHeading& autoHeadingInfo)
{	
	flag_autoheading = autoHeadingInfo.switchSetting;
}

void subscribeAutoDepth(const dexrov_msgs::AutoDepth& autoDepthInfo)
{	
	flag_autodepth = autoDepthInfo.switchSetting;	
}

void subscribeAutoAltitude(const dexrov_msgs::AutoAltitude& autoAltitudeInfo)
{	
	flag_autoaltitude = autoAltitudeInfo.switchSetting;	
}



int main (int argc, char **argv)
{
	ros::init(argc, argv, "vehicle_kinematic_control");
	ros::NodeHandle nh;
	ros::Rate loop_rate(10);
	
	ros::Publisher twist_publisher;
	ros::Publisher reached_publisher;
	ros::Publisher error_publisher;
	ros::Subscriber estimated_pose_subscriber;
	ros::Subscriber desired_pose_subscriber;

	ros::Subscriber autoaltitude_subscriber;
	ros::Subscriber autoheading_subscriber;
	ros::Subscriber autodepth_subscriber;

	twist_publisher = nh.advertise<geometry_msgs::Twist>("/rov/cmd_vel", 1);
	reached_publisher = nh.advertise<dexrov_msgs::Waypoints>("/rov/waypoints_reached", 1);
	error_publisher = nh.advertise<geometry_msgs::PoseStamped>("/rov/error", 1);

	estimated_pose_subscriber = nh.subscribe("/rov/pose_high", 1, MsgCallback_estimated);
	desired_pose_subscriber = nh.subscribe("/mcc/waypoints", 1, MsgCallback);
	autoheading_subscriber = nh.subscribe("/rov/autohead_ros", 1, subscribeAutoHeading);
	autodepth_subscriber = nh.subscribe("/rov/autodepth_ros", 1, subscribeAutoDepth);
	autoaltitude_subscriber = nh.subscribe("/rov/autoalt_ros", 1, subscribeAutoAltitude);
	
	double PositionGain, OrientationGain, DistanceStop, AngleStop;
	double lin_x_max= 0.5;
	double lin_y_max = 0.5;
	double lin_z_max = 0.5;
	double ang_z_max = 3;

	ros::param::get("~PositionGain", PositionGain);
	ros::param::get("~OrientationGain", OrientationGain);
	ros::param::get("~DistanceStop", DistanceStop);
	ros::param::get("~AngleStop", AngleStop);
	ros::param::get("~MaximumLinearVelx", lin_x_max);
	ros::param::get("~MaximumLinearVely", lin_y_max);
	ros::param::get("~MaximumLinearVelz", lin_z_max);
	ros::param::get("~MaximumAngularVel", ang_z_max);
	
	VectorXd dq(6);
	VectorXd quat_reached(4);
	Vector3d rpy_temp;
	geometry_msgs::Twist velocity_msg;
	
	MatrixXd K(6,6);
	VectorXd gain(6);
	gain << PositionGain,PositionGain,PositionGain,0,0,OrientationGain;
	K = gain.asDiagonal();
	
	Task vehicle_oripos(17,K,reference);
	TaskHierarchy hierarchy;
	ExtendedTaskHierarchy ext_hierarchy;
	hierarchy.insert(vehicle_oripos);
	ext_hierarchy.setTaskHierarchy(hierarchy);
	
	while(ros::ok()){
	
	if(flag_goal_pose && flag_estimated_pose && new_waypoints){
	
	
		reference[0] = wayp_list(0,current_wp);
		reference[1] = wayp_list(1,current_wp);
		reference[2] = wayp_list(2,current_wp);
		reference[3] = wayp_list(3,current_wp);
		reference[4] = wayp_list(4,current_wp);
		reference[5] = wayp_list(5,current_wp);

		
		if(flag_autoaltitude){
			reference[2] = current_pose[2];
		}
		if(flag_autodepth){
			reference[2] = current_pose[2];
		}
		if(flag_autoheading){
			reference[5] = current_pose[5];
		}
	
		ext_hierarchy.setTaskParameters(17,reference);
		

//cout << "\n\n" << ext_hierarchy.getTaskError(17,current_pose)(0)+ext_hierarchy.getTaskError(17,current_pose)(1)+ext_hierarchy.getTaskError(17,current_pose)(2)<< "\n\n";

Vector3d position_error;
position_error = ext_hierarchy.getTaskError(17,current_pose).head(3);
cout << position_error << "\n\n";
//cout << "\n\n" << ext_hierarchy.getTaskError(17,current_pose)(5) << "\n\n";
/*
		if(ext_hierarchy.getTaskError(17,current_pose).head(3).norm() < DistanceStop && ext_hierarchy.getTaskError(17,current_pose)(5) < AngleStop){
		
			goal_reached = true;
		
			if(current_wp < wayp_list.cols()-1){
			
				current_wp++;
				
			}
			else{
			
				current_wp = 0;
				new_waypoints = false;
			
			}
			
			
			
		}
*/		
		dq = ext_hierarchy.computeDq(current_pose);
		
	}
	
	else{
	
		dq << 0,0,0,0,0,0;
	}
	
	
	/* Velocity saturation */
	if(dq(0) > lin_x_max)
		dq(0) = lin_x_max;
	if(dq(0) < -lin_x_max)
		dq(0) = -lin_x_max;
		
	if(dq(1) > lin_y_max)
		dq(1) = lin_y_max;
	if(dq(1) < -lin_y_max)
		dq(1) = -lin_y_max;
		
	if(dq(2) > lin_z_max)
		dq(2) = lin_z_max;
	if(dq(2) < -lin_z_max)
		dq(2) = -lin_z_max;
		
	if(dq(5) > ang_z_max)
		dq(5) = ang_z_max;
	if(dq(5) < -ang_z_max)
		dq(5) = -ang_z_max;
		
	velocity_msg.linear.x = dq(0);
	velocity_msg.linear.y = dq(1);
	velocity_msg.linear.z = dq(2);
	velocity_msg.angular.x = 0;
	velocity_msg.angular.y = 0;
	velocity_msg.angular.z = dq(5);
	twist_publisher.publish(velocity_msg);

	if(goal_reached){

		dexrov_msgs::Waypoints reached_msg;
		
		reached_msg.waypoints_list.resize(1);
		
		reached_msg.waypoints_list[0].header.stamp = ros::Time::now();
		reached_msg.waypoints_list[0].header.frame_id = "odom";
		
		reached_msg.waypoints_list[0].pose.position.x = reference[0];
		reached_msg.waypoints_list[0].pose.position.y = reference[1];
		reached_msg.waypoints_list[0].pose.position.z = reference[2];
			
		rpy_temp(0) = reference[3];
		rpy_temp(1) = reference[4];
		rpy_temp(2) = reference[5];
			
		quat_reached = rpy2quat(rpy_temp);
		reached_msg.waypoints_list[0].pose.orientation.x = quat_reached(0);
		reached_msg.waypoints_list[0].pose.orientation.y = quat_reached(1);
		reached_msg.waypoints_list[0].pose.orientation.z = quat_reached(2);
		reached_msg.waypoints_list[0].pose.orientation.w = quat_reached(3);
		
		reached_publisher.publish(reached_msg);
		goal_reached = false;
	
	}
	
	ros::spinOnce();
	loop_rate.sleep();
	
	}
	
}


