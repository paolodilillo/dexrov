#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <ExtendedTaskHierarchy.h>
#include <MiniPID.h>

using namespace std;

double des[4];
double current[6];
Matrix3d R;
bool init = false;
VectorXd temp(6);



double ComputeFeedforward(double v_des, int id){

double F[2], L[2], V[2], R[2], O[2];
double minV = -5;
double maxV = 5;


F[0] = -1.75;	F[1] = 1.75;
L[0] = -1.25;	L[1] = 1.25;
V[0] = -0.75;	V[1] = 0.75;
R[0] = -120;	R[1] = 120;

switch(id)
{

	case 1:
	
	O[0] = F[0];
	O[1] = F[1];
	
	break;


	case 2:
	
	O[0] = L[0];
	O[1] = L[1];
	
	break;
	
	
	case 3:
	
	O[0] = V[0];
	O[1] = V[1];
	
	break;
	
	
	case 4:
	
	O[0] = R[0];
	O[1] = R[1];
	
	break;

}


double volt;

volt = ((v_des-O[1])/(O[0]-O[1]))*minV - ((v_des-O[0])/(O[0]-O[1]))*maxV;

return volt; 


}



void DesiredTwistCB(const geometry_msgs::Twist msg)
{

	des[0] = msg.linear.x;
	des[1] = msg.linear.y;
	des[2] = msg.linear.z;
	des[3] = msg.angular.z;

}


void CurrentTwistCB(const geometry_msgs::Twist msg)
{

	
	VectorXd zita(6);
	
	temp(0) = msg.linear.x;
	temp(1) = msg.linear.y;
	temp(2) = msg.linear.z;
	temp(3) = msg.angular.x;
	temp(4) = msg.angular.y;
	temp(5) = msg.angular.z;

	zita = R.transpose() * temp;
	current[0] = zita(0);
	current[1] = zita(1);
	current[2] = zita(2);
	current[3] = zita(3);
	current[4] = zita(4);
	current[5] = zita(5);

}

void CurrentPoseCB(const geometry_msgs::PoseStamped msg)
{


	VectorXd CurrQuat(4);
	CurrQuat(0) = msg.pose.orientation.x;
	CurrQuat(1) = msg.pose.orientation.y;
	CurrQuat(2) = msg.pose.orientation.z;
	CurrQuat(3) = msg.pose.orientation.w;

	R = quat2rot(CurrQuat);
	init = true;
}




int main(int argc, char **argv)
{
  
	ros::init(argc, argv, "vehicle_dynamic_control");

	ros::NodeHandle n;
	ros::Subscriber desired_twist_sub = n.subscribe("/rov/cmd_vel2", 1, DesiredTwistCB);
	ros::Subscriber current_twist_sub = n.subscribe("/rov/twist", 1, CurrentTwistCB);
	ros::Subscriber current_pose_sub = n.subscribe("/rov/pose_high", 1, CurrentPoseCB);
	ros::Publisher twist_pub = n.advertise<geometry_msgs::Twist>("/volt", 1);
	ros::Publisher twist_pub_vehicle = n.advertise<geometry_msgs::Twist>("/rov/twist_vehicle",1);
	ros::Rate loop_rate(1000);
  	
  	double Kpx, Kdx, Kix, Kpy, Kdy, Kiy, Kpz, Kdz, Kiz, Kpr, Kdr, Kir;
  	double max = 500.0;
  	double min = -500.0;
  	double MaxIx, MaxIy, MaxIz, MaxIr;
  	double feedforward_x = 0.0, feedforward_y = 0.0, feedforward_z = 0.0, feedforward_r = 0.0;
  	double output[4];
  	bool ComputeFeedForward = false;
  	
  	ros::param::get("~Kpx", Kpx);
	ros::param::get("~Kdx", Kdx);
	ros::param::get("~Kix", Kix);
	ros::param::get("~MaxIx", MaxIx);
	
  	ros::param::get("~Kpy", Kpy);
	ros::param::get("~Kdy", Kdy);
	ros::param::get("~Kiy", Kiy);
	ros::param::get("~MaxIy", MaxIy);
	
  	ros::param::get("~Kpz", Kpz);
	ros::param::get("~Kdz", Kdz);
	ros::param::get("~Kiz", Kiz);
	ros::param::get("~MaxIz", MaxIz);
	
  	ros::param::get("~Kpr", Kpr);
	ros::param::get("~Kdr", Kdr);
	ros::param::get("~Kir", Kir);
	ros::param::get("~MaxIr", MaxIr);
	
	ros::param::get("~ComputeFeedForward", ComputeFeedForward);

	geometry_msgs::Twist TwistMsg;
	
	MiniPID pid01(Kpx,Kix,Kdx);
	pid01.setOutputLimits(min,max);
	pid01.setMaxIOutput(MaxIx);
		
	MiniPID pid02(Kpy,Kiy,Kdy);
	pid02.setOutputLimits(min,max);
	pid02.setMaxIOutput(MaxIy);

	MiniPID pid03(Kpz,Kiz,Kdz);
	pid03.setOutputLimits(min,max);
	pid03.setMaxIOutput(MaxIz);

	MiniPID pid04(Kpr,Kir,Kdr);
	pid04.setOutputLimits(min,max);
	pid04.setMaxIOutput(MaxIr);	
		
	while (ros::ok())
	{
	
	if(!init){
		ros::spinOnce();
		loop_rate.sleep();
		
		}
		
		
	if(ComputeFeedForward){
		feedforward_x = ComputeFeedforward(des[0],1);
		feedforward_y = ComputeFeedforward(des[1],2);
		feedforward_z = ComputeFeedforward(des[2],3);
		feedforward_r = ComputeFeedforward(des[3],4);
	}
		pid01.setF(feedforward_x);
		pid02.setF(feedforward_y);
		pid03.setF(feedforward_z);
		pid04.setF(feedforward_r);
		
		output[0] = pid01.getOutput(current[0], des[0]);
		output[1] = pid02.getOutput(current[1], des[1]);
		output[2] = pid03.getOutput(current[2], des[2]);// - 1.2;
		output[3] = pid04.getOutput(temp(5), des[3]);
		   
		TwistMsg.linear.x = output[0];
		TwistMsg.linear.y = output[1];
		TwistMsg.linear.z = output[2];
		TwistMsg.angular.z = output[3];
		twist_pub.publish(TwistMsg);
		
		geometry_msgs::Twist TwistBodyFrame;
		TwistBodyFrame.linear.x = current[0];
		TwistBodyFrame.linear.y = current[1];
		TwistBodyFrame.linear.z = current[2];
		TwistBodyFrame.linear.x = current[3];
		TwistBodyFrame.linear.y = current[4];
		TwistBodyFrame.linear.z = current[5];
		twist_pub_vehicle.publish(TwistBodyFrame);
		
	       	for(int i=0;i<4;i++){
	       	
	       		cout << output[i] << "\n";
	       	}
	       	
	       	cout << "\n\n";
	       	
		ros::spinOnce();
		loop_rate.sleep();
	    
	}


	return 0;
}
