#include "ros/ros.h"
#include <geometry_msgs/Twist.h>



int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "noise");

  ros::NodeHandle n;

  ros::Publisher chatter_pub = n.advertise<geometry_msgs::Twist>("rov/cmd_vel", 1);

  ros::Rate loop_rate(100);

  
  geometry_msgs::Twist msg;
  
  double t = 0.0;
  double ts = 0.01;
  double a = 0.1;
  double omega = 0.5;
  
  while (ros::ok())
  {
   msg.linear.y = a * cos(omega*t);
   msg.linear.x = a * sin(omega*t);
   msg.linear.z = 0;//a/2 * cos(omega*t);
   t += ts;
  
    chatter_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
