#include <ros/ros.h>
#include <iostream>
#include <cstdlib>
#include <math.h> 

#include <Eigen/Dense>
#include <limits>


#include <dexrov_msgs/AutoHeading.h>
#include <dexrov_msgs/AutoDepth.h>
#include <dexrov_msgs/AutoAltitude.h>
#include <dexrov_msgs/CameraTilt.h>
#include <dexrov_msgs/LightControl.h>

#include <dexrov_msgs/RovTeleopCmd.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>

#include <std_msgs/Float32MultiArray.h>

//#include "qtpkg/inputs.h"
//#include <isme_rov_platform_baseline/Volts.h>
//#include <std_msgs/String.h>

#include <diagnostic_msgs/DiagnosticStatus.h>

#include <dexrov_msgs/Command.h>
#include <geometry_msgs/Twist.h>

using namespace Eigen;
using namespace std;

dexrov_msgs::Command msg_CmdAck;

dexrov_msgs::AutoHeading msg_AutoHeading;
dexrov_msgs::AutoDepth msg_AutoDepth;
dexrov_msgs::AutoAltitude msg_AutoAltitude;
dexrov_msgs::CameraTilt msg_CameraTilt;
dexrov_msgs::LightControl msg_LightControl;

std_msgs::Float32MultiArray sensors_data_msg;

geometry_msgs::PoseStamped msg_Tilt_value;
geometry_msgs::TwistStamped msg_Twist;

dexrov_msgs::RovTeleopCmd msg_Volt;

ros::Publisher command_ack_publisher;
ros::Publisher twist_publisher;
ros::Publisher *tilt_publisher;
ros::Publisher *sensors_publisher;
ros::Publisher rov_pub;

ros::Publisher voltage_publisher;

ros::Publisher diagnostic_publisher;

ros::Subscriber sub_AutoHeading;
ros::Subscriber sub_AutoDepth;
ros::Subscriber sub_AutoAltitude;
ros::Subscriber sub_CameraTilt;
ros::Subscriber sub_LightControl;

ros::Subscriber sub_TeleOp_Thrusters;
//ros::Subscriber sub_Vel_from_Controller;
ros::Subscriber sub_Vel_from_Controller_Volt;

ros::Subscriber sub_Command_Type;

ros::Subscriber sub_MCC_Watchdog;

ros::Subscriber sub_output_pid;

ros::Subscriber inputs_from_gui_subscriber;

int ret;

int command_type = 2;

bool MCC_Watchdog_flag = false;

int seqNumber_AutoHeading = 0;
int seqNumber_AutoDepth = 0;
int seqNumber_AutoAltitude = 0;
int seqNumber_CameraTilt = 0;
int seqNumber_LightControl = 0;

double lateral;
double vertical;
double forwardreverse;
double rotate;

double altitude;
double depth;
double heading;
double tilt;

float u_max;	//m/s	(-10% of maximum velocity in Performance/Dimensions)
float v_max;
float w_max;
float r_max; 	//rad/s

ros::Time start_time;

ros::Time start_time_MCC_Watchdog;

ros::Time end_time;

diagnostic_msgs::DiagnosticStatus diagnostic_msg;

void writePower() //Power at DAQ board
{

	std::cout << "\n\nVolt written on DAQ board\n\n";

}

void setDIObit(int outputPin, bool value, int command_type, int seqNumber) //Set digital I/O
{

	msg_CmdAck.ID = command_type;
//	msg_CmdAck.sequence_number = seqNumber;
	msg_CmdAck.command = value;
	
    command_ack_publisher.publish(msg_CmdAck); //Publish Ack
}

void read_AnalogDigitConverter() //Read sensors value
{
    double altitude_volts, depth_volts, heading_volts, tilt_volts;
	
    altitude = 1;
    depth = 1;
    heading = 1;
    tilt = 1;
    /*
    ROS_INFO("altitude: %f\n", altitude);
    ROS_INFO("depth: %f\n", depth);
    ROS_INFO("heading: %f\n", heading);
    ROS_INFO("tilt: %f\n", tilt);
*/
    sensors_data_msg.data.resize(4,1);

    sensors_data_msg.data[0] = heading;
    sensors_data_msg.data[1] = depth;
    sensors_data_msg.data[2] = altitude;
    sensors_data_msg.data[3] = tilt;

    sensors_publisher->publish(sensors_data_msg);

    msg_Tilt_value.pose.orientation.y = tilt;
    tilt_publisher->publish(msg_Tilt_value);
}

void stop_DigitalAnalogConverter()
{

std::cout << "\n\nThrusters stopped\n\n";

}

void write_DigitalAnalogConverter(double lat, double vert, double fow_rev, double rot) //Write thrusters value
{
    if (MCC_Watchdog_flag == true)
    {
        if(command_type == 2)
        {
		std::cout << "\n\nThrusters written\n\n";
        }
    }
    else
        stop_DigitalAnalogConverter();
}

void write_teleopDigitalAnalogConverter(double lat, double vert, double fow_rev, double rot) //Write thrusters value
{
    if(command_type == 1)
    {
	std::cout << "\n\nThrusters written\n\n";
    }
}

void callback_AutoHeading(const dexrov_msgs::AutoHeading& autoHeadingInfo) //Callback AutoHeading
{
    msg_AutoHeading.switchSetting = autoHeadingInfo.switchSetting;
	
    setDIObit(0, msg_AutoHeading.switchSetting, 1, seqNumber_AutoHeading);
//	msg_CmdAck.ID = AUTOHEADING_CMD_STATUS;
	//msg_CmdAck.sequence_number = seqNumber;
//	msg_CmdAck.command = msg_AutoHeading.switchSetting;
	
//    command_ack_publisher.publish(msg_CmdAck); //Publish Ack
    seqNumber_AutoHeading = seqNumber_AutoHeading + 1;
    ROS_INFO("AutoHeading %d\n", msg_AutoHeading.switchSetting);
}

void callback_AutoDepth(const dexrov_msgs::AutoDepth& autoDepthInfo) //Callback AutoDepth
{
    msg_AutoDepth.switchSetting = autoDepthInfo.switchSetting;
	
    setDIObit(0, msg_AutoDepth.switchSetting, 3, seqNumber_AutoDepth);

    ROS_INFO("AutoDepth %d\n", msg_AutoDepth.switchSetting);
    seqNumber_AutoDepth = seqNumber_AutoDepth + 1;
}

void callback_AutoAltitude(const dexrov_msgs::AutoAltitude& autoAltitudeInfo) //Callback AutoAltitude
{
    msg_AutoAltitude.switchSetting = autoAltitudeInfo.switchSetting;
	
    setDIObit(0, msg_AutoAltitude.switchSetting, 2, seqNumber_AutoAltitude);

    ROS_INFO("AutoAltitude %d\n", msg_AutoAltitude.switchSetting);
    seqNumber_AutoAltitude = seqNumber_AutoAltitude + 1;
}

void callback_CameraTilt(const dexrov_msgs::CameraTilt& cameraTiltInfo) //Callback tilt of cameras
{
    msg_CameraTilt.cameraName = cameraTiltInfo.cameraName;
    msg_CameraTilt.tiltDirection = cameraTiltInfo.tiltDirection;
	
    if(msg_CameraTilt.cameraName == "camDown")
    {
  /*      if (msg_CameraTilt.tiltDirection == true)
        {
           setDIObit(0, false, 5, seqNumber_CameraTilt);
        }
*/
        setDIObit(0, msg_CameraTilt.tiltDirection, 5, seqNumber_CameraTilt);
	ROS_INFO("Camera: CAMDOWN %d\n", msg_CameraTilt.tiltDirection);
		
        diagnostic_msg.level = 0;
        diagnostic_msg.message = "Camera Down message received";

        diagnostic_publisher.publish(diagnostic_msg);
    }
    else
    {
  /*
        if (msg_CameraTilt.tiltDirection == true)
        {
            setDIObit(0, false, 5, seqNumber_CameraTilt);
        }
*/
        setDIObit(0, msg_CameraTilt.tiltDirection, 5, seqNumber_CameraTilt);
	ROS_INFO("Camera: CAMUP %d\n", msg_CameraTilt.tiltDirection);

        diagnostic_msg.level = 0;
        diagnostic_msg.message = "Camera Up message received";

        diagnostic_publisher.publish(diagnostic_msg);
    }

    seqNumber_CameraTilt = seqNumber_CameraTilt + 1;
}

void callback_LightControl(const dexrov_msgs::LightControl& lightControlInfo) //Callback of light control
{
    msg_LightControl.lightName = lightControlInfo.lightName;
    msg_LightControl.switchSetting = lightControlInfo.switchSetting;
	cout << "\n\nLIGHT CONTROL CB\n\n";
    if (msg_LightControl.lightName == "light1")
    {
        setDIObit(0, msg_LightControl.switchSetting, 4, seqNumber_LightControl);

	ROS_INFO("LIGHT CONTROL: light 1 %d\n", msg_LightControl.switchSetting);     

        diagnostic_msg.level = 0;
        diagnostic_msg.message = "Light 1 message received";

        diagnostic_publisher.publish(diagnostic_msg);
    }
    else
    {
        setDIObit(0, msg_LightControl.switchSetting, 4, seqNumber_LightControl);

	ROS_INFO("LIGHT CONTROL: light 2 %d\n", msg_LightControl.switchSetting);

        diagnostic_msg.level = 0;
        diagnostic_msg.message = "Light 2 message received";

        diagnostic_publisher.publish(diagnostic_msg);
    }

    seqNumber_LightControl = seqNumber_LightControl +1;
}

//Saturation method
float sat (Vector4f x, float c)
{
    float value;
    Vector4f temp;
    temp(0) = fabs(x(0));
    temp(1) = fabs(x(1));
    temp(2) = fabs(x(2));
    temp(3) = fabs(x(3));
    float normInf = temp.maxCoeff();

    if (normInf == 0.0)
        value = 0.0;
    else if (fabs(normInf) < c)
        value = 1.0;
    else
        value = c/(fabs(normInf));

    return value;
}

void callback_output_pid(const geometry_msgs::TwistStamped msg_output)
{
    start_time = ros::Time::now();

    float lat;
    float vert;
    float forw;
    float rot;
    Vector4f vector;
    Vector4f sat_vector;

    //Convert in volts
    lat = (double)msg_output.twist.linear.y;
    vert = (double)msg_output.twist.linear.z;
    forw = (double)msg_output.twist.linear.x;
    rot = (double)msg_output.twist.angular.z;

    vector(0) = lat;
    vector(1) = vert;
    vector(2) = forw;
    vector(3) = rot;

    sat_vector = vector*sat(vector,5.0);

 /*   lat = (double)msg_output.twist.linear.y;
    vert = (double)msg_output.twist.linear.z;
    forw = (double)msg_output.twist.linear.x;
    rot = (double)msg_output.twist.angular.z; */

    lat = sat_vector(0);
    vert = sat_vector(1);
    forw = sat_vector(2);
    rot = sat_vector(3);

    msg_Volt.forwardReverse = forw*1000;
    msg_Volt.lateral = lat*1000;
    msg_Volt.vertical = vert*1000;
    msg_Volt.rotate = rot*1000;

    voltage_publisher.publish(msg_Volt);

//    write_DigitalAnalogConverter(lat, vert, forw, rot);
}

void callback_TeleOp_Thrusters(const dexrov_msgs::RovTeleopCmd& msg_rovTeleOpCmd) //Callback teleop of thrusters
{	
	
    if (command_type == 1)
    {
        double tmpLateral;
        double tmpVertical;
        double tmpForwReve;
        double tmpRotate;

        tmpLateral = ((double)msg_rovTeleOpCmd.lateral)/100;
        tmpVertical = ((double)msg_rovTeleOpCmd.vertical)/100;
        tmpForwReve = ((double)msg_rovTeleOpCmd.forwardReverse)/100;
        tmpRotate = ((double)msg_rovTeleOpCmd.rotate);
        tmpRotate = tmpRotate * M_PI/180;
 // 	std::cout << "\n\nWritten voltage\n\n";
 	if(MCC_Watchdog_flag){
  	geometry_msgs::Twist vel_msg;
  	vel_msg.linear.x = tmpForwReve;
  	vel_msg.linear.y = tmpLateral;
  	vel_msg.linear.z = tmpVertical;
  	vel_msg.angular.z = tmpRotate;
  	rov_pub.publish(vel_msg);    
  	std::cout << vel_msg;
  	}
  	else{
  	
  	geometry_msgs::Twist vel_msg;
  	vel_msg.linear.x = 0;
  	vel_msg.linear.y = 0;
  	vel_msg.linear.z = 0;
  	vel_msg.angular.z = 0;
  	rov_pub.publish(vel_msg);    
  	
  	}
  	setDIObit(0, command_type, 7, 0);
//        write_teleopDigitalAnalogConverter(tmpLateral, tmpVertical, tmpForwReve, tmpRotate);
    }
}


void callback_Command_Type(const dexrov_msgs::Command& msg_Command) //Callback command type
{
    command_type = msg_Command.command;

    if (command_type == 0)
    {
//        stop_DigitalAnalogConverter();
        diagnostic_msg.level = 0;
        diagnostic_msg.message = "Thrusters have been stopped by MCC command";
	std::cout << "\nThrusters have been stopped by MCC command\n";
        diagnostic_publisher.publish(diagnostic_msg);
        
	msg_CmdAck.ID = 8;
	msg_CmdAck.command = command_type;
        command_ack_publisher.publish(msg_CmdAck); //Publish Ack
    }
    if (command_type == 1)
    {
//        stop_DigitalAnalogConverter();
        diagnostic_msg.level = 0;
        diagnostic_msg.message = "RovTeleopCmd enabled";
	std::cout << "\nRovTeleopCmd enabled\n";
        diagnostic_publisher.publish(diagnostic_msg);
        
        msg_CmdAck.ID = 8;
	msg_CmdAck.command = command_type;
    	command_ack_publisher.publish(msg_CmdAck); //Publish Ack
    }
    if (command_type == 2)
    {
//        stop_DigitalAnalogConverter();
        diagnostic_msg.level = 0;
        diagnostic_msg.message = "Control Law enabled";
        std::cout << "\nControl Law enabled\n";
	
	msg_CmdAck.ID = 8;
	msg_CmdAck.command = command_type;
        command_ack_publisher.publish(msg_CmdAck); //Publish Ack
        diagnostic_publisher.publish(diagnostic_msg);
    }
    
    
}

/*void inputs_callback(const qtpkg::inputs msg)
{
    u_max = msg.u_max;
    v_max = msg.v_max;
    w_max = msg.w_max;
    r_max = msg.r_max * M_PI/180;
}
*/
void callback_MCC_Watchdog(const dexrov_msgs::Command msg) //Callback watchdog
{
    start_time_MCC_Watchdog = ros::Time::now();
//    std::cout << "\nreceived heartbeat\n";
    MCC_Watchdog_flag = true;
}

int main (int argc, char **argv)
{
//    ret = motenc.OpenBoard();
	
  // writePower();
//    write_DigitalAnalogConverter(0.0, 0.0, 0.0, 0.0);
    ros::init(argc, argv, "wrapper_DexROV");
    ros::NodeHandle nodeLocal("~");
    ros::NodeHandle nh;

    //default values
    u_max = 1.6;
    v_max = 1.3;
    w_max = 0.68;
    r_max = 120.0 * M_PI/180;

  /*  motenc.ClearDIOOutputBit(MOTENCLITE_AUTOHEADING_PIN);
    motenc.ClearDIOOutputBit(MOTENCLITE_AUTOALTITUDE_PIN);
    motenc.ClearDIOOutputBit(MOTENCLITE_AUTODEPTH_PIN);
    motenc.ClearDIOOutputBit(MOTENCLITE_LIGHT1_PIN);
    motenc.ClearDIOOutputBit(MOTENCLITE_LIGHT2_PIN);
    motenc.ClearDIOOutputBit(MOTENCLITE_TILTCAMERAUP_PIN);
    motenc.ClearDIOOutputBit(MOTENCLITE_TILTCAMERADOWN_PIN);
*/
    tilt_publisher = new ros::Publisher(nh.advertise<geometry_msgs::PoseStamped>("/camera/pose",1));
    twist_publisher = nh.advertise<geometry_msgs::TwistStamped>("/debug/desired_velocity", 1);    
    command_ack_publisher = nh.advertise<dexrov_msgs::Command>("/rov/command_ack", 1);
    sensors_publisher = new ros::Publisher(nh.advertise<std_msgs::Float32MultiArray>("/rov/sensors_data",1));
    voltage_publisher = nh.advertise<dexrov_msgs::RovTeleopCmd>("/debug/voltage", 1);
    diagnostic_publisher = nh.advertise<diagnostic_msgs::DiagnosticStatus>("/debug/diagnostic", 1);
    rov_pub = nh.advertise<geometry_msgs::Twist>("/rov/cmd_vel",1);
    //Read values from ROS launch
/*	if(nodeLocal.getParam("u_max", u_max)) {
		printf("Read parameter surge max speed = %f m/s\n", u_max);
	}
	if(nodeLocal.getParam("v_max", v_max)) {
		printf("Read parameter sway max speed = %f m/s\n", v_max);
	}
	if(nodeLocal.getParam("w_max", w_max)) {
		printf("Read parameter heave max speed = %f m/s\n", w_max);
	}
	if(nodeLocal.getParam("r_max", r_max)) {
		r_max = r_max * M_PI/180;
		printf("Read parameter yaw max speed = %f rad/s\n", r_max);
        }*/
	
    sub_AutoHeading = nh.subscribe("/mcc/auto_heading", 1, &callback_AutoHeading);
    sub_AutoDepth = nh.subscribe("/mcc/auto_depth", 1, &callback_AutoDepth);
    sub_AutoAltitude = nh.subscribe("/mcc/auto_altitude", 1, &callback_AutoAltitude);
    sub_CameraTilt = nh.subscribe("/mcc/camera_tilt", 1, &callback_CameraTilt);
    sub_LightControl = nh.subscribe("/mcc/light_control", 1, &callback_LightControl);

    sub_TeleOp_Thrusters = nh.subscribe("/mcc/teleoperation_command",1, &callback_TeleOp_Thrusters);
//	sub_Vel_from_Controller = nh.subscribe("/rov/control_law_vel", 1, &callback_Vel_from_Controller);
//    sub_Vel_from_Controller_Volt = nh.subscribe("/rov/control_law_vel_volt", 1, &callback_Vel_from_Controller_Volt);
    sub_output_pid = nh.subscribe("/debug/output_pid", 1, &callback_output_pid);    
    sub_Command_Type = nh.subscribe("/mcc/teleop_status", 1, &callback_Command_Type);
    sub_MCC_Watchdog = nh.subscribe("/mcc/heartbeat", 1, &callback_MCC_Watchdog);
//    inputs_from_gui_subscriber = nh.subscribe("/debug/start", 1, inputs_callback);
	
    ros::Rate rate(0.5);
	
    while(ros::ok())
    {
       //cout << msg_Volt << "\n\n";

       read_AnalogDigitConverter();  //Read ROV information

       end_time = ros::Time::now();

       if(((end_time - start_time_MCC_Watchdog).toSec()) > 5)
       {
   //         write_DigitalAnalogConverter(0.0, 0.0, 0.0, 0.0);
            ROS_INFO_STREAM("WARNING: Communication with MCC does not work\n");
            MCC_Watchdog_flag = false;

            diagnostic_msg.level = 1;
            diagnostic_msg.message = "Communication with MCC does not work";

            diagnostic_publisher.publish(diagnostic_msg);
       }

      if(((end_time - start_time).toSec()) > 5) //Control on time of messages
       {
 //           write_DigitalAnalogConverter(0.0, 0.0, 0.0, 0.0);
      //      ROS_INFO_STREAM("WARNING: Communication with control law does not work\n");

            diagnostic_msg.level = 1;
            diagnostic_msg.message = "Wrapper does not receive velocity message: thrusters have been stopped";

            diagnostic_publisher.publish(diagnostic_msg);
       }

        ros::spinOnce();
        rate.sleep();
    }
}
