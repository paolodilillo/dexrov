#include <vehicle_dynamics.h>

using namespace Eigen;

/*

MODELLO MARIS

*/
VehicleDynamics::VehicleDynamics()
{
}
VehicleDynamics::~VehicleDynamics()
{
}

VectorXd VehicleDynamics::InverseDynamics(VectorXd e, VectorXd nu, VectorXd dnu, Vector3d g0, VectorXd nu_c)
{

    Vector3d rv_g, rv_b;
    MatrixXd M_v(6, 6), D(6, 6), DD(6, 6);
    double m_v, B, W;
    VectorXd temp1(6), temp2(6), temp3(6);

    g0 << 0, 0, 9.81;
    rv_g << -0.001, 0, 0.1;
    rv_b << 0.0, 0, 0;
    m_v = 450;
    W = m_v * g0.norm();
    B = 4714;

    temp1 << 700, 900, 1000, 93, 93, 93;
    temp2 << 50, 150, 115, 24, 24, 24;
    temp3 << 320, 360, 430, 31, 31, 31;

    M_v = temp1.asDiagonal();
    D = temp2.asDiagonal();
    DD = temp3.asDiagonal();

    double xG, yG, zG, xB, yB, zB;
    xG = rv_g(0);
    yG = rv_g(1);
    zG = rv_g(2);
    xB = rv_b(0);
    yB = rv_b(1);
    zB = rv_b(2);

    double t1, t2, t3, t4, t5, t6, t7, t8, t9, t10;
    t1 = pow(e(0), 2);
    t2 = pow(e(1), 2);
    t3 = pow(e(2), 2);
    t4 = e(0) * e(1);
    t5 = e(0) * e(2);
    t6 = e(1) * e(2);
    t7 = e(3) * e(0);
    t8 = e(3) * e(1);
    t9 = e(3) * e(2);
    t10 = pow(e(3), 2);

    double dummy1, dummy2, dummy3;

    dummy1 = -t10 + t1 + t2 - t3;
    dummy2 = 2 * (t8 - t5);
    dummy3 = 2 * (t7 + t6);

    VectorXd g(6);
    g(0) = dummy2 * (W - B);
    g(1) = -dummy3 * (W - B);
    g(2) = dummy1 * (W - B);
    g(3) = dummy1 * (yG * W - yB * B) + dummy3 * (zG * W - zB * B);
    g(4) = -dummy1 * (xG * W - xB * B) + dummy2 * (zG * W - zB * B);
    g(5) = -dummy3 * (xG * W - xB * B) - dummy2 * (yG * W - yB * B);

    Matrix3d R;
    R = quat2rot(e);
    MatrixXd Rappo(6, 6);
    Rappo.block(0, 0, 3, 3) = R;
    Rappo.block(0, 3, 3, 3) = Matrix3d::Zero();
    Rappo.block(3, 3, 3, 3) = Matrix3d::Zero();
    Rappo.block(3, 0, 3, 3) = Matrix3d::Zero();

    VectorXd nu_r(6);
    VectorXd tau(6);

    nu_c = Rappo.transpose() * nu_c;
    nu_r = nu - nu_c;

    tau = M_v * dnu + D * nu_r + DD * nu_r.cwiseProduct(nu_r.cwiseAbs()) + g;

    return tau;
}

VectorXd VehicleDynamics::DirectDynamics(Vector3d eta2, VectorXd zita, VectorXd tau, VectorXd nu_c)
{

    VectorXd tau_n(6);
    Vector3d g0;
    g0 << 0, 0, 9.81;
    VectorXd zeros(6);
    zeros.setZero();
    VectorXd zeros2(6);
    zeros2.setZero();

    VectorXd quat(4);
    quat = rpy2quat(eta2);

    tau_n = InverseDynamics(quat, zita, zeros, g0, nu_c);

    g0 << 0, 0, 0;
    VectorXd e(6);
    e.setZero();

    MatrixXd M(6, 6);
    /*
    for(int i=0;i<6;i++){

        e.setZero();
        e(i) = 1;
        M.col(i) = InverseDynamics(eta2,zeros,e,g0,zeros2);

    }
*/
    VectorXd temp1(6);
    temp1 << 700, 900, 1000, 93, 93, 93;
    M = temp1.asDiagonal();
    VectorXd dzita(6);
    dzita = mypinv(M) * (tau - tau_n);

    return dzita;
}

VectorXd VehicleDynamics::computeG(VectorXd e)
{

    Vector3d g0;
    g0 << 0, 0, 9.81;
    double m_v, B, W;
    m_v = 450;
    W = m_v * g0.norm();
    B = 4714;
    Vector3d rv_g, rv_b;

    double xG, yG, zG, xB, yB, zB;
    xG = rv_g(0);
    yG = rv_g(1);
    zG = rv_g(2);
    xB = rv_b(0);
    yB = rv_b(1);
    zB = rv_b(2);

    double t1, t2, t3, t4, t5, t6, t7, t8, t9, t10;
    t1 = pow(e(0), 2);
    t2 = pow(e(1), 2);
    t3 = pow(e(2), 2);
    t4 = e(0) * e(1);
    t5 = e(0) * e(2);
    t6 = e(1) * e(2);
    t7 = e(3) * e(0);
    t8 = e(3) * e(1);
    t9 = e(3) * e(2);
    t10 = pow(e(3), 2);

    double dummy1, dummy2, dummy3;

    dummy1 = -t10 + t1 + t2 - t3;
    dummy2 = 2 * (t8 - t5);
    dummy3 = 2 * (t7 + t6);

    VectorXd g(6);
    g(0) = dummy2 * (W - B);
    g(1) = -dummy3 * (W - B);
    g(2) = dummy1 * (W - B);
    g(3) = dummy1 * (yG * W - yB * B) + dummy3 * (zG * W - zB * B);
    g(4) = -dummy1 * (xG * W - xB * B) + dummy2 * (zG * W - zB * B);
    g(5) = -dummy3 * (xG * W - xB * B) - dummy2 * (yG * W - yB * B);

    return g;
}

AdaptiveControlVehicle::AdaptiveControlVehicle(VectorXd gamma)
{
    VectorXd l(6), d(6), in(9);
    l << 1, 1, 1, 1, 1, 1;
    d << 1, 1, 1, 1, 1, 1;
    in << 1, 1, 1, 1, 1, 1, 1, 1, 1;
    gamma_v.resize(9);
    gamma_v = gamma;
    lambda = l.asDiagonal();
    Kd = d.asDiagonal();
    invKgamma = in.asDiagonal();
}

AdaptiveControlVehicle::~AdaptiveControlVehicle()
{
}

VectorXd AdaptiveControlVehicle::AdaptiveControl(VectorXd eta, VectorXd nu, VectorXd eta_d, VectorXd nu_d)
{

    VectorXd quat, quat_d;
    quat = rpy2quat(eta.tail(3));
    quat_d = rpy2quat(eta_d.tail(3));

    Vector3d e_o;
    e_o = quatError(quat_d, quat);

    Vector3d eta_tilde1;
    eta_tilde1 = eta_d.head(3) - eta.head(3);

    VectorXd nu_tilde(6);
    nu_tilde = nu_d - nu;

    Matrix3d R;
    R = quat2rot(quat);

    VectorXd error(6);
    error.head(3) = R.transpose() * eta_tilde1;
    error.tail(3) = e_o;

    VectorXd s(6);
    s = lambda * error + nu_tilde;

    Vector3d temp;
    temp << 0, 0, 1;

    MatrixXd phi_p(6, 9);
   /* phi_p.block(0, 0, 3, 3) << Matrix3d::Zero();
    phi_p.block(0, 3, 3, 3) << R.transpose();
    phi_p.block(0, 6, 3, 3) << Matrix3d::Zero();
    phi_p.block(3, 0, 3, 3) << skew(R.transpose() * temp);
    phi_p.block(3, 3, 3, 3) << Matrix3d::Zero();
    phi_p.block(0, 6, 3, 3) << R.transpose();
*/
    phi_p(0,0) =  0;  phi_p(0,1) = 0 ;  phi_p(0,2) =  0;         phi_p(0,3) =  R.transpose()(0,0);  phi_p(0,4) =  R.transpose()(0,1);    phi_p(0,5) = R.transpose()(0,2);         phi_p(0,6) =  0;  phi_p(0,7) =  0;  phi_p(0,8) = 0;
    phi_p(1,0) =  0;  phi_p(1,1) = 0 ;  phi_p(1,2) =  0;         phi_p(1,3) =  R.transpose()(1,0);  phi_p(1,4) =  R.transpose()(1,1);    phi_p(1,5) =  R.transpose()(1,2);         phi_p(1,6) =  0;  phi_p(1,7) =  0;  phi_p(1,8) = 0;
    phi_p(2,0) =  0;  phi_p(2,1) = 0 ;  phi_p(2,2) =  0;         phi_p(2,3) =  R.transpose()(2,0);  phi_p(2,4) =  R.transpose()(2,1);    phi_p(2,5) =  R.transpose()(2,2);         phi_p(2,6) =  0;  phi_p(2,7) =  0;  phi_p(2,8) = 0;

    phi_p(3,0) =  skew(R.transpose() * temp)(0,0);  phi_p(3,1) = skew(R.transpose() * temp)(0,1) ;  phi_p(3,2) =  skew(R.transpose() * temp)(0,2);           phi_p(3,3) =  0;  phi_p(3,4) = 0 ;  phi_p(3,5) = 0 ;           phi_p(3,6) =  R.transpose()(0,0);  phi_p(3,7) =  R.transpose()(0,1);  phi_p(3,8) = R.transpose()(0,2);
    phi_p(4,0) =  skew(R.transpose() * temp)(1,0);  phi_p(4,1) =  skew(R.transpose() * temp)(1,1);  phi_p(4,2) =  skew(R.transpose() * temp)(1,2);           phi_p(4,3) =  0;  phi_p(4,4) = 0 ;  phi_p(4,5) = 0 ;           phi_p(4,6) =  R.transpose()(1,0);  phi_p(4,7) =  R.transpose()(1,1);  phi_p(4,8) = R.transpose()(1,2);
    phi_p(5,0) =  skew(R.transpose() * temp)(2,0);  phi_p(5,1) =  skew(R.transpose() * temp)(2,1);  phi_p(5,2) =  skew(R.transpose() * temp)(2,2);           phi_p(5,3) =  0;  phi_p(5,4) = 0 ;  phi_p(5,5) = 0 ;           phi_p(5,6) =  R.transpose()(2,0);  phi_p(5,7) =  R.transpose()(2,1);  phi_p(5,8) = R.transpose()(2,2);

    VectorXd tau(6);
    tau = Kd * s + phi_p * gamma_v;
    gamma_v_dot = invKgamma * phi_p.transpose() * s;
    gamma_v = gamma_v +  gamma_v_dot * ts;





    return tau;
}
