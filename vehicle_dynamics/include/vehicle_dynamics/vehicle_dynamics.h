#include <Eigen/Core>
#include <Eigen/Dense>
#include <ExtendedTaskHierarchy.h>

using namespace Eigen;

class VehicleDynamics
{

private:
  MatrixXd M_v;
  MatrixXd D;
  MatrixXd DD;
  double rho;

public:
  VehicleDynamics();
  ~VehicleDynamics();
  VectorXd InverseDynamics(VectorXd e, VectorXd nu, VectorXd dnu, Vector3d g0, VectorXd nu_c);
  VectorXd DirectDynamics(Vector3d eta2, VectorXd zita, VectorXd tau, VectorXd nu_c);
  VectorXd computeG(VectorXd e);
};

class AdaptiveControlVehicle
{

private:
  VectorXd gamma_v;
  VectorXd gamma_v_dot;
  MatrixXd Kd;
  MatrixXd lambda;
  MatrixXd invKgamma;
  double ts = 0.001;

public:
  AdaptiveControlVehicle(VectorXd gamma);
  ~AdaptiveControlVehicle();
  VectorXd AdaptiveControl(VectorXd eta, VectorXd nu, VectorXd eta_d, VectorXd nu_d);
};
