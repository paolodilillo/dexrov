#include "ros/ros.h"
#include "std_msgs/String.h"
#include <Eigen/Core>

using namespace std;
using namespace Eigen;

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "talker");

  ros::NodeHandle n;

 
  ros::Rate loop_rate(100);
	
  int m = 1000;
  MatrixXd A = MatrixXd::Random(m,m);
  MatrixXd B = MatrixXd::Random(m,m);
  MatrixXd C(m,m);
  
  ros::Time start, end;
  while (ros::ok())
  {
   
    start = ros::Time::now();
    
    C = A*B;
    
    end = ros::Time::now();
    
    cout << "tempo: " << end-start << endl;

    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
