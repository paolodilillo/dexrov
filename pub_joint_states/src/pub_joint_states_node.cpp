#include "ros/ros.h"
#include <sensor_msgs/JointState.h>
#include <Eigen/Core>
#include <ExtendedTaskHierarchy.h>

using namespace std;
using namespace Eigen;

VectorXd qRightReal(6), qLeftReal(6), qRightRealTemp(6), qLeftRealTemp(6), qRightGazebo(6), qLeftGazebo(6);
double xavier_fix, xavier_fix2;
bool init=false;


void chatterCallback2(const sensor_msgs::JointState msg)
{
	qLeftRealTemp(0) = msg.position[0];
	qLeftRealTemp(1) = msg.position[1];
	qLeftRealTemp(2) = msg.position[2];
	qLeftRealTemp(3) = msg.position[3];
	qLeftRealTemp(4) = msg.position[4];
	qLeftRealTemp(5) = msg.position[5];
	qLeftReal = mask_Gazebo2DH(qLeftRealTemp, qLeftRealTemp.size());
	
	qLeftGazebo(0) = qLeftReal(0);// - PI/2;
	xavier_fix2 = qLeftReal(0) - PI/2;
	qLeftGazebo(1) = -(qLeftReal(1) - 1.1961);
	qLeftGazebo(2) = qLeftReal(2) +  1.1961;
	qLeftGazebo(3) = -qLeftReal(3);
	qLeftGazebo(4) = -qLeftReal(4) + PI/2;
	qLeftGazebo(5) = qLeftReal(5);

}

void chatterCallback(const sensor_msgs::JointState msg)
{
	qRightRealTemp(0) = msg.position[0];
	qRightRealTemp(1) = msg.position[1];
	qRightRealTemp(2) = msg.position[2];
	qRightRealTemp(3) = msg.position[3];
	qRightRealTemp(4) = msg.position[4];
	qRightRealTemp(5) = msg.position[5];
	qRightReal = mask_Gazebo2DH_right(qRightRealTemp, qRightRealTemp.size());
	
	qRightGazebo(0) = qRightReal(0);// - PI/2;
	xavier_fix = qRightReal(0) - PI/2;
	qRightGazebo(1) = -(qRightReal(1) - 1.1961);
	qRightGazebo(2) = qRightReal(2) +  1.1961;
	qRightGazebo(3) = -qRightReal(3);
	qRightGazebo(4) = -qRightReal(4);
	qRightGazebo(5) = qRightReal(5);
	
	init = true;
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "joint_publisher");

  ros::NodeHandle n;

  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("/joint_states", 1);
  ros::Publisher right_pub = n.advertise<sensor_msgs::JointState>("/arm/R/joint_state", 1);
  ros::Publisher left_pub = n.advertise<sensor_msgs::JointState>("/arm/L/joint_state", 1);

  ros::Subscriber sub_left = n.subscribe("/arm/L/fbk_joint_position", 1, chatterCallback2);
  ros::Subscriber sub = n.subscribe("/arm/R/fbk_joint_position", 1, chatterCallback);
  ros::Rate loop_rate(5);

  sensor_msgs::JointState msgRight;
  
  sensor_msgs::JointState msgRight2;
  sensor_msgs::JointState msgLeft2;
  
  msgRight2.name.resize(6);
  msgRight2.position.resize(6);
  msgLeft2.name.resize(6);
  msgLeft2.position.resize(6);
  
  msgRight.name.resize(12);
  msgRight.position.resize(12);
  
  while (ros::ok())
  {
   
   if(init == true){
  
    msgRight.header.stamp = ros::Time::now();
    msgRight.name[0] = "arm_right_joint1";
    msgRight.name[1] = "arm_right_joint2";
    msgRight.name[2] = "arm_right_joint3";
    msgRight.name[3] = "arm_right_joint4";
    msgRight.name[4] = "arm_right_joint5";
    msgRight.name[5] = "arm_right_joint6";
    msgRight.position[0] = qRightGazebo(0);
    msgRight.position[1] = qRightGazebo(1);
    msgRight.position[2] = qRightGazebo(2);
    msgRight.position[3] = qRightGazebo(3);
    msgRight.position[4] = qRightGazebo(4);
    msgRight.position[5] = qRightGazebo(5);
    
    msgRight2.header.stamp = ros::Time::now();
    msgRight2.name[0] = "arm_right_joint1";
    msgRight2.name[1] = "arm_right_joint2";
    msgRight2.name[2] = "arm_right_joint3";
    msgRight2.name[3] = "arm_right_joint4";
    msgRight2.name[4] = "arm_right_joint5";
    msgRight2.name[5] = "arm_right_joint6";
    msgRight2.position[0] = xavier_fix;
    msgRight2.position[1] = qRightGazebo(1);
    msgRight2.position[2] = qRightGazebo(2);
    msgRight2.position[3] = qRightGazebo(3);
    msgRight2.position[4] = qRightGazebo(4);
    msgRight2.position[5] = qRightGazebo(5);
    
    msgRight.name[6] = "arm_left_joint1";
    msgRight.name[7] = "arm_left_joint2";
    msgRight.name[8] = "arm_left_joint3";
    msgRight.name[9] = "arm_left_joint4";
    msgRight.name[10] = "arm_left_joint5";
    msgRight.name[11] = "arm_left_joint6";
    msgRight.position[6] = qLeftGazebo(0);
    msgRight.position[7] = qLeftGazebo(1);
    msgRight.position[8] = qLeftGazebo(2);
    msgRight.position[9] = qLeftGazebo(3);
    msgRight.position[10] = qLeftGazebo(4);
    msgRight.position[11] = qLeftGazebo(5);
    
    msgLeft2.name[0] = "arm_left_joint1";
    msgLeft2.name[1] = "arm_left_joint2";
    msgLeft2.name[2] = "arm_left_joint3";
    msgLeft2.name[3] = "arm_left_joint4";
    msgLeft2.name[4] = "arm_left_joint5";
    msgLeft2.name[5] = "arm_left_joint6";
    msgLeft2.position[0] = xavier_fix2;
    msgLeft2.position[1] = qLeftGazebo(1);
    msgLeft2.position[2] = qLeftGazebo(2);
    msgLeft2.position[3] = qLeftGazebo(3);
    msgLeft2.position[4] = qLeftGazebo(4);
    msgLeft2.position[5] = qLeftGazebo(5);
    
    joint_pub.publish(msgRight);
    right_pub.publish(msgRight2);
    left_pub.publish(msgLeft2);
    
   }
    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
